# credit-flow-gen CFG低代码工具

## 介绍

CFG是一款基于 Vue3 + TypeScript 快速打造的低代码开发工具。具备以下特性&能力：

- 提供低代码工具脚手架CLI：可通过npm全局安装，是一款可以在本地运行低代码开发平台工具。生成的相关文件也保存在本地项目目录下。

- 组件->区块->页面->应用：创建应用，每个应用下创建页面，每个页面由若干区块和组件组成，每个区块又由若干组件组成。

- 应用源码支持模块方式的Vue3代码导出：低代码工具（编辑器）基于Vue3+TypeScript开发，支持导出Vue3源码，并且是按照模块的维度导出成zip压缩包并支持下载，压缩包文件包括：应用配置文件、页面.vue文件、路由文件、useHooks函数文件、入口index.ts文件。

- 丰富的工具方法库&工具方法开发脚手架(TODO)：工具方法库基于TypeScript开发。官方提供基础工具方法（因每个业务不同，需要业务自行封装各自的jsApi方法和定制化业务方法）。提供脚手架允许开发者开发工具方法，参与共建。

## 软件架构

![软件架构图](./soft-arch.png)

## 环境要求

参考[credit-flow-gen CFG低代码工具开发指引文档](https://docs.qq.com/doc/DQ0xNcE13TlhzaHpv)中的“环境准备”一节。

## 快速体验

[使用脚手架体验](./packages/create-cfg/README.md)

## 二次开发或共建

### 按照上述环境要求，安装依赖

### 拉取&启动项目

如果需要二开或贡献代码，可以拉取仓库master分支

```sh
git clone https://gitee.com/george_chen/credit-flow-gen.git
cd credit-flow-gen
npm run setup && npm run build && npm run apps:web:dev
```

- 首次启动需要执行初始化：`npm run setup && npm run build`
- 启动开发环境：`npm run apps:web:dev`
- 清理项目环境：`npm run clean` 清理后需要重新执行初始化

## 参与贡献

1. 拉取git项目
2. 基于master，新建本地开发分支：rel_xxx / feat_xxx
3. 开发完成，提交代码至本地开发分支
4. 新建 Pull Request，合入master
5. 注意：如果需要升级子应用或者模块的版本&发布npmjs，需要将你的开发分支合到next，使用next分支进行版本&发布npmjs，然后新建 Pull Request，next合入master

## 模块说明

- `packages` lerna发布模块
  - `base` 适配Node环境和浏览器环境-基础模块
  - `charts` 封装图表组件模块
  - `cli` 命令行和创建Vite配置-工具包模块
  - `coder` Vue文件代码生成器模块
  - `core` 低代码协议和模型定义-核心模块
  - `designer` 低代码设计器模块
  - `icons` 图标库模块
  - `local` 提供本地开发低代码平台-路由接口和插件模块
  - `materials` 低代码平台-物料库模块
  - `node` 适配Node环境-工具包模块
  - `renderer` 低代码Vue渲染器模块
  - `ui` 低代码平台界面-通用组件库模块
  - `utils` 适配浏览器环境-工具包模块
  - `pro` 低代码平台-通用版CFG.PRO
  - `web` 低代码平台-Web端应用使用模块
  - `create-cfg` 体验低代码CLI工具
- `apps` 应用
  - `web` CFG低代码平台-Web端应用项目示例

## FAQ

Q1: 执行pnpm add --global lerna报错：ERR_PNPM_NO_GLOBAL_BIN_DIR  Unable to find the global bin directory
Run "pnpm setup" to create it automatically, or set the global-bin-dir setting, or the PNPM_HOME env variable. The global bin directory should be in the PATH.

A1: 1）执行pnpm setup
2）重新启动命令提示符或 PowerShell，再执行pnpm add --global lerna

Q2: 根目录下执行npm run build构建packages下的某个模块（比如cfg-packages-pro模块，vue-tsc && cross-env ENV_TYPE=live vite build）。报错：error TS6305: Output file 'xxx/credit-flow-gen/packages/pro/proxy.config.d.ts' has not been built from source file 'xxx/credit-flow-gen/packages/pro/proxy.config.ts'.The file is in the program because: Matched by default include pattern '**/*'。类似这样的报错一般都是因为npm依赖版本升级带来的问题。

A2: 检查根目录package.json或者packages下相应模块的package.json。比如上面cfg-packages-pro模块执行npm run build实际是：vue-tsc && cross-env ENV_TYPE=live vite build。主要和根目录package.json的3个npm包有关：typescript、vue-tsc、vite。可以选择固定包版本号（"typescript": "5.4.3"）或者采用~指定 npm 包的版本范围（"typescript": "~5.4.3"）。
