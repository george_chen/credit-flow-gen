import { createViteConfig } from 'cfg-packages-cli';
import { createDevTools } from 'cfg-packages-pro/vite';
import proxy from './proxy.config';

export default createViteConfig({
  proxy,
  // 导出目录需要配置成静态文件托管，这样就可以下载压缩文件了
  staticDirs: ['.exports'],
  plugins: [
    createDevTools({
      devMode: false,
      pluginNodeModulesDir: '../../node_modules'
    })
  ]
});
