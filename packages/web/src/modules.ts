import { ContextMode } from './renderer';

export function createModules(mode: ContextMode = ContextMode.Runtime) {
  if (mode === ContextMode.Runtime || process.env.NODE_ENV === 'development') {
    return import.meta.glob(['/.cfg/projects/*.json', '/.cfg/files/*.json', '/.cfg/vue/*.vue']);
  } else {
    return import.meta.glob(['/.cfg/projects/*.json', '/.cfg/vue/*.vue']);
  }
}
