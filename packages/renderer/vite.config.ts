import { createViteConfig } from 'cfg-packages-cli';

export default createViteConfig({
  lib: true,
  dts: true,
  version: true,
  formats: ['es', 'cjs'],
  external: [
    'vue',
    'vue-router',
    'cfg-packages-base',
    'cfg-packages-core',
    'cfg-packages-ui',
    'cfg-packages-utils',
    'cfg-packages-icons',
    'element-plus',
    '@element-plus/icons-vue',
    'mockjs'
  ]
});
