import { expect, test } from 'vitest';
import { createProvider, ContextMode } from '../dist/index.mjs';

test('createProvider', () => {
  const mode = ContextMode.Design;
  const { provider } = createProvider({
    service: {},
    dependencies: {},
    mode
  });
  expect(provider.mode).toBe(mode);
});
