import { defineComponent, h } from 'vue';
import { useRoute } from 'vue-router';
import { useTitle } from '@vueuse/core';
import { useProvider } from './provider';

// 指定页面id的路由/page/:id/:prjId下的页面容器组件
export const PageContainer = defineComponent({
  name: 'CfgPageContainer',
  async setup() {
    const provider = useProvider();
    const route = useRoute();
    const id = route.params.id as string;
    const file = id ? provider.getPage(id) : provider.getHomepage();
    // 页面文件（一般是vue文件）对应的组件
    const component = file ? await provider.getRenderComponent(file.id) : null;
    if (file) {
      useTitle(file.title || 'CFG低代码平台');
    }
    return {
      provider,
      component,
      file,
      query: route.query
    };
  },
  render() {
    if (this.component) {
      return h(this.component, this.query);
    } else {
      return h('div', '页面不存在');
    }
  }
});
