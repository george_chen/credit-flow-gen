import { defineComponent, h } from 'vue';
import { XStartup } from 'cfg-packages-ui';

// 启动页
export const StartupContainer = defineComponent({
  name: 'CfgStartupContainer',
  render() {
    return h(XStartup);
  }
});
