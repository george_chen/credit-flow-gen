/**!
 * Copyright (c) 2024, credit-flow-gen CFG All rights reserved.
 * @name cfg-packages-renderer
 * @version 0.0.48
 */
export const version = '0.0.48';
