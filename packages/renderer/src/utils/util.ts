import type { Plugin } from 'vue';
import { isFunction, isString } from 'cfg-packages-utils';

export function toString(value: any) {
  return isString(value) ? value : JSON.stringify(value);
}

/**
 * css样式表内容添加到document.adoptedStyleSheets，如果浏览器不支持此特性，
 * 则回退到传统的 document.head <style> 标签方式
 * @param global
 * @param id
 * @param css
 */
export function adoptedStyleSheets(global: Window, id: string, css: string) {
  const CSSStyleSheet = (global as any).CSSStyleSheet;
  // chrome > 71 才支持 replaceSync
  if (CSSStyleSheet.prototype.replaceSync) {
    const styleSheet = new CSSStyleSheet();
    styleSheet.id = id;
    styleSheet.replaceSync(css);
    const doc: any = global.document;
    const adoptedStyleSheets = doc.adoptedStyleSheets;
    const sheets = Array.from(adoptedStyleSheets).filter((n: any) => n.id !== id);
    doc.adoptedStyleSheets = [...sheets, styleSheet];
  } else {
    const doc = global.document;
    let styleSheet = doc.getElementById(id);
    if (styleSheet) {
      styleSheet.innerHTML = css;
    } else {
      styleSheet = doc.createElement('style');
      styleSheet.id = id;
      styleSheet.innerHTML = css;
      doc.head.appendChild(styleSheet);
    }
  }
}

/**
 * 根据url获取css样式表内容，添加到document
 * @param id
 * @param url
 */
export async function loadCss(id: string, url: string) {
  const css = await window
    .fetch(url)
    .then((res) => res.text())
    .catch(() => '');
  if (css) {
    adoptedStyleSheets(window, id, css);
  }
}

/**
 * css样式文件urls数组遍历添加到document.head <link> 标签中
 * @param urls
 * @param global
 */
export function loadCssUrl(urls: string[], global: any = window) {
  const doc = global.document;
  const head = global.document.head;
  for (const url of urls) {
    const el = doc.getElementById(url);
    if (!el) {
      const link = doc.createElement('link');
      link.rel = 'stylesheet';
      link.id = url;
      link.href = url;
      head.appendChild(link);
    }
  }
}

/**
 * 动态加载 JavaScript 文件并返回特定的全局模块对象
 * @param urls 一个字符串数组，包含需要加载的 JavaScript 文件的 URL
 * @param library 需要在全局对象中查找的模块名称
 * @param global 可选参数，默认为 window，表示全局对象
 * @returns
 */
export async function loadScriptUrl(urls: string[], library: string, global: any = window) {
  // 在全局对象中检查指定的 library 是否已存在。如果存在，直接返回该模块
  let module = global[library];
  if (module) return module.default || module;
  // 动态加载脚本并使用reslove返回模块
  const doc = global.document;
  const head = global.document.head;
  return new Promise((reslove, inject) => {
    for (const url of urls) {
      const el = doc.createElement('script');
      el.src = url;
      el.onload = () => {
        module = global[library];
        if (module) {
          reslove(module.default || module);
        } else {
          inject(null);
        }
      };
      el.onerror = (e: any) => {
        inject(e);
      };
      head.appendChild(el);
    }
  });
}

export function isVuePlugin(value: unknown): value is Plugin {
  return isFunction(value) || isFunction((value as any)?.install);
}
