import { isUrl, url as urlUtils, dedupArray } from 'cfg-packages-utils';
import { version } from '../version';
import type { Dependencie, MaterialDescription } from 'cfg-packages-core';

export function fillBasePath(urls: string[], basePath: string) {
  return urls.map((url) => {
    if (isUrl(url)) {
      return url;
    }
    return `${basePath}${url}`;
  });
}

export function isCSSUrl(url: string): boolean {
  return /\.css$/.test(url);
}

export function isJSUrl(url: string): boolean {
  return /\.js$/.test(url);
}

export function isJSON(url: string): boolean {
  return /\.json$/.test(url);
}

/**
 * script 脚本链接url后面跟上版本号
 * @param scripts
 * @returns
 */
export function createAssetScripts(scripts: string[]): string {
  return scripts.map((url) => `<script src="${urlUtils.append(url, { v: version })}"></script>`).join('');
}

/**
 * css 样式文件链接url后面跟上版本号
 * @param css
 * @returns
 */
export function createAssetsCss(css: string[] = []) {
  return css.map((url) => `<link rel="stylesheet" href="${urlUtils.append(url, { v: version })}" />`).join('');
}

/**
 * 从依赖项数组 deps 中提取出脚本文件、CSS文件、物料资源、库导出、库映射和库的本地化映射 等信息，
 * 按类别整理返回
 * @param deps
 * @param basePath
 * @returns
 */
export function parseDeps(deps: Dependencie[], basePath: string) {
  // 从传入的依赖项数组中过滤出启用的依赖项
  const packages = deps.filter((n) => !!n.enabled);

  // 初始化存储脚本、CSS、物料资源、库导出、库映射和库的本地化映射 等的数组和对象
  const scripts: string[] = [];
  const css: string[] = [];
  const materials: string[] = [];
  const libraryExports: string[] = [];
  const libraryMap: Record<string, string[]> = {};
  const libraryLocaleMap: Record<string, string> = {};
  const materialExports: string[] = [];
  const materialMapLibrary: Record<string, string> = {};

  // 遍历启用的依赖项，提取相关信息
  packages.forEach(({ urls, assetsUrl, library, assetsLibrary, localeLibrary }) => {
    // 处理依赖项的urls，区分JS和CSS
    urls?.forEach((url) => {
      if (isJSUrl(url)) {
        scripts.push(url);
      }
      if (isCSSUrl(url)) {
        css.push(url);
      }
    });

    // 处理库相关的信息
    if (library) {
      libraryExports.push(library);
      libraryMap[library] = fillBasePath(urls || [], basePath);
      if (localeLibrary) {
        libraryLocaleMap[library] = localeLibrary;
      }
    }

    // 处理物料静态资源相关的信息
    if (assetsUrl) {
      materials.push(assetsUrl);
    }
    if (assetsLibrary) {
      materialExports.push(assetsLibrary);
    }
    if (library && assetsLibrary) {
      materialMapLibrary[assetsLibrary] = library;
    }
  });

  // 返回整理后的结果
  return {
    scripts: fillBasePath(scripts, basePath),
    css: fillBasePath(css, basePath),
    materials: fillBasePath(materials, basePath),
    libraryExports,
    materialExports: dedupArray(materialExports),
    materialMapLibrary,
    libraryMap,
    libraryLocaleMap
  };
}

/**
 * 根据组件名称/别名/组件父类，从库中获取原始组件
 * @param desc
 * @param lib
 * @returns
 */
export function getRawComponent(desc: MaterialDescription, lib: any) {
  const { name, parent, alias } = desc;
  return parent ? lib[parent]?.[alias || name] : lib[alias || name];
}
