import type { JSExpression, JSFunction } from 'cfg-packages-core';
import { logger } from 'cfg-packages-utils';

/**
 * 解析一个JavaScript表达式或函数，并将执行结果作为函数返回
 * @param str 一个包含JavaScript表达式或函数的对象
 * @param self 上下文对象，在解析表达式时会使用这个对象来替代 this
 * @param thisRequired 布尔值，是否需要 this 关键字
 * @param throwError 布尔值，解析表达式时发生错误是否抛出异常
 * @returns 执行结果函数
 */
export function parseExpression(str: JSExpression | JSFunction, self: any, thisRequired = false, throwError = false) {
  try {
    const contextArr = ['"use strict";', 'var __self = arguments[0];'];
    contextArr.push('return ');
    let tarStr: string = (str.value || '').trim();
    tarStr = tarStr.replace(/this(\W|$)/g, (_a: any, b: any) => `__self${b}`);
    tarStr = contextArr.join('\n') + tarStr;
    const code = `with(${thisRequired ? '{}' : '$scope || {}'}) { ${tarStr} }`;
    return new Function('$scope', code)(self);
  } catch (err: any) {
    logger.error('parseExpression.error', err, str, self?.__self ?? self);
    if (throwError) {
      throw err;
    }
  }
}

/**
 * 解析一个JavaScript函数，并将执行结果显示作为函数返回
 * @param str
 * @param self
 * @param thisRequired
 * @param throwError
 * @returns
 */
export function parseFunction(str: JSFunction, self: any, thisRequired = false, throwError = false) {
  const fn = parseExpression(str, self, thisRequired, throwError);
  if (typeof fn !== 'function') {
    logger.error('parseFunction.error', 'not a function', str, self?.__self ?? self);
    if (throwError) {
      throw new Error(`"${str.value}" not a function`);
    }
  }
  // eslint-disable-next-line @typescript-eslint/ban-types
  return fn as Function;
}

export function isJSExpression(data: any): data is JSExpression {
  return data && data.type === 'JSExpression';
}

export function isJSFunction(x: any): x is JSFunction {
  return typeof x === 'object' && x && x.type === 'JSFunction';
}

export function isJSCode(data: unknown): data is JSExpression | JSFunction {
  return isJSExpression(data) || isJSFunction(data);
}

export function JSCodeToString(data: unknown) {
  if (isJSCode(data)) {
    return data.value;
  } else {
    return JSON.stringify(data);
  }
}
