# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.63](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.62...cfg-packages-pro@0.0.63) (2024-10-23)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.62](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.61...cfg-packages-pro@0.0.62) (2024-10-12)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.61](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.60...cfg-packages-pro@0.0.61) (2024-10-12)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.60](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.59...cfg-packages-pro@0.0.60) (2024-10-12)

### Features

- pkg.json 中 devDependencies 依赖项放到 dependencies 中 ([6343215](https://gitee.com/george_chen/credit-flow-gen/commits/63432152bc2c3e8d2f2eae255fad6031974e0862))

## [0.0.59](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.58...cfg-packages-pro@0.0.59) (2024-10-12)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.58](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.57...cfg-packages-pro@0.0.58) (2024-10-12)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.57](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.56...cfg-packages-pro@0.0.57) (2024-10-11)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.56](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.55...cfg-packages-pro@0.0.56) (2024-10-11)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.55](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.54...cfg-packages-pro@0.0.55) (2024-10-10)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.54](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.53...cfg-packages-pro@0.0.54) (2024-09-13)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.53](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.52...cfg-packages-pro@0.0.53) (2024-09-13)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.52](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.51...cfg-packages-pro@0.0.52) (2024-09-13)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.51](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.50...cfg-packages-pro@0.0.51) (2024-09-13)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.50](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.49...cfg-packages-pro@0.0.50) (2024-09-10)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.49](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.48...cfg-packages-pro@0.0.49) (2024-09-01)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.48](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.47...cfg-packages-pro@0.0.48) (2024-08-30)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.47](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.46...cfg-packages-pro@0.0.47) (2024-08-30)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.46](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.45...cfg-packages-pro@0.0.46) (2024-08-29)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.45](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.44...cfg-packages-pro@0.0.45) (2024-08-22)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.44](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.43...cfg-packages-pro@0.0.44) (2024-08-21)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.43](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.42...cfg-packages-pro@0.0.43) (2024-08-20)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.42](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.41...cfg-packages-pro@0.0.42) (2024-08-20)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.41](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.40...cfg-packages-pro@0.0.41) (2024-08-20)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.40](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.39...cfg-packages-pro@0.0.40) (2024-08-20)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.39](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.38...cfg-packages-pro@0.0.39) (2024-08-20)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.38](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.37...cfg-packages-pro@0.0.38) (2024-08-12)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.37](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.36...cfg-packages-pro@0.0.37) (2024-08-12)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.36](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.35...cfg-packages-pro@0.0.36) (2024-08-12)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.35](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.34...cfg-packages-pro@0.0.35) (2024-08-11)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.34](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.33...cfg-packages-pro@0.0.34) (2024-08-11)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.33](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.32...cfg-packages-pro@0.0.33) (2024-08-11)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.32](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.31...cfg-packages-pro@0.0.32) (2024-08-11)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.31](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.30...cfg-packages-pro@0.0.31) (2024-08-11)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.30](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.29...cfg-packages-pro@0.0.30) (2024-08-09)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.29](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.28...cfg-packages-pro@0.0.29) (2024-08-09)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.28](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.27...cfg-packages-pro@0.0.28) (2024-08-09)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.27](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.24...cfg-packages-pro@0.0.27) (2024-08-09)

### Features

- 完善编辑器部分功能 ([8f5769e](https://gitee.com/george_chen/credit-flow-gen/commits/8f5769ecd4286777be534cbfeb9d221274052b49))
- 项目导出完善完成，准备发版 ([d744af6](https://gitee.com/george_chen/credit-flow-gen/commits/d744af60ea8cec90f266a0cc25c5634c41adbf0b))
- 项目导出完善完成，准备发版 ([66a970e](https://gitee.com/george_chen/credit-flow-gen/commits/66a970e777f06024a270c9be41aa6c26a165535f))
- 项目列表页，新增/编辑项目开发中 ([963b466](https://gitee.com/george_chen/credit-flow-gen/commits/963b466c02609acc7cc932d9e0d6fe37857a108b))
- 项目列表页及项目切换相关功能完成开发 ([b6da9e8](https://gitee.com/george_chen/credit-flow-gen/commits/b6da9e85328869fa1931a5465f5656644daccb04))

## [0.0.26](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.24...cfg-packages-pro@0.0.26) (2024-08-09)

### Features

- 完善编辑器部分功能 ([8f5769e](https://gitee.com/george_chen/credit-flow-gen/commits/8f5769ecd4286777be534cbfeb9d221274052b49))
- 项目导出完善完成，准备发版 ([66a970e](https://gitee.com/george_chen/credit-flow-gen/commits/66a970e777f06024a270c9be41aa6c26a165535f))
- 项目列表页，新增/编辑项目开发中 ([963b466](https://gitee.com/george_chen/credit-flow-gen/commits/963b466c02609acc7cc932d9e0d6fe37857a108b))
- 项目列表页及项目切换相关功能完成开发 ([b6da9e8](https://gitee.com/george_chen/credit-flow-gen/commits/b6da9e85328869fa1931a5465f5656644daccb04))

## [0.0.25](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.24...cfg-packages-pro@0.0.25) (2024-08-09)

### Features

- 完善编辑器部分功能 ([8f5769e](https://gitee.com/george_chen/credit-flow-gen/commits/8f5769ecd4286777be534cbfeb9d221274052b49))
- 项目列表页，新增/编辑项目开发中 ([963b466](https://gitee.com/george_chen/credit-flow-gen/commits/963b466c02609acc7cc932d9e0d6fe37857a108b))
- 项目列表页及项目切换相关功能完成开发 ([b6da9e8](https://gitee.com/george_chen/credit-flow-gen/commits/b6da9e85328869fa1931a5465f5656644daccb04))

## [0.0.24](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.23...cfg-packages-pro@0.0.24) (2024-07-23)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.23](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.22...cfg-packages-pro@0.0.23) (2024-07-23)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.22](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.21...cfg-packages-pro@0.0.22) (2024-07-23)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.21](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.20...cfg-packages-pro@0.0.21) (2024-07-22)

### Features

- 项目列表页，源码导出功能 开发中 ([3c4266b](https://gitee.com/george_chen/credit-flow-gen/commits/3c4266b8f454912752cbdf3959e2e66619e0dae7))

## [0.0.20](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.19...cfg-packages-pro@0.0.20) (2024-07-08)

### Features

- cfg-packages-web 低代码平台-Web 端应用使用模块 开发完成，待发布 npm 包 ([c9fae6c](https://gitee.com/george_chen/credit-flow-gen/commits/c9fae6c52f2b4a331133dc2d9837b072d98a433b))

## [0.0.19](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.18...cfg-packages-pro@0.0.19) (2024-07-06)

### Features

- cfg-packages-pro 低代码开发平台-通用版 CFG.PRO 开发完成 ([b157b61](https://gitee.com/george_chen/credit-flow-gen/commits/b157b61fe88c18593356c1d35859d626b102a2c9))

## [0.0.18](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.17...cfg-packages-pro@0.0.18) (2024-07-01)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.17](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.16...cfg-packages-pro@0.0.17) (2024-07-01)

### Features

- cfg-packages-designer 模块开发完成 ([107bfcf](https://gitee.com/george_chen/credit-flow-gen/commits/107bfcfd30cd8aef651f67cf94d9f918cea3c1e9))

## [0.0.16](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.15...cfg-packages-pro@0.0.16) (2024-06-25)

### Features

- cfg-packages-renderer 开发中 ([2ee44d1](https://gitee.com/george_chen/credit-flow-gen/commits/2ee44d1c59239835798f72be1cfe6cde679576d8))

## [0.0.15](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.14...cfg-packages-pro@0.0.15) (2024-06-14)

### Features

- cfg-packages-materials 物料包模块增加 ui、charts、element 三大物料，后续再增加 vant、antv ([a7ec2f2](https://gitee.com/george_chen/credit-flow-gen/commits/a7ec2f2b0ff68fd2f4ccfbe6f918b3d6b2c44b12))

## [0.0.14](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.13...cfg-packages-pro@0.0.14) (2024-06-13)

### Features

- 完成 cfg-packages-ui 开发，准备发布 npm 包 ([96db65a](https://gitee.com/george_chen/credit-flow-gen/commits/96db65aa0ff114e77dba06ef85f7b2ae35d30de2))

## [0.0.13](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.12...cfg-packages-pro@0.0.13) (2024-06-03)

### Features

- 完成 cfg-packages-charts npm 包开发 ([62e90fd](https://gitee.com/george_chen/credit-flow-gen/commits/62e90fd7747705fe7b45876e02c56ca969d4e532))

## [0.0.12](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.11...cfg-packages-pro@0.0.12) (2024-06-03)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.11](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.10...cfg-packages-pro@0.0.11) (2024-06-02)

### Features

- 更新 prettier 相关依赖安装包；完成 cfg-packages-coder 开发；cfg-packages-local 开发中 ([58b5531](https://gitee.com/george_chen/credit-flow-gen/commits/58b5531e8b800f029ac390e81d741e028ffccabd))

## [0.0.10](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.9...cfg-packages-pro@0.0.10) (2024-05-31)

### Features

- cfg-packages-utils 包开发中，需要加入部分方法 ([c9b80b4](https://gitee.com/george_chen/credit-flow-gen/commits/c9b80b469bc23624e05a74ea6576c328e7c12907))

## [0.0.9](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.8...cfg-packages-pro@0.0.9) (2024-05-31)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.8](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.7...cfg-packages-pro@0.0.8) (2024-05-28)

**Note:** Version bump only for package cfg-packages-pro

## [0.0.7](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.6...cfg-packages-pro@0.0.7) (2024-05-28)

### Features

- 提取项目公共 npm 包到根目录下的 package.json；完成 cfg-packages-core 模块的开发 ([b64dc5f](https://gitee.com/george_chen/credit-flow-gen/commits/b64dc5f86d396d0c14cc7423b15f22c0816415fe))

## [0.0.6](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.5...cfg-packages-pro@0.0.6) (2024-05-18)

### Features

- cfg-packages-cli 包开发中 ([cd1abc7](https://gitee.com/george_chen/credit-flow-gen/commits/cd1abc738f9cbbade1fd37268518c75885ce9a93))

## [0.0.5](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.4...cfg-packages-pro@0.0.5) (2024-05-12)

### Features

- packages/base 模块初始化 ([154afd9](https://gitee.com/george_chen/credit-flow-gen/commits/154afd9d88021af694f9e43dd6a461076aba8c92))

## [0.0.4](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.3...cfg-packages-pro@0.0.4) (2024-05-10)

### Features

- 初步完善 cfg-packages-pro 模块包 ([4606f78](https://gitee.com/george_chen/credit-flow-gen/commits/4606f786c399b123c69715b192946fbea52070a3))

## [0.0.3](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-pro@0.0.2...cfg-packages-pro@0.0.3) (2024-05-10)

### Features

- 初步完善 cfg-packages-pro 模块包 ([2c4caf2](https://gitee.com/george_chen/credit-flow-gen/commits/2c4caf21cca7a65ffe3d2a30967cf86c4893e043))
- 完善 publish ([918e941](https://gitee.com/george_chen/credit-flow-gen/commits/918e941a75112c36c0b7095d6b05429d288a2e51))

## 0.0.2 (2024-05-09)

### Features

- 完成跨项目包依赖 ([c5df67f](https://gitee.com/george_chen/credit-flow-gen/commits/c5df67f72aaa4ad8d61ee1d280f32dc75c7c1cf4))
- 修改 packages\pro\package.json 的 name ([17b7d0a](https://gitee.com/george_chen/credit-flow-gen/commits/17b7d0a5e315c18f397d27fa5708696fc7ac1bf9))
- 验证 lerna version ([ab31f56](https://gitee.com/george_chen/credit-flow-gen/commits/ab31f56e30ea5c6c944ce8edaec37cb9f5763fc3))

## 0.0.1 (2024-05-08)

### Features

- 完成跨项目包依赖 ([c5df67f](https://gitee.com/george_chen/credit-flow-gen/commits/c5df67f72aaa4ad8d61ee1d280f32dc75c7c1cf4))
- 验证 lerna version ([ab31f56](https://gitee.com/george_chen/credit-flow-gen/commits/ab31f56e30ea5c6c944ce8edaec37cb9f5763fc3))
