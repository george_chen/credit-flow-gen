/**
 * 不同环境下配置门户主机 (PORTAL_HOST) 和门户主机接口 (PORTAL_HOST_I) 的地址
 */
// 本地环境的配置
export const local = {
  PORTAL_HOST: 'sso-sit.newpearl.com',
  PORTAL_HOST_I: 'ssoi-sit.newpearl.com'
};
// 开发环境的配置
export const dev = {
  PORTAL_HOST: 'sso-sit.newpearl.com',
  PORTAL_HOST_I: 'ssoi-sit.newpearl.com'
};
// 系统集成测试环境的配置
export const sit = {
  PORTAL_HOST: 'sso-sit.newpearl.com',
  PORTAL_HOST_I: 'ssoi-sit.newpearl.com'
};
// 用户验收测试环境的配置
export const uat = {
  PORTAL_HOST: 'sso-uat.newpearl.com',
  PORTAL_HOST_I: 'ssoi-uat.newpearl.com'
};
// 预发布环境的配置
export const pre = {
  PORTAL_HOST: 'sso.newpearl.com',
  PORTAL_HOST_I: 'ssoi.newpearl.com'
};
// 生产环境的配置
export const live = {
  PORTAL_HOST: 'sso.newpearl.com',
  PORTAL_HOST_I: 'ssoi.newpearl.com'
};
