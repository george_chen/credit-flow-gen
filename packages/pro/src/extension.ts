import * as Vue from 'vue';

import * as core from 'cfg-packages-core';
import * as CfgUtils from 'cfg-packages-utils';
import * as CfgUI from 'cfg-packages-ui';
import * as designer from 'cfg-packages-designer';
import * as renderer from 'cfg-packages-renderer';
import * as CfgIcons from 'cfg-packages-icons';
import type { EngineOptions } from 'cfg-packages-designer';

import * as ElementPlus from 'element-plus';

export interface ExtensionOptions {
  urls: string[];
  library: string;
  params?: any[];
}

export type ExtensionFactory = () => Partial<EngineOptions> | void;

export class Extension {
  private urls: string[] = [];
  private library = '';
  private params: any[] = [];

  constructor(options: ExtensionOptions) {
    const __CFG_PRO__ = {
      ...core,
      ...designer,
      ...renderer
    };

    (window as any).Vue = Vue;
    (window as any).__CFG_PRO__ = __CFG_PRO__;
    (window as any).CfgUtils = CfgUtils;
    (window as any).CfgIcons = CfgIcons;
    (window as any).CfgUI = CfgUI;
    (window as any).ElementPlus = ElementPlus;

    const { urls, library, params = [] } = options;
    this.urls = urls;
    this.library = library;
    this.params = params;
  }

  /**
   * 加载外部插件css资源和js资源
   * @returns
   */
  async load(): Promise<Partial<EngineOptions> | undefined> {
    const css = this.urls.filter((n) => renderer.isCSSUrl(n));
    const scripts = this.urls.filter((n) => renderer.isJSUrl(n));
    renderer.loadCssUrl(css);
    if (scripts.length) {
      const output = await renderer.loadScriptUrl(scripts, this.library).catch(() => null);
      if (output && typeof output === 'function') {
        return output.apply(output, this.params);
      }
    }
  }
}
