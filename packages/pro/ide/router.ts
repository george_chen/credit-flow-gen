import { createRouter, createWebHashHistory } from 'vue-router';

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('./views/Index.vue')
  },
  {
    path: '/preview/:id/:prjId',
    name: 'preview',
    component: () => import('./views/Preview.vue')
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;
