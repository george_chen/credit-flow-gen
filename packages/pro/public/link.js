/**
 * html中插入 <script src="/cfg-packages-pro/link.js"></script>
 */

(function () {
  // <div id="cfgLink">xxx</div> 元素 和 style样式
  const style = document.createElement('style');
  style.innerHTML = `
    #cfgLink {
      position: absolute;
      right: 50px;
      bottom: 50px;
      width: 40px;
      height: 40px;
      border: none;
      z-index: 9999;
      cursor: pointer;
      border-radius: 50%;
      display: none;
      justify-content: center;
      align-items: center;
      border: 1px solid #eee;
      background: rgba(255,255,255,.8);
      box-shadow: 0 0 3px rgba(0,0,0,.1);
    }
    #cfgLink svg {
      height: 20px;
      width: 20px;
      color: #333;
    }
    #cfgLink:hover {
      background: #ecf5ff;
    }
    #cfgLink:hover svg {
      color:#409eff;
    }
    .cfg-link-dragging {
      user-select: none;
      iframe {
        user-select: none;
        pointer-events: none;
      }
    }
  `;
  const el = document.createElement('div');
  el.id = 'cfgLink';
  el.innerHTML =
    '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024"><path fill="currentColor" d="m199.04 672.64 193.984 112 224-387.968-193.92-112-224 388.032zm-23.872 60.16 32.896 148.288 144.896-45.696zM455.04 229.248l193.92 112 56.704-98.112-193.984-112-56.64 98.112zM104.32 708.8l384-665.024 304.768 175.936L409.152 884.8h.064l-248.448 78.336zm384 254.272v-64h448v64h-448z"></path></svg>';
  document.head.appendChild(style);
  document.body.appendChild(el);

  let dragging = false;
  let diffX;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  let diffY;
  let isDragged = false;
  let startPosition = null;
  const size = 50;

  /**
   * 拖拉拽cfgLink，更新位置
   */
  const update = function (e) {
    if (dragging) {
      const x = Math.min(Math.max(0, e.clientX - diffX), window.innerWidth - size);
      const y = Math.min(Math.max(0, e.clientY - diffX), window.innerHeight - size);
      el.style.left = x + 'px';
      el.style.top = y + 'px';
    }
  };

  /**
   * 处理各个页面cfgLink是否显示
   * @returns
   */
  const handleLinkElShow = function () {
    // 启动页和项目列表页 隐藏cfgLink
    if (window.location.hash === '#/' || window.location.hash === '#/project-list') {
      el.style.display = 'none';
      return;
    }
    el.style.display = 'flex';
  };

  el.addEventListener('mousedown', function (e) {
    const rect = el.getBoundingClientRect();
    diffX = e.clientX - rect.left;
    diffY = e.clientY - rect.top;
    document.body.classList.add('cfg-link-dragging');
    startPosition = {
      x: e.clientX,
      y: e.clientY
    };
    isDragged = false;
    dragging = true;
  });

  window.addEventListener('mouseup', function (e) {
    dragging = false;
    e.stopPropagation();
    e.preventDefault();
    document.body.classList.remove('cfg-link-dragging');
    if (startPosition && Math.abs(startPosition.x - e.clientX) < 10 && Math.abs(startPosition.y - e.clientY) < 10) {
      isDragged = false;
    } else {
      isDragged = true;
    }
  });

  // 拖拽事件
  window.addEventListener('mousemove', function (e) {
    update(e);
  });

  // 页面首次加载时的处理逻辑
  window.addEventListener('load', function () {
    handleLinkElShow();
  });
  // 页面hash变化时的处理逻辑
  window.addEventListener('hashchange', function () {
    handleLinkElShow();
  });
  window.onhashchange = handleLinkElShow;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  el.addEventListener('click', function (e) {
    if (!isDragged) {
      const section = window.location.hash.split('/');
      const options = window.__CFG_LINK__ || {};
      let path = options.href || window.location.pathname + 'cfg-packages-pro/#/';
      let queryParams = '';
      // 提取 prjId 和 id
      const prjId = section.pop() || '';
      const id = section.pop() || '';
      // 如果有 prjId，添加到查询参数中
      if (prjId) {
        queryParams += `prjId=${encodeURIComponent(prjId)}`;
      }
      // 如果有 id，添加到查询参数中，如果有 prjId，则使用 & 连接
      if (id) {
        queryParams += (queryParams ? '&' : '') + `id=${encodeURIComponent(id)}`;
      }
      // 如果有查询参数，添加到路径中
      if (queryParams) {
        // 确保 # 后面有 ? 符号
        if (!path.includes('?')) {
          path += '?';
        }
        path += queryParams;
      }
      window.open(path, '_blank');
    }
  });
})();
