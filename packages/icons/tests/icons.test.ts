import { expect, test } from 'vitest';
import { CfgIconBlock } from '../dist/index.mjs';

test('CfgIconBlock.name is correct', () => {
  expect(CfgIconBlock.name).toBe('CfgIconBlock');
});
