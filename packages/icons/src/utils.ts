import { defineComponent, createVNode, markRaw } from 'vue';

/**
 * iconfont生成Vue组件方法
 * @param name
 * @param className
 * @returns
 */
export function createIconComponent(name: string, className: string) {
  return markRaw(
    defineComponent({
      name,
      render() {
        return createVNode('i', { class: className });
      }
    })
  );
}
