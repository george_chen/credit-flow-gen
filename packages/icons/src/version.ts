/**!
 * Copyright (c) 2024, credit-flow-gen CFG All rights reserved.
 * @name cfg-packages-icons
 * @version 0.0.55
 */
export const version = '0.0.55';
