import { type App } from 'vue';
import { icons } from './index';
export * from './index';

/**
 * 将icons遍历注册到app上作为全局组件
 * @param app
 * @returns
 */
export const install = (app: App) => {
  const installed = (app as any).__CFG_ICONS_INSTALLED__;
  if (installed) return;
  for (const [key, component] of Object.entries(icons)) {
    app.component(key, component as any);
  }
  (app as any).__CFG_ICONS_INSTALLED__ = true;
};
