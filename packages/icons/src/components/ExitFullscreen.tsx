/* eslint-disable prettier/prettier */
import { defineComponent, type PropType } from 'vue';

export const ExitFullscreen = defineComponent({
  // eslint-disable-next-line vue/component-definition-name-casing
  name: 'ExitFullscreen',
  props: {
    colors: {
      type: Array as PropType<string[]>,
      default: () => []
    }
  },
  setup(props) {
    // prettier-ignore
    return () => {
      const paths: any[] = [
        {
          path: 'M298.666667 298.666667V85.333333h85.333333v298.666667H85.333333V298.666667h213.333334z m426.666666-213.333334v213.333334h213.333334v85.333333h-298.666667V85.333333h85.333333zM298.666667 938.666667v-213.333334H85.333333v-85.333333h298.666667v298.666667H298.666667z m426.666666 0h-85.333333v-298.666667h298.666667v85.333333h-213.333334v213.333334z'
        }
      ];
      return (
        <svg viewBox="0 0 1024 1024" width="1em" height="1em">
          {paths.map((n: { path: string }, i: number) => {
            return <path d={n.path} fill={props.colors[i] ?? 'currentColor'} />;
          })}
        </svg>
      );
    };
  }
});
