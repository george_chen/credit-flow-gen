/* eslint-disable prettier/prettier */
import { defineComponent, type PropType } from 'vue';

export const Popup = defineComponent({
  // eslint-disable-next-line vue/component-definition-name-casing
  name: 'Popup',
  props: {
    colors: {
      type: Array as PropType<string[]>,
      default: () => []
    }
  },
  setup(props) {
    // prettier-ignore
    return () => {
      const paths: any[] = [
        {
          path: 'M959.72 0H294.216a63.96 63.96 0 0 0-63.96 63.96v127.92H64.28A63.96 63.96 0 0 0 0.32 255.84V959.4a63.96 63.96 0 0 0 63.96 63.96h703.56a63.96 63.96 0 0 0 63.96-63.96V792.465h127.92a63.96 63.96 0 0 0 63.96-63.96V63.96A63.96 63.96 0 0 0 959.72 0zM767.84 728.505V959.4H64.28V255.84h703.56z m189.322 0H831.8V255.84a63.96 63.96 0 0 0-63.96-63.96H294.216V63.96H959.72z'
        }
      ];
      return (
        <svg viewBox="0 0 1024 1024" width="1em" height="1em">
          {paths.map((n: { path: string }, i: number) => {
            return <path d={n.path} fill={props.colors[i] ?? 'currentColor'} />;
          })}
        </svg>
      );
    };
  }
});
