/* eslint-disable prettier/prettier */
import { defineComponent, type PropType } from 'vue';

export const Rows = defineComponent({
  // eslint-disable-next-line vue/component-definition-name-casing
  name: 'Rows',
  props: {
    colors: {
      type: Array as PropType<string[]>,
      default: () => []
    }
  },
  setup(props) {
    // prettier-ignore
    return () => {
      const paths: any[] = [
        {
          path: 'M915.692308 275.692308h-807.384616c-15.753846 0-29.538462-13.784615-29.538461-29.538462v-59.076923C78.769231 171.323077 92.553846 157.538462 108.307692 157.538462h807.384616c15.753846 0 29.538462 13.784615 29.538461 29.538461v59.076923c0 15.753846-13.784615 29.538462-29.538461 29.538462z m0 293.415384h-807.384616c-15.753846 0-29.538462-13.784615-29.538461-29.538461v-59.076923c0-13.784615 13.784615-27.569231 29.538461-27.569231h807.384616c15.753846 0 29.538462 13.784615 29.538461 29.538461v59.076924c0 13.784615-13.784615 27.569231-29.538461 27.56923z m0 297.353846h-807.384616c-15.753846 0-29.538462-13.784615-29.538461-29.538461v-59.076923c0-15.753846 13.784615-29.538462 29.538461-29.538462h807.384616c15.753846 0 29.538462 13.784615 29.538461 29.538462v59.076923c0 15.753846-13.784615 29.538462-29.538461 29.538461z'
        }
      ];
      return (
        <svg viewBox="0 0 1024 1024" width="1em" height="1em">
          {paths.map((n: { path: string }, i: number) => {
            return <path d={n.path} fill={props.colors[i] ?? 'currentColor'} />;
          })}
        </svg>
      );
    };
  }
});
