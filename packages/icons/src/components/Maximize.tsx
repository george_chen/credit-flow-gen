/* eslint-disable prettier/prettier */
import { defineComponent, type PropType } from 'vue';

export const Maximize = defineComponent({
  // eslint-disable-next-line vue/component-definition-name-casing
  name: 'Maximize',
  props: {
    colors: {
      type: Array as PropType<string[]>,
      default: () => []
    }
  },
  setup(props) {
    // prettier-ignore
    return () => {
      const paths: any[] = [
        {
          path: 'M926.45937303 97.54062697v828.2973677H97.54062697V97.54062697h828.91874606m4.97102697-77.6722963h-838.8608c-39.7682157 0-72.07989097 32.31167525-72.07989097 72.07989096v839.48217837c0 39.7682157 32.31167525 72.07989097 72.07989097 72.07989097h839.48217837c39.7682157 0 72.07989097-32.31167525 72.07989096-72.07989097v-838.8608c0-40.38959408-32.31167525-72.70126933-72.70126933-72.70126933 0.62137837 0 0 0 0 0z'
        }
      ];
      return (
        <svg viewBox="0 0 1024 1024" width="1em" height="1em">
          {paths.map((n: { path: string }, i: number) => {
            return <path d={n.path} fill={props.colors[i] ?? 'currentColor'} />;
          })}
        </svg>
      );
    };
  }
});
