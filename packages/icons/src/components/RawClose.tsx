/* eslint-disable prettier/prettier */
import { defineComponent, type PropType } from 'vue';

export const RawClose = defineComponent({
  // eslint-disable-next-line vue/component-definition-name-casing
  name: 'RawClose',
  props: {
    colors: {
      type: Array as PropType<string[]>,
      default: () => []
    }
  },
  setup(props) {
    // prettier-ignore
    return () => {
      const paths: any[] = [
        {
          path: 'M512 432.46249999L865.92499999 78.49999973a56.13749998 56.13749998 0 0 1 79.53750003-5e-8 56.17500029 56.17500029 0 0 1 0 79.53750002L591.49999971 512l353.925 353.92499999c21.97500029 21.97500029 22.12499972 57.4125003 0 79.53750003a56.17500029 56.17500029 0 0 1-79.53750004 0L512 591.49999971 158.07500001 945.50000027a56.13749998 56.13749998 0 0 1-79.53750003 5e-8 56.17500029 56.17500029 0 0 1 0-79.53750002L432.50000029 512 78.49999973 158.07500001a56.13749998 56.13749998 0 0 1-5e-8-79.53750003 56.17500029 56.17500029 0 0 1 79.53750002 0L512 432.50000029z'
        }
      ];
      return (
        <svg viewBox="0 0 1024 1024" width="1em" height="1em">
          {paths.map((n: { path: string }, i: number) => {
            return <path d={n.path} fill={props.colors[i] ?? 'currentColor'} />;
          })}
        </svg>
      );
    };
  }
});
