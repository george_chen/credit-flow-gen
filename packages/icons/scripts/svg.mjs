import { resolve, join } from 'path';
import { readdirSync, readFileSync, outputFileSync, emptyDirSync } from 'cfg-packages-node';
import { upperFirstCamelCase } from 'cfg-packages-base';
import { parseString } from 'xml2js';

const SRC_COMPONENETS_PATH = resolve('src/components');
// 读取src/svg目录下的全部svg文件
const SRC_SVG_DIR_PATH = resolve('src/svg');
const files = readdirSync(SRC_SVG_DIR_PATH);
// svg转换成tsx依赖的模板文件
const TEMPLATE_PATH = resolve('src/template.tsx');
const template = readFileSync(TEMPLATE_PATH, 'utf-8');

/**
 * svg内容解析出paths
 * @param {*} svg
 * @returns
 */
const svgParser = async (svg) => {
  return new Promise((resolve, reject) => {
    parseString(svg, (err, result) => {
      if (err) {
        console.error(err);
        reject(null);
        return;
      }
      const paths = (result.svg?.path || []).map((n) => {
        return {
          path: n.$.d,
          color: n.$.fill || undefined
        };
      });
      resolve(paths);
    });
  });
};

const outputComponents = async (files) => {
  const exports = [];
  const regex = /^[A-Z]+/;
  // 遍历src/svg目录下每个svg文件
  for (let file of files) {
    // 1. 读取每个svg文件内容
    const _name = file.split('.')[0];
    const name = upperFirstCamelCase(regex.test(_name) ? _name : `icon-${_name}`);
    const filePath = join(SRC_SVG_DIR_PATH, file);
    const fileContent = readFileSync(filePath, 'utf-8');
    // 2. 调用svgParser方法将svg内容解析出paths
    const paths = await svgParser(fileContent).catch((e) => {
      console.error('svgParser error: ', file, e);
      return null;
    });
    if (!paths) continue;
    // 3. paths和name插入模板src/template.tsx，生成最终的tsx文件内容，写入src/components/${name}.tsx
    // 创建 paths 对象数组的字符串表示
    const pathsString = paths
      .map(({ path, color }) =>
        color === undefined
          ? `
        {
          path: '${path}'
        }`
          : `
        {
          path: '${path}',
          color: '${color}'
        }`
      )
      .join(',')
      .trim();
    const content = template.replace(/__name__/g, name).replace("'__paths__'", pathsString);
    const output = join(SRC_COMPONENETS_PATH, `${name}.tsx`);
    outputFileSync(output, content, 'utf-8');
    exports.push(`export * from './${name}';`);
  }
  return exports.join('\n');
};

const execute = async () => {
  console.log('svg to vue start.');
  // 清空src/components目录
  emptyDirSync(SRC_COMPONENETS_PATH);
  // src/svg目录下全部svg文件转换成Vue组件的全部导出，写入src/components/index.ts
  let exportsContent = await outputComponents(files);
  exportsContent += '\n';
  outputFileSync(join(SRC_COMPONENETS_PATH, `index.ts`), exportsContent, 'utf-8');
  console.log('svg to vue success!');
};

// 执行脚本初始方法
execute();
