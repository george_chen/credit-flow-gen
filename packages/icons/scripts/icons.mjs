import { resolve } from 'path';
import { writeFileSync, readJsonSync } from 'cfg-packages-node';
import { upperFirstCamelCase } from 'cfg-packages-base';

console.log('create icons.ts start!');

const OUTPUT_PATH = resolve('src/icons.ts');

// 读取src/iconfont/iconfont.json中的图标内容
const SRC_ICONFONT_FILE_PATH = resolve('src/iconfont/iconfont.json');
const json = readJsonSync(SRC_ICONFONT_FILE_PATH, 'utf-8');
const icons = json.glyphs || [];

const comments = [];
const content = [`import { createIconComponent } from './utils';`];

for (const item of icons) {
  const className = `cfg-icon-${item.font_class}`;
  const name = upperFirstCamelCase(className);
  comments.push(name);
  content.push(`export const ${name} = createIconComponent('${name}', '${className}');`);
}

// 开头插入注释
content.unshift(`/**
 * ${comments.join(', ')}
 */`);

// 写入src/icons.ts文件
writeFileSync(OUTPUT_PATH, content.join('\n') + '\n', 'utf-8');

console.log('create icons.ts success!');
