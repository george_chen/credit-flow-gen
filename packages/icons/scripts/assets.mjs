import { resolve } from 'path';
import { readdirSync, outputFileSync } from 'cfg-packages-node';
import { upperFirstCamelCase } from 'cfg-packages-base';

console.log('assets build start.');

// 读取assets目录下的全部svg文件
const SRC_SVG_DIR_PATH = resolve('assets');
const files = readdirSync(SRC_SVG_DIR_PATH);

const imports = [];
const names = [];

for (const file of files) {
  const _name = file.split('.')[0];
  const name = upperFirstCamelCase(`icon-${_name}`);
  names.push(name);
  imports.push(`import ${name} from '../assets/${_name}.svg';`);
}

const content = `/// <reference types="vite/client" />
${imports.join('\n')}\n
export {
  ${names.join(',\n  ')}
};
`;

// 内容写入src/assets.ts文件
outputFileSync(resolve('src/assets.ts'), content, 'utf-8');

console.log('assets build success!');
