import { createViteConfig } from 'cfg-packages-cli';

/**
 * UMD(Universal Module Definition)模式，旨在解决模块在不同环境中的兼容性问题。
 * 可以同时在浏览器和 Node.js 环境下运行。
 */
const isUmd = !!process.env.UMD;

export default createViteConfig({
  lib: true,
  dts: isUmd ? false : true, // 如果是 UMD 模式，不生成 TypeScript 定义文件
  buildTarget: isUmd ? 'es2015' : 'esnext', // UMD 模式下的构建目标
  entry: isUmd ? 'src/install.ts' : 'src/index.ts', // UMD 模式下的入口文件
  version: true,
  library: 'CfgIcons',
  emptyOutDir: isUmd ? false : true,
  // UMD 模式已经内置了@element-plus/icons-vue，无需重复添加
  external: isUmd ? ['vue'] : ['vue', '@element-plus/icons-vue'], // UMD 模式下的外部依赖
  externalGlobals: isUmd
    ? {
        vue: 'Vue' // 将 Vue 作为全局变量暴露
      }
    : undefined,
  formats: isUmd ? ['umd'] : ['es'] // UMD 格式
});
