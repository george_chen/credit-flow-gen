import { expect, test } from 'vitest';
import { isBlock, BlockModel } from '../dist/index.mjs';
import { uid, upperFirstCamelCase } from 'cfg-packages-base';

test('isBlock', () => {
  const block = new BlockModel({
    id: uid(),
    name: upperFirstCamelCase('untitledBlock')
  });
  expect(isBlock(block)).toBe(true);
});
