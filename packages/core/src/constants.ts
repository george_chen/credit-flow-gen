/**
 * 内置物料名
 */
export const BUILT_IN_NAME = 'BuiltIn';

/**
 * 内置 vue 组件物料名
 */
export const BUILT_IN_VUE = 'VueMaterial';

/**
 * 内置 vue-router 组件物料名
 */
export const BUILT_IN_VUE_ROUTER = 'VueRouterMaterial';

/**
 * 内置物料
 */
export const BUILT_IN_MATERIALS = [BUILT_IN_VUE, BUILT_IN_VUE_ROUTER];

/**
 * 内置类库包名和导出名映射
 */
export const BUILT_IN_LIBRARAY_MAP: Record<string, string> = {
  vue: 'Vue',
  'vue-router': 'VueRouter'
};

/**
 * 内置组件
 */
export const BUILT_IN_COMPONENTS: Record<string, string[]> = {
  /**
   * Vue 内置组件：
   * Transition：用于包裹一个元素/组件，为其添加进入/离开的过渡效果。
   * TransitionGroup：类似于 Transition，但用于多个元素/组件的过渡。
   * KeepAlive：用于缓存不活动的组件实例，保留其状态。
   * Teleport：将其子组件渲染到指定的 DOM 节点外。
   * Suspense：用于处理异步组件加载的占位内容。
   */
  [BUILT_IN_VUE]: ['Transition', 'TransitionGroup', 'KeepAlive', 'Teleport', 'Suspense'],
  /**
   * Vue Router 内置组件：
   * RouterView：用于显示匹配的路由组件。
   * RouterLink：用于导航链接，触发路由变更。
   */
  [BUILT_IN_VUE_ROUTER]: ['RouterView', 'RouterLink']
};

/**
 * 内置html标签
 */
export const BUILT_IN_TAGS = ['slot', 'template', 'component', 'img', 'div', 'p', 'h1', 'h2', 'h3', 'span', 'a'];
