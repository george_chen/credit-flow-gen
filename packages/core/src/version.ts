/**!
 * Copyright (c) 2024, credit-flow-gen CFG All rights reserved.
 * @name cfg-packages-core
 * @version 0.0.57
 */
export const version = '0.0.57';
