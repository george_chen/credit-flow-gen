/**
 * 基类，子类可通过extends继承该类
 * protected保证该类和子类可调用
 */
export class Base {
  /**
   * 数组，用于存储回调函数。这些回调函数会在对象变得“就绪(isReady = true)”时被触发
   */
  protected listeners: Array<() => void> = [];

  /**
   * 布尔值，表示对象是否已经处于“就绪”状态
   */
  protected isReady = false;

  /**
   * 用于使对象就绪，且触发所有回调函数
   * 对象已经就绪，触发所有回调函数，清空回调函数数组，因为在对象已经就绪后，不再需要存储这些回调函数
   */
  protected triggerReady() {
    this.isReady = true;
    for (const listener of this.listeners) {
      listener();
    }
    this.listeners = [];
  }

  /**
   * 注册回调函数：如果对象已经就绪，直接触发回调函数，否则将回调函数存储到回调函数数组中
   * @param callback 回调函数
   */
  ready(callback: () => void) {
    if (this.isReady) {
      callback();
    } else {
      this.listeners.push(callback);
    }
  }

  /**
   * 重置对象“就绪”状态，将 isReady 重新设置为 false，表示对象不再处于“就绪”状态
   */
  resetReady() {
    this.isReady = false;
  }
}
