import type { NodeEvents, NodeEvent, NodeModifiers, JSFunction } from '../protocols';

/**
 * 事件类模型
 */
export class EventModel {
  public readonly name: string;
  public handler: JSFunction;
  public modifiers: NodeModifiers = {};

  constructor(private schema: NodeEvent) {
    const { name, handler } = this.schema;
    this.name = name;
    this.handler = handler;
    this.update(schema);
  }

  /**
   * 根据给定的schema，更新事件实例相关属性
   * @param schema
   */
  update(schema: Partial<NodeEvent>) {
    Object.assign(this.schema, schema);
    const { handler, modifiers = {} } = this.schema;
    this.handler = handler;
    this.modifiers = modifiers;
  }

  /**
   * 获取DSL
   * @param events
   * @returns
   */
  static toDsl(events: Record<string, EventModel>): NodeEvents {
    return Object.entries(events).reduce((result, [name, event]) => {
      const { handler, modifiers } = event;
      result[name] = { name, handler, modifiers };
      return result;
    }, {} as NodeEvents);
  }

  /**
   * 将入参events数组格式化解析成事件实例
   * @param events
   * @returns
   */
  static parse(events: NodeEvents = {}): Record<string, EventModel> {
    return Object.entries(events).reduce((result, [name, schema]) => {
      result[name] = new EventModel(schema);
      return result;
    }, {} as Record<string, EventModel>);
  }
}
