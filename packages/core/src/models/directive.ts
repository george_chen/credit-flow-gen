import type { NodeDirective, JSExpression, NodeModifiers, NodeDirectiveIterator } from '../protocols';
import { uid } from 'cfg-packages-base';

/**
 * 指令类模型
 */
export class DirectiveModel {
  /**
   * 标识
   */
  public readonly id: string;

  /**
   * 指令名称
   */
  public name = '';

  /**
   * 参数
   */
  public arg?: string | JSExpression;

  /**
   * 修饰符
   */
  public modifiers?: NodeModifiers;

  /**
   * 指令值
   */
  public value?: JSExpression;

  /**
   *  v-for 迭代器
   */
  public iterator?: NodeDirectiveIterator;

  constructor(private schema: NodeDirective) {
    this.id = schema.id || uid();
    this.update(schema);
  }

  /**
   * 根据给定的schema，更新指令实例相关属性
   * @param schema
   */
  update(schema: NodeDirective) {
    Object.assign(this.schema, schema);
    const { name, arg, modifiers, value, iterator } = this.schema;
    this.name = name;
    this.arg = arg;
    this.modifiers = modifiers;
    this.value = value;
    this.iterator = iterator;
  }

  /**
   * 将入参directives数组格式化解析成指令实例
   * @param directives
   * @returns
   */
  static parse(directives: NodeDirective[] = []) {
    return directives.map((value) => new DirectiveModel(value));
  }

  /**
   * 获取DSL
   * @param directives
   * @returns
   */
  static toDsl(directives: DirectiveModel[] = []): NodeDirective[] {
    return directives.map((directive) => {
      const { name, arg, modifiers, value, iterator, id } = directive;
      return {
        id,
        name,
        arg,
        modifiers,
        value,
        iterator
      };
    });
  }
}
