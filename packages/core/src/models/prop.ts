import type { JSONValue, JSExpression, JSFunction, NodeProps } from '../protocols';

/**
 * 属性类模型
 */
export class PropModel {
  /**
   * 用于标识属性是否未设置。设置的值与默认值一致，表示未设置，在 toDsl 会排查该属性
   */
  isUnset = false;

  constructor(
    public name: string,
    public value?: JSONValue | JSExpression | JSFunction,
    public defaultValue?: JSONValue | JSExpression | JSFunction
  ) {
    this.setValue(value);
  }

  /**
   * 用于设置属性的值，并更新 isUnset 属性。如果当前值与默认值一致，isUnset 为 true，否则为 false
   * @param value
   */
  setValue(value: JSONValue | JSExpression | JSFunction) {
    this.value = value;
    this.isUnset = this.value === this.defaultValue;
  }

  /**
   * 返回属性的值。如果属性的值未设置，则返回默认值
   * @returns
   */
  getValue() {
    return this.value ?? this.defaultValue;
  }

  /**
   * 获取DSL，遍历所有属性，如果属性未设置（isUnset 为 false），则将其添加到结果对象中
   * @param props
   * @returns
   */
  static toDsl(props: Record<string, PropModel> = {}): NodeProps {
    return Object.entries(props).reduce((result, [name, prop]) => {
      if (!prop.isUnset) {
        result[name] = prop.getValue();
      }
      return result;
    }, {} as NodeProps);
  }

  /**
   * 将入参props对象格式化解析成属性实例
   * @param props
   * @returns
   */
  static parse(props: NodeProps = {}): Record<string, PropModel> {
    return Object.entries(props).reduce((result, [name, value]) => {
      // TODO: 处理默认值 defaultValue。。。
      result[name] = new PropModel(name, value);
      return result;
    }, {} as Record<string, PropModel>);
  }
}
