import { uid, timestamp, merge, upperFirstCamelCase, delay } from 'cfg-packages-base';
import type {
  ProjectSchema,
  Dependencie,
  PageFile,
  BlockFile,
  ApiSchema,
  MetaSchema,
  ProjectConfig
} from '../protocols';
import { emitter, type ModelEventType } from '../tools';
import { BlockModel } from './block';

export interface ProjectModelEvent {
  model: ProjectModel;
  type: ModelEventType;
  data: any;
}

/**
 * 项目信息更新时触发事件
 */
export const EVENT_PROJECT_CHANGE = 'EVENT_PROJECT_CHANGE';

/**
 * 打开/关闭文件时触发事件
 */
export const EVENT_PROJECT_ACTIVED = 'EVENT_PROJECT_ACTIVED';

/**
 * 依赖更新时触发
 */
export const EVENT_PROJECT_DEPS_CHANGE = 'EVENT_PROJECT_DEPS_CHANGE';

/**
 * 页面文件更新
 */
export const EVENT_PROJECT_PAGES_CHANGE = 'EVENT_PROJECT_PAGES_CHANGE';

/**
 * 区块文件更新
 */
export const EVENT_PROJECT_BLOCKS_CHANGE = 'EVENT_PROJECT_BLOCKS_CHANGE';

/**
 * API更新
 */
export const EVENT_PROJECT_APIS_CHANGE = 'EVENT_PROJECT_APIS_CHANGE';

/**
 * Meta更新
 */
export const EVENT_PROJECT_META_CHANGE = 'EVENT_PROJECT_META_CHANGE';

/**
 * 项目发布
 */
export const EVENT_PROJECT_PUBLISH = 'EVENT_PROJECT_PUBLISH';

/**
 * 项目文件发布
 */
export const EVENT_PROJECT_FILE_PUBLISH = 'EVENT_PROJECT_FILE_PUBLISH';

/**
 * 项目导出
 */
export const EVENT_PROJECT_EXPORT = 'EVENT_PROJECT_EXPORT';

/**
 * 核心-项目类模型
 */
export class ProjectModel {
  id = '';
  name = '';
  description = '';
  creator = '';
  homepage = '';
  dependencies: Dependencie[] = [];
  pages: PageFile[] = [];
  blocks: BlockFile[] = [];
  apis: ApiSchema[] = [];
  meta: MetaSchema[] = [];
  currentFile: PageFile | BlockFile | null = null;
  config: ProjectConfig = {};

  // 项目实例属性列表
  static attrs: string[] = [
    'name',
    'homepage',
    'description',
    'creator',
    'dependencies',
    'pages',
    'blocks',
    'apis',
    'meta',
    'config'
  ];

  constructor(schema: ProjectSchema) {
    const { id } = schema;
    this.id = id || uid();
    this.update(schema, true);
  }

  /**
   * 根据给定的schema，更新项目实例相关属性
   * @param schema
   * @param silent
   */
  update(schema: Partial<ProjectSchema>, silent = false) {
    for (const key of ProjectModel.attrs) {
      const value = schema[key as keyof ProjectSchema];
      if (value) {
        (this as any)[key] = value;
      }
    }
    if (!silent) {
      emitter.emit(EVENT_PROJECT_CHANGE, {
        model: this,
        type: 'update',
        data: schema
      });
    }
  }

  /**
   * 判断给定文件是否页面文件
   * @param file
   * @returns
   */
  isPageFile(file: PageFile | BlockFile): file is PageFile {
    return file.type === 'page';
  }

  /**
   * 获取DSL
   * @returns
   */
  toDsl(version?: string) {
    const { id } = this;
    const attrs = ProjectModel.attrs.reduce((result, current) => {
      result[current] = (this as any)[current];
      return result;
    }, {} as Record<string, any>);
    // 项目存在页面时，删除每个页面DSL（项目DSL不需要页面DSL）
    if (attrs.pages) {
      attrs.pages = attrs.pages.map((n: PageFile) => {
        delete n.dsl;
        return n;
      });
    }
    // 项目存在区块时，删除每个区块DSL（项目DSL不需要区块DSL）
    if (attrs.blocks) {
      attrs.blocks = attrs.blocks.map((n: BlockFile) => {
        delete n.dsl;
        return n;
      });
    }
    return {
      __CFG_PROJECT__: true,
      __VERSION__: version || timestamp().toString(),
      id,
      ...attrs
    } as ProjectSchema;
  }

  /**
   * 打开文件
   * @param file
   * @param silent
   */
  active(file: BlockFile | PageFile, silent = false) {
    this.currentFile = file;
    if (!silent) {
      emitter.emit(EVENT_PROJECT_ACTIVED, {
        model: this,
        type: 'update',
        data: file
      });
    }
  }

  /**
   * 关闭文件
   * @param silent
   */
  deactivate(silent = false) {
    this.currentFile = null;
    if (!silent) {
      emitter.emit(EVENT_PROJECT_ACTIVED, {
        model: this,
        type: 'update',
        data: null
      });
    }
  }

  /**
   * 新增或更新依赖
   * @param item
   * @param silent
   */
  setDeps(item: Dependencie, silent = false) {
    const deps = this.dependencies;
    const index = deps.findIndex((n) => n.package === item.package);
    let type: ModelEventType;
    if (index > -1) {
      type = 'update';
      deps.splice(index, 1, {
        ...deps[index],
        ...item
      });
    } else {
      type = 'create';
      deps.push(item);
    }
    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type,
        data: item
      };
      emitter.emit(EVENT_PROJECT_DEPS_CHANGE, event);
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }
  }

  /**
   * 删除依赖
   * @param item
   * @param silent
   */
  removeDeps(item: Dependencie, silent = false) {
    const deps = this.dependencies;
    const index = deps.findIndex((n) => n.package === item.package);
    if (index > -1) {
      deps.splice(index, 1);
    }
    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type: 'delete',
        data: item
      };
      emitter.emit(EVENT_PROJECT_DEPS_CHANGE, event);
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }
  }

  /**
   * 根据页面id查找页面或目录
   * @param id
   * @returns
   */
  getPage(id: string) {
    const finder = (id: string, pages: PageFile[] = []): PageFile | undefined => {
      for (const page of pages) {
        // id匹配则返回page
        // 存在子项（说明该page属于目录）则遍历子项（子项可能是页面或目录）直到id匹配到page（page可能是页面或目录）为止
        if (page.id === id) {
          return page;
        } else {
          if (page.children && page.children.length) {
            const match = finder(id, page.children);
            if (match) {
              return match;
            }
          }
        }
      }
    };
    return finder(id, this.pages);
  }

  /**
   * 查找全部页面，不含目录
   * @returns
   */
  getPages() {
    const finder = (pages: PageFile[] = []) => {
      let result: PageFile[] = [];
      for (const page of pages) {
        if (page.dir) {
          if (page.children && page.children.length) {
            result = result.concat(finder(page.children));
          }
        } else {
          result.push(page);
        }
      }
      return result;
    };
    return finder(this.pages);
  }

  /**
   * 新建页面
   * @param page
   * @param parentId
   * @param silent
   */
  async createPage(page: PageFile, parentId?: string, silent = false) {
    // 源码文件用name作为文件名
    page.id = page.raw ? page.name : page.id || uid();
    page.type = 'page';
    // 当前需要新增的页面是目录的情况
    if (page.dir) {
      page.children = [];
    } else {
      // 当前需要新增的页面是页面的情况
      page.dsl =
        page.dsl ||
        new BlockModel({
          id: page.id,
          name: upperFirstCamelCase(page.name)
        }).toDsl();
    }
    // 父id存在时，将页面添加到目录的children中
    if (parentId) {
      const dir = this.getPage(parentId);
      if (dir) {
        if (dir.children) {
          dir.children.push(page);
        } else {
          dir.children = [page];
        }
      } else {
        console.warn(`not found PageFile by parent id: ${parentId}`);
      }
    } else {
      this.pages.push(page);
    }

    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type: 'create',
        data: page
      };
      emitter.emit(EVENT_PROJECT_PAGES_CHANGE, event);
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }

    // 没有打开任何文件时，自动打开新建的页面
    if (!this.currentFile && !page.dir) {
      await delay(1000);
      this.active(page, silent);
    }
  }

  /**
   * 更新页面
   * @param page
   * @param silent
   */
  updatePage(page: PageFile, silent = false) {
    const match = this.getPage(page.id);
    if (match) {
      Object.assign(match, page);
    } else {
      console.warn(`not found PageFile by id: ${page.id}`);
    }
    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type: 'update',
        data: page
      };
      emitter.emit(EVENT_PROJECT_PAGES_CHANGE, event);
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }
  }

  /**
   * 复制页面
   * @param page
   * @param parentId
   * @param silent
   */
  clonePage(page: PageFile, parentId?: string, silent = false) {
    const id = uid();
    const name = `${page.name}Copy`;
    const title = `${page.title}_副本`;

    const dsl = new BlockModel({
      id,
      name
    }).toDsl();
    const newPage = merge({}, page, { id, name, title, dsl });
    const pages = parentId ? this.getPage(parentId)?.children || [] : this.pages;
    const index = pages.findIndex((n) => n.id === page.id);
    // 复制的页面插入到原页面之后
    pages.splice(index + 1, 0, newPage);

    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type: 'clone',
        data: {
          page,
          newPage
        }
      };
      emitter.emit(EVENT_PROJECT_PAGES_CHANGE, event);
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }
  }

  /**
   * 删除页面或目录
   * @param id
   * @param silent
   */
  removePage(id: string, silent = false) {
    const pageFile = this.getPage(id);

    const remover = (id: string, pages: PageFile[]): void => {
      const index = pages.findIndex((n) => n.id === id);
      if (index >= 0) {
        pages.splice(index, 1);
        return;
      }
      for (const page of pages) {
        if (page.children && page.children.length) {
          return remover(id, page.children);
        }
      }
    };
    remover(id, this.pages);

    if (id === this.homepage) {
      this.homepage = '';
    }
    if (this.currentFile?.id === id) {
      this.deactivate(silent);
    }

    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type: 'delete',
        data: pageFile
      };
      emitter.emit(EVENT_PROJECT_PAGES_CHANGE, event);
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }
  }

  /**
   * 获取区块文件
   * @param id
   * @returns
   */
  getBlock(id: string) {
    return this.blocks.find((n) => n.id === id);
  }

  /**
   * 创建区块
   * @param block
   * @param silent
   */
  async createBlock(block: BlockFile, silent = false) {
    const id = block.id || uid();
    const name = upperFirstCamelCase(block.name);
    block.id = id;
    block.type = 'block';
    block.dsl = new BlockModel({ id, name }).toDsl();
    this.blocks.push(block);
    const type = block.fromType || 'Schema';

    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type: 'create',
        data: block
      };
      emitter.emit(EVENT_PROJECT_BLOCKS_CHANGE, event);
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }

    if (!this.currentFile && type === 'Schema') {
      await delay(1000);
      this.active(block, silent);
    }
  }

  /**
   * 更新区块
   * @param block
   * @param silent
   */
  updateBlock(block: BlockFile, silent = false) {
    const match = this.getBlock(block.id);
    if (match) {
      Object.assign(match, block);
      if (match.dsl) {
        match.dsl.name = block.name;
      }
    } else {
      console.warn(`not found BlockFile by id: ${block.id}`);
    }

    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type: 'update',
        data: block
      };
      emitter.emit(EVENT_PROJECT_BLOCKS_CHANGE, event);
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }
  }

  /**
   * 删除区块
   * @param id
   * @param silent
   */
  removeBlock(id: string, silent = false) {
    const blockFile = this.getBlock(id);
    const blocks = this.blocks;
    const index = blocks.findIndex((n) => n.id === id);
    if (index > -1) {
      blocks.splice(index, 1);
      if (this.currentFile?.id === id) {
        this.deactivate(silent);
      }
    } else {
      console.warn(`not found BlockFile by id: ${id}`);
    }

    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type: 'delete',
        data: blockFile
      };
      emitter.emit(EVENT_PROJECT_BLOCKS_CHANGE, event);
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }
  }

  /**
   * 给定区块名称，检查区块列表中是否存在该名称的区块
   * @param name
   * @param excludes
   * @returns
   */
  existBlockName(name: string, excludes: string[] = []) {
    return this.blocks.some((n) => {
      return n.name === name && !excludes.includes(n.id);
    });
  }

  /**
   * 给定页面名称，检查页面列表中是否存在该名称的页面
   * @param name
   * @param excludes
   * @returns
   */
  existPageName(name: string, excludes: string[] = []) {
    const pages = this.getPages();
    return pages.some((n) => n.name === name && !excludes.includes(n.id));
  }

  /**
   * 新增或更新api
   * @param item
   * @param silent
   */
  setApi(item: ApiSchema, silent = false) {
    const match = this.apis.find((n) => n.name === item.name || n.id === item.id);
    let type: ModelEventType;
    if (match) {
      type = 'update';
      Object.assign(match, item);
    } else {
      type = 'create';
      item.id = uid();
      this.apis.push(item);
    }

    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type,
        data: item
      };
      emitter.emit(EVENT_PROJECT_APIS_CHANGE, event);
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }
  }

  /**
   * 删除api
   * @param name
   * @param silent
   */
  removeApi(name: string, silent = false) {
    const index = this.apis.findIndex((n) => n.name === name || n.id === name);
    if (index > -1) {
      this.apis.splice(index, 1);
    } else {
      console.warn(`not found Api by name: ${name}`);
    }

    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type: 'delete',
        data: name
      };
      emitter.emit(EVENT_PROJECT_APIS_CHANGE, event);
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }
  }

  /**
   * 给定api名称，检查api列表中是否存在该名称的api
   * @param name
   * @param excludes
   * @returns
   */
  existApiName(name: string, excludes: string[] = []) {
    return this.apis.some((n) => n.name === name && !excludes.includes(n.id));
  }

  /**
   * 给定item来设置meta，有则更新，无则创建
   * @param item
   * @param silent
   */
  setMeta(item: MetaSchema, silent = false) {
    const match = this.meta.find((n) => n.code === item.code || n.id === item.id);
    let type: ModelEventType;
    if (match) {
      type = 'update';
      Object.assign(match, item);
    } else {
      type = 'create';
      item.id = uid();
      this.meta.push(item);
    }

    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type,
        data: item
      };
      emitter.emit(EVENT_PROJECT_META_CHANGE, event);
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }
  }

  /**
   * 给定meta code或者id，从meta数组中删除该meta
   * @param code
   * @param silent
   */
  removeMeta(code: string, silent = false) {
    const index = this.meta.findIndex((n) => n.code === code || n.id === code);
    if (index > -1) {
      this.meta.splice(index, 1);
    } else {
      console.warn(`not found meta by code or id: ${code}`);
    }

    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type: 'delete',
        data: code
      };
      emitter.emit(EVENT_PROJECT_META_CHANGE, event);
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }
  }

  /**
   * 给定meta code或者id，检查meta列表中是否存在该名称的meta
   * @param code
   * @param excludes
   * @returns
   */
  existMetaCode(code: string, excludes: string[] = []) {
    return this.meta.some((n) => n.code === code && !excludes.includes(n.id));
  }

  /**
   * 设置给定id的页面为主页
   * @param id
   * @param silent
   */
  setHomepage(id: string, silent = false) {
    this.homepage = id;
    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type: 'update',
        data: id
      };
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }
  }

  /**
   * 更新项目配置
   * @param config
   * @param silent
   */
  setConfig(config: ProjectConfig, silent = false) {
    this.config = Object.assign(this.config, config);
    if (!silent) {
      const event: ProjectModelEvent = {
        model: this,
        type: 'update',
        data: config
      };
      emitter.emit(EVENT_PROJECT_CHANGE, event);
    }
  }

  /**
   * 若存在给定file文件，则发布该文件；否则发布全站
   * @param file
   */
  publish(file?: PageFile | BlockFile) {
    const event: ProjectModelEvent = {
      model: this,
      type: 'publish',
      data: file || this
    };
    if (file) {
      emitter.emit(EVENT_PROJECT_FILE_PUBLISH, event);
    } else {
      emitter.emit(EVENT_PROJECT_PUBLISH, event);
    }
  }

  /**
   * 导出项目(项目模块，包括：.vue页面/区块文件、路由、API等，可直接运行看效果)
   */
  exportPrj() {
    const event: ProjectModelEvent = {
      model: this,
      type: 'export',
      data: this
    };
    emitter.emit(EVENT_PROJECT_EXPORT, event);
  }
}
