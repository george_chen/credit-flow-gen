import { type DefineComponent } from 'vue';

/**
 * 区块插件类型定义
 */
export interface BlockPlugin {
  component: DefineComponent<any, any, any, any> | Record<string, any>;
  css: string[];
}
