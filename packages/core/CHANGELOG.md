# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.57](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.56...cfg-packages-core@0.0.57) (2024-10-23)

**Note:** Version bump only for package cfg-packages-core

## [0.0.56](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.55...cfg-packages-core@0.0.56) (2024-10-12)

**Note:** Version bump only for package cfg-packages-core

## [0.0.55](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.54...cfg-packages-core@0.0.55) (2024-10-12)

**Note:** Version bump only for package cfg-packages-core

## [0.0.54](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.53...cfg-packages-core@0.0.54) (2024-10-12)

**Note:** Version bump only for package cfg-packages-core

## [0.0.53](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.52...cfg-packages-core@0.0.53) (2024-10-12)

**Note:** Version bump only for package cfg-packages-core

## [0.0.52](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.51...cfg-packages-core@0.0.52) (2024-10-12)

**Note:** Version bump only for package cfg-packages-core

## [0.0.51](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.50...cfg-packages-core@0.0.51) (2024-10-11)

**Note:** Version bump only for package cfg-packages-core

## [0.0.50](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.49...cfg-packages-core@0.0.50) (2024-10-11)

**Note:** Version bump only for package cfg-packages-core

## [0.0.49](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.48...cfg-packages-core@0.0.49) (2024-10-10)

### Features

- 每一个项目增加创建人字段；项目列表支持关键字搜索；样式调整 ([cdaa650](https://gitee.com/george_chen/credit-flow-gen/commits/cdaa6501fddc77d67ab7a8d18962d21503c4cc7a))

## [0.0.48](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.47...cfg-packages-core@0.0.48) (2024-09-13)

**Note:** Version bump only for package cfg-packages-core

## [0.0.47](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.46...cfg-packages-core@0.0.47) (2024-09-13)

**Note:** Version bump only for package cfg-packages-core

## [0.0.46](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.45...cfg-packages-core@0.0.46) (2024-09-13)

**Note:** Version bump only for package cfg-packages-core

## [0.0.45](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.44...cfg-packages-core@0.0.45) (2024-09-13)

**Note:** Version bump only for package cfg-packages-core

## [0.0.44](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.43...cfg-packages-core@0.0.44) (2024-09-10)

**Note:** Version bump only for package cfg-packages-core

## [0.0.43](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.42...cfg-packages-core@0.0.43) (2024-09-01)

**Note:** Version bump only for package cfg-packages-core

## [0.0.42](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.41...cfg-packages-core@0.0.42) (2024-08-30)

**Note:** Version bump only for package cfg-packages-core

## [0.0.41](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.40...cfg-packages-core@0.0.41) (2024-08-30)

**Note:** Version bump only for package cfg-packages-core

## [0.0.40](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.39...cfg-packages-core@0.0.40) (2024-08-29)

**Note:** Version bump only for package cfg-packages-core

## [0.0.39](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.38...cfg-packages-core@0.0.39) (2024-08-22)

**Note:** Version bump only for package cfg-packages-core

## [0.0.38](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.37...cfg-packages-core@0.0.38) (2024-08-21)

**Note:** Version bump only for package cfg-packages-core

## [0.0.37](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.36...cfg-packages-core@0.0.37) (2024-08-20)

**Note:** Version bump only for package cfg-packages-core

## [0.0.36](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.35...cfg-packages-core@0.0.36) (2024-08-20)

**Note:** Version bump only for package cfg-packages-core

## [0.0.35](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.34...cfg-packages-core@0.0.35) (2024-08-20)

**Note:** Version bump only for package cfg-packages-core

## [0.0.34](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.33...cfg-packages-core@0.0.34) (2024-08-20)

**Note:** Version bump only for package cfg-packages-core

## [0.0.33](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.32...cfg-packages-core@0.0.33) (2024-08-20)

**Note:** Version bump only for package cfg-packages-core

## [0.0.32](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.31...cfg-packages-core@0.0.32) (2024-08-12)

**Note:** Version bump only for package cfg-packages-core

## [0.0.31](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.30...cfg-packages-core@0.0.31) (2024-08-12)

**Note:** Version bump only for package cfg-packages-core

## [0.0.30](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.29...cfg-packages-core@0.0.30) (2024-08-12)

**Note:** Version bump only for package cfg-packages-core

## [0.0.29](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.28...cfg-packages-core@0.0.29) (2024-08-11)

**Note:** Version bump only for package cfg-packages-core

## [0.0.28](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.27...cfg-packages-core@0.0.28) (2024-08-11)

**Note:** Version bump only for package cfg-packages-core

## [0.0.27](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.26...cfg-packages-core@0.0.27) (2024-08-11)

**Note:** Version bump only for package cfg-packages-core

## [0.0.26](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.25...cfg-packages-core@0.0.26) (2024-08-11)

### Features

- 开发完成，待发版 ([dfb56fd](https://gitee.com/george_chen/credit-flow-gen/commits/dfb56fdf7a7b478ccb1227b7ea9fe3b416e9b402))

## [0.0.25](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.24...cfg-packages-core@0.0.25) (2024-08-11)

**Note:** Version bump only for package cfg-packages-core

## [0.0.24](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.23...cfg-packages-core@0.0.24) (2024-08-09)

**Note:** Version bump only for package cfg-packages-core

## [0.0.23](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.22...cfg-packages-core@0.0.23) (2024-08-09)

**Note:** Version bump only for package cfg-packages-core

## [0.0.22](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.21...cfg-packages-core@0.0.22) (2024-08-09)

**Note:** Version bump only for package cfg-packages-core

## [0.0.21](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.18...cfg-packages-core@0.0.21) (2024-08-09)

### Features

- 项目导出完善完成，准备发版 ([d744af6](https://gitee.com/george_chen/credit-flow-gen/commits/d744af60ea8cec90f266a0cc25c5634c41adbf0b))
- 项目导出完善完成，准备发版 ([66a970e](https://gitee.com/george_chen/credit-flow-gen/commits/66a970e777f06024a270c9be41aa6c26a165535f))
- 项目列表页，新增/编辑项目开发中 ([963b466](https://gitee.com/george_chen/credit-flow-gen/commits/963b466c02609acc7cc932d9e0d6fe37857a108b))
- 项目列表页及项目切换相关功能完成开发 ([b6da9e8](https://gitee.com/george_chen/credit-flow-gen/commits/b6da9e85328869fa1931a5465f5656644daccb04))

## [0.0.20](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.18...cfg-packages-core@0.0.20) (2024-08-09)

### Features

- 项目导出完善完成，准备发版 ([66a970e](https://gitee.com/george_chen/credit-flow-gen/commits/66a970e777f06024a270c9be41aa6c26a165535f))
- 项目列表页，新增/编辑项目开发中 ([963b466](https://gitee.com/george_chen/credit-flow-gen/commits/963b466c02609acc7cc932d9e0d6fe37857a108b))
- 项目列表页及项目切换相关功能完成开发 ([b6da9e8](https://gitee.com/george_chen/credit-flow-gen/commits/b6da9e85328869fa1931a5465f5656644daccb04))

## [0.0.19](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.18...cfg-packages-core@0.0.19) (2024-08-09)

### Features

- 项目列表页，新增/编辑项目开发中 ([963b466](https://gitee.com/george_chen/credit-flow-gen/commits/963b466c02609acc7cc932d9e0d6fe37857a108b))
- 项目列表页及项目切换相关功能完成开发 ([b6da9e8](https://gitee.com/george_chen/credit-flow-gen/commits/b6da9e85328869fa1931a5465f5656644daccb04))

## [0.0.18](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.17...cfg-packages-core@0.0.18) (2024-07-23)

**Note:** Version bump only for package cfg-packages-core

## [0.0.17](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.16...cfg-packages-core@0.0.17) (2024-07-23)

**Note:** Version bump only for package cfg-packages-core

## [0.0.16](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.15...cfg-packages-core@0.0.16) (2024-07-23)

**Note:** Version bump only for package cfg-packages-core

## [0.0.15](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.14...cfg-packages-core@0.0.15) (2024-07-22)

### Features

- 项目列表页，源码导出功能 开发中 ([3c4266b](https://gitee.com/george_chen/credit-flow-gen/commits/3c4266b8f454912752cbdf3959e2e66619e0dae7))
- 源码导出功能 开发基本完成 ([f625c94](https://gitee.com/george_chen/credit-flow-gen/commits/f625c949d25a0f1c162790397d7fd0668e020b4f))

## [0.0.14](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.13...cfg-packages-core@0.0.14) (2024-07-08)

**Note:** Version bump only for package cfg-packages-core

## [0.0.13](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.12...cfg-packages-core@0.0.13) (2024-07-06)

**Note:** Version bump only for package cfg-packages-core

## [0.0.12](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.11...cfg-packages-core@0.0.12) (2024-07-01)

**Note:** Version bump only for package cfg-packages-core

## [0.0.11](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.10...cfg-packages-core@0.0.11) (2024-07-01)

### Features

- cfg-packages-designer 模块开发完成 ([107bfcf](https://gitee.com/george_chen/credit-flow-gen/commits/107bfcfd30cd8aef651f67cf94d9f918cea3c1e9))

## [0.0.10](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.9...cfg-packages-core@0.0.10) (2024-06-25)

### Features

- 更新 cfg-packages-renderer 完善注释 ([b6d7cb5](https://gitee.com/george_chen/credit-flow-gen/commits/b6d7cb5efebf63f172c8ffdd5e14633029bcca15))

## [0.0.9](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.8...cfg-packages-core@0.0.9) (2024-06-14)

**Note:** Version bump only for package cfg-packages-core

## [0.0.8](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.7...cfg-packages-core@0.0.8) (2024-06-13)

**Note:** Version bump only for package cfg-packages-core

## [0.0.7](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.6...cfg-packages-core@0.0.7) (2024-06-03)

**Note:** Version bump only for package cfg-packages-core

## [0.0.6](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.5...cfg-packages-core@0.0.6) (2024-06-03)

### Features

- 完成 cfg-packages-local npm 包开发 ([154762d](https://gitee.com/george_chen/credit-flow-gen/commits/154762dce238724b37b670dbee9ba8719e26b6af))

## [0.0.5](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.4...cfg-packages-core@0.0.5) (2024-06-02)

### Features

- 更新 prettier 相关依赖安装包；完成 cfg-packages-coder 开发；cfg-packages-local 开发中 ([58b5531](https://gitee.com/george_chen/credit-flow-gen/commits/58b5531e8b800f029ac390e81d741e028ffccabd))

## [0.0.4](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.3...cfg-packages-core@0.0.4) (2024-05-31)

### Features

- cfg-packages-utils 包开发中，需要加入部分方法 ([c9b80b4](https://gitee.com/george_chen/credit-flow-gen/commits/c9b80b469bc23624e05a74ea6576c328e7c12907))

## [0.0.3](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.2...cfg-packages-core@0.0.3) (2024-05-31)

### Features

- 完成 cfg-packages-icons 开发 ([57a310b](https://gitee.com/george_chen/credit-flow-gen/commits/57a310b6e24601adc1696b94f602bdc28213f8e5))

## [0.0.2](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-core@0.0.1...cfg-packages-core@0.0.2) (2024-05-28)

### Features

- update cfg-packages-core version.ts ([b46b244](https://gitee.com/george_chen/credit-flow-gen/commits/b46b24461c42d153107ef055f6f7945dcf47545d))

## 0.0.1 (2024-05-28)

### Features

- 提取项目公共 npm 包到根目录下的 package.json；完成 cfg-packages-core 模块的开发 ([b64dc5f](https://gitee.com/george_chen/credit-flow-gen/commits/b64dc5f86d396d0c14cc7423b15f22c0816415fe))
