import { createViteConfig } from 'cfg-packages-cli';

export default createViteConfig({
  lib: true,
  dts: true,
  version: true,
  formats: ['es', 'cjs'],
  external: ['cfg-packages-base']
});
