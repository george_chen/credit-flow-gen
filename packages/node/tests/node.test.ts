import { expect, test } from 'vitest';
import { isUrl } from '../dist/index.mjs';

test('isUrl', () => {
  const url = 'https://baidu.com';
  expect(isUrl(url)).toBe(true);
});
