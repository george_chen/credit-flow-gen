# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.8](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-node@0.0.7...cfg-packages-node@0.0.8) (2024-10-23)

**Note:** Version bump only for package cfg-packages-node

## [0.0.7](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-node@0.0.6...cfg-packages-node@0.0.7) (2024-08-22)

**Note:** Version bump only for package cfg-packages-node

## [0.0.6](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-node@0.0.5...cfg-packages-node@0.0.6) (2024-06-13)

**Note:** Version bump only for package cfg-packages-node

## [0.0.5](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-node@0.0.4...cfg-packages-node@0.0.5) (2024-06-03)

**Note:** Version bump only for package cfg-packages-node

## [0.0.4](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-node@0.0.3...cfg-packages-node@0.0.4) (2024-05-31)

**Note:** Version bump only for package cfg-packages-node

## [0.0.3](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-node@0.0.2...cfg-packages-node@0.0.3) (2024-05-31)

### Features

- 完成 cfg-packages-icons 开发 ([57a310b](https://gitee.com/george_chen/credit-flow-gen/commits/57a310b6e24601adc1696b94f602bdc28213f8e5))

## [0.0.2](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-node@0.0.1...cfg-packages-node@0.0.2) (2024-05-28)

### Features

- 更改 pkg.json author ([648d4ec](https://gitee.com/george_chen/credit-flow-gen/commits/648d4ec13abd7554140512e49f7c117d603f7201))
- 提取项目公共 npm 包到根目录下的 package.json；完成 cfg-packages-core 模块的开发 ([b64dc5f](https://gitee.com/george_chen/credit-flow-gen/commits/b64dc5f86d396d0c14cc7423b15f22c0816415fe))

## 0.0.1 (2024-05-18)

### Features

- 完成 cfg-packages-node 适配 Node 环境-工具包模块开发 ([5b067da](https://gitee.com/george_chen/credit-flow-gen/commits/5b067daa9ed7c770b1ab5cf6354e0bbbb5b9df97))
- 完成 cfg-packages-node 适配 Node 环境-工具包模块开发 ([a320df5](https://gitee.com/george_chen/credit-flow-gen/commits/a320df5ec92acdd18d4ff2b778ec6bf5b2693201))
- 完成 cfg-packages-node 适配 Node 环境-工具包模块开发 ([abaaa93](https://gitee.com/george_chen/credit-flow-gen/commits/abaaa9387e8aaeaf4d4d9c88fd32e695db62728b))
- 完成 cfg-packages-node 适配 Node 环境-工具包模块开发 ([67b80ea](https://gitee.com/george_chen/credit-flow-gen/commits/67b80ea7d225a99ce0e58b2706eaf5c0816e4a40))
- cfg-packages-cli 包开发中 ([cd1abc7](https://gitee.com/george_chen/credit-flow-gen/commits/cd1abc738f9cbbade1fd37268518c75885ce9a93))
