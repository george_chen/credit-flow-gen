import { removeSync } from 'cfg-packages-node';
import { resolve } from 'path';

export function rm(path: string) {
  const target = resolve(path);
  try {
    removeSync(target);
  } catch (e) {
    console.error('rm error', e);
  }
}
