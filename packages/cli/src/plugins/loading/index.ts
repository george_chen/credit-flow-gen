import { type Plugin } from 'vite';

// 定义加载进度条的外观和位置
const style = `
<style>
.dark {
  background-color: #141414;
}
#cfg-ide-loading {
  width: 400px;
  height: 100px;
  position: fixed;
  left:50%;
  top: 50%;
  transform: translate(-50%,-50%);
  z-index: 9999;
  border-radius: 4px;
  background-color: rgba(255,255,255,0.1);
  box-shadow: 0 0 10px rgba(0,0,0,0.2);
  padding: 20px;
  font-size: 12px;
  box-sizing: border-box;
}
.cfg-ide-loading__bar {
  height: 14px;
  border-radius: 4px;
  margin-top: 10px;
  border: 1px solid #eee;
  overflow: hidden;
}
.cfg-ide-loading__title {
  display: flex;
  justify-content: space-between;
}
.dark .cfg-ide-loading__title {
  color:#fff;
}
.cfg-ide-loading__value {
  display: block;
  height: 100%;
  background-color: #409eff;
  border-radius: 4px;
  font-size: 0;
}
</style>
`;

// 加载进度条的整体布局，包括一个进度条容器和显示加载状态的文本
const mask = `
<div id="cfg-ide-loading">
<div class="cfg-ide-loading__title"><span>正在加载资源.... </span><span id="cfg-ide-loading-count"></span></div>
<div class="cfg-ide-loading__bar">
  <span class="cfg-ide-loading__value" id="cfg-ide-loading-value" style="width:0%"></span>
</div>
</div>
`;

// 加载进度条的逻辑，包括：计算总资源数。更新加载进度。移除加载进度条
const script = `
<script>
(function(){
  var loading = document.querySelector('#cfg-ide-loading');
  var countEl = document.querySelector('#cfg-ide-loading-count');
  var valueEl = document.querySelector('#cfg-ide-loading-value');
  var links = document.querySelectorAll('link,script');
  var total = links.length;
  var setValue = function(current) {
    countEl.innerHTML = current + '/' +total;
    valueEl.style.width = (current * 100 / total) + '%';
    if(!total || current === total) {
      setTimeout(function() {
         loading.parentNode.removeChild(loading);
         loading = null;
         countEl = null;
         valueEl = null;
      }, 100);
    }
  }

  var current = 0;
  links.forEach(function(link) {
    link.onload = function() {
        ++current;
        setValue(current);
        link.onload = null;
    }
  })
  window.addEventListener('load',function() {
    current = total
    setValue(current)
  });

})()

</script> 
`;

/**
 * 在网页加载时显示一个加载进度条。该插件主要应用于 Vite 构建工具中，
 * 通过修改 HTML 文件来插入自定义的 CSS 样式和 JavaScript 脚本，从而实现加载进度的显示。
 * 该插件适用于需要显示资源加载进度提示的场景，如单页面应用程序（SPA）加载大量资源时，可以提升用户体验。
 * @returns 插件对象
 */
export function loadingPlugin(): Plugin {
  return {
    name: 'cfg-loading-plugin',
    transformIndexHtml(html: string) {
      return html
        .replace('</head>', `${style}</head>`)
        .replace('<body>', `<body>${mask}`)
        .replace('</body>', `${script}</body>`);
    }
  };
}
