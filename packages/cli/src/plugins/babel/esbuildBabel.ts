import babel, { type TransformOptions } from '@babel/core';
import type { Loader, Plugin, OnLoadArgs, OnLoadResult } from 'esbuild';
import fs from 'fs';
import path from 'path';

/**
 * Original: https://github.com/nativew/esbuild-plugin-babel
 * Copied, because there was a problem with `type: "module"` in `package.json`
 */
export interface ESBuildPluginBabelOptions {
  config?: TransformOptions;
  filter?: RegExp;
  namespace?: string;
  loader?: Loader | ((path: string) => Loader);
}
/**
 * esbuildPluginBabel 插件定义，用于在 esbuild 构建过程中集成 Babel 以处理和转换 JavaScript 文件
 * @param options 插件选项
 * @returns
 */
export const esbuildPluginBabel = (options: ESBuildPluginBabelOptions = {}): Plugin => ({
  name: 'babel',
  // 执行插件
  setup(build: any) {
    // filter: 正则表达式，用于筛选需要转换的文件，默认为匹配所有文件
    // namespace: 命名空间，用于筛选特定命名空间中的文件，默认为空字符串
    // config: Babel 的配置选项，默认空配置
    // loader: 文件加载器，可以是一个函数或者一个静态值
    const { filter = /.*/, namespace = '', config = {}, loader } = options;
    // 根据 loader 参数的类型（函数或静态值）确定文件加载器
    // 如果 loader 是函数，则调用函数并传入文件路径参数，否则直接返回 loader
    const resolveLoader = (args: OnLoadArgs): Loader | undefined => {
      if (typeof loader === 'function') {
        return loader(args.path);
      }
      return loader;
    };
    // 异步函数，通过 Babel 转换文件内容
    const transformContents = async (args: OnLoadArgs, contents: string): Promise<OnLoadResult> => {
      // 加载 Babel 配置，并添加 caller 信息
      const babelOptions = babel.loadOptions({
        filename: args.path,
        ...config,
        caller: {
          name: 'esbuild-plugin-babel',
          supportsStaticESM: true
        }
      }) as TransformOptions;
      // 检查 Babel 配置是否生成 source map，如果是，设置 sourceFileName
      if (!babelOptions) {
        return { contents, loader: resolveLoader(args) };
      }
      if (babelOptions.sourceMaps) {
        babelOptions.sourceFileName = path.relative(process.cwd(), args.path);
      }
      // 使用 Babel 同步转换代码，并返回转换后的内容和 loader
      return new Promise((resolve, reject) => {
        babel.transform(contents, babelOptions, (error, result) => {
          error
            ? reject(error)
            : resolve({
                contents: result?.code ?? '',
                loader: resolveLoader(args)
              });
        });
      });
    };
    // 注册 onLoad 钩子，当匹配 filter 和 namespace 的文件被加载时调用
    build.onLoad({ filter, namespace }, async (args: any) => {
      // 读取文件内容，并调用 transformContents 进行转换，返回转换后的结果
      const contents = await fs.promises.readFile(args.path, 'utf8');
      return transformContents(args, contents);
    });
  }
});
