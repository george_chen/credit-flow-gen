import babel, { type TransformOptions } from '@babel/core';
import { type Loader } from 'esbuild';
import { type Plugin } from 'vite';

import { esbuildPluginBabel } from './esbuildBabel';

export interface BabelPluginOptions {
  apply?: 'serve' | 'build';
  babelConfig?: TransformOptions;
  filter?: RegExp;
  loader?: Loader | ((path: string) => Loader);
}

const DEFAULT_FILTER = /\.jsx?$/;

/**
 * Babel 插件的定义，用于集成到一个打包工具（比如 Vite 或 Rollup）中，
 * 以便在构建过程中对代码进行转换。这个插件使用 Babel 来处理 JavaScript 文件，
 * 按照指定的配置和过滤条件来决定是否对文件进行转换。
 * @param babelConfig Babel 的配置选项
 * @param filter 正则表达式或过滤函数，用于筛选需要转换的文件，默认值DEFAULT_FILTER
 * @param apply  插件的应用时机，可以是 serve 或 build
 * @param loader 用于指定 esbuild 的 loader，可以是一个字符串或函数
 * @returns 插件对象
 */
export const babelPlugin = ({
  babelConfig = {},
  filter = DEFAULT_FILTER,
  apply,
  loader
}: BabelPluginOptions = {}): Plugin => {
  return {
    name: 'babel-plugin',
    apply,
    // 插件的执行顺序，这里设置为'pre'，表示这个插件在其他插件之前执行
    enforce: 'pre',
    config() {
      // 配置打包工具的选项，特别是 esbuild 的配置。
      // 这里通过 esbuildPluginBabel插件 集成 Babel 处理代码
      return {
        optimizeDeps: {
          esbuildOptions: {
            plugins: [
              esbuildPluginBabel({
                config: { ...babelConfig },
                filter,
                loader
              })
            ]
          }
        }
      } as any;
    },
    // 该方法在每个文件被处理时调用，用于对文件进行转换
    // code: 文件的内容；id: 文件的路径
    transform(code, id) {
      const shouldTransform = filter.test(id);
      if (!shouldTransform) {
        return;
      }
      const { code: output, map } =
        babel.transformSync(code, {
          filename: id,
          ...babelConfig
        }) ?? {};
      // 返回转换后的代码和 source map（如果存在）
      return {
        code: output ?? '',
        map
      };
    }
  };
};

export * from './esbuildBabel';
