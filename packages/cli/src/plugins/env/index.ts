import { type Plugin } from 'vite';
import { resolve } from 'path';
import { pathExistsSync, readJsonSync } from 'cfg-packages-node';
import { type EnvType, type EnvConfig } from '../../vite/types';

// 从指定envPath路径读取env.json文件(如有)和env.${type}.json文件(如有) 的配置内容
function getConfig(envPath: string, type: string): Record<string, any> {
  const defaults = resolve(envPath, 'env.json');
  const env = resolve(envPath, `env.${type}.json`);
  const defaultConfig = pathExistsSync(defaults) ? readJsonSync(defaults) : {};
  const envConfig = pathExistsSync(env) ? readJsonSync(env) : {};
  return Object.assign({}, defaultConfig, envConfig);
}

// 环境类型和配置转换为全局变量定义，返回的 define 对象包含这些定义
const createEnv = (type: string, envConfig: EnvConfig) => {
  return {
    'process.env': {
      ENV_TYPE: type,
      NODE_ENV: process.env.NODE_ENV,
      ...envConfig
    }
  };
};

export interface EnvPluginOptions {
  dir?: string;
}

/**
 * 构建过程中根据环境类型加载相应的配置文件，并将配置中的变量定义为全局常量，使其可以在代码中访问
 * @param options 插件选项，默认为空对象，选项属性如下：
 * dir: 环境配置文件所在的目录，默认值为当前目录 './'
 * @returns 插件对象，其中包含以下属性：
 * name: 插件的名称
 * config: 构建工具调用的配置钩子，返回一个包含环境变量定义的配置对象
 */
export function envPlugin(options: EnvPluginOptions = {}): Plugin {
  const { dir = './' } = options;
  return {
    name: 'cfg-env-plugin',
    config: () => {
      // 环境配置标识
      const envType: EnvType = (process.env.ENV_TYPE || 'local') as EnvType;
      // 读取相应环境标识下的配置内容
      const envConfig = getConfig(dir, envType);
      // 定义process.env全局变量，包含环境配置内容
      const define = createEnv(envType, envConfig);
      // 构建工具将定义注入到全局作用域中
      return {
        define
      };
    }
  };
}
