import { type Plugin } from 'vite';
import { resolve } from 'path';
import serveStatic from 'serve-static';

export interface StaticPluginOption {
  path: string;
  dir: string;
}

/**
 * 在 Vite 开发服务器和预览服务器中提供静态文件服务。该插件允许配置多个静态文件路径，
 * 并将这些路径映射到本地目录，从而使静态文件在开发和预览过程中可以被访问。
 * 适用于需要在开发和预览过程中提供静态文件（如图像、样式表、脚本文件等）的场景，
 * 通过简单配置即可实现文件的托管和访问。
 * @param options 插件选项，可以是字符串数组或 StaticPluginOption 对象数组
 * @returns 插件对象
 */
export function staticPlugin(options: Array<string | StaticPluginOption> = []): Plugin {
  const opts = options.map((item) => {
    return typeof item === 'string' ? { path: '/', dir: item } : item;
  });
  return {
    name: 'cfg-static-server',
    // 配置vite开发服务器的中间件，用于提供静态文件服务
    configureServer(server) {
      for (const option of opts) {
        server.middlewares.use(option.path, serveStatic(resolve(option.dir)));
      }
    },
    // 配置vite预览服务器的中间件，用于提供静态文件服务
    configurePreviewServer(server) {
      for (const option of opts) {
        server.middlewares.use(option.path, serveStatic(resolve(option.dir)));
      }
    }
  };
}
