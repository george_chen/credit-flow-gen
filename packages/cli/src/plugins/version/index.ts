import { writeFileSync, readJsonSync } from 'cfg-packages-node';
import { resolve } from 'path';

// 版本号写入文件
function writeVersion(file: string) {
  const pkg = readJsonSync(resolve('package.json'));
  const date = new Date();
  const year = date.getFullYear();
  const banner = `/**!
 * Copyright (c) ${year}, credit-flow-gen CFG All rights reserved.
 * @name ${pkg.name}
 * @version ${pkg.version}
 */\n`;
  const code = `export const version = '${pkg.version}';`;
  const content = `${banner}${code}\n`;
  const toPath = resolve(file);
  writeFileSync(toPath, content, 'utf-8');
}

/**
 * packages下的每个包版本号写入插件
 * @param output 版本号文件路径
 * @returns 插件对象
 */
export function versionPlugin(output = 'src/version.ts') {
  return {
    name: 'write-version',
    buildStart() {
      writeVersion(output);
    }
  };
}
