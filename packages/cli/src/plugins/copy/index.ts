import { type Plugin, type ResolvedConfig } from 'vite';
import { copySync, emptyDirSync } from 'cfg-packages-node';
import { resolve } from 'path';

export interface CopyPluginOption {
  from: string;
  to: string;
  emptyDir?: boolean;
}
/**
 * 在构建过程中复制文件或目录。这个插件可以集成到构建工具（如 Vite 或 Rollup）中，
 * 在构建结束时将指定的文件或目录复制到输出目录。
 * @param options 复制文件或目录的配置选项，选项属性如下：
 * from: 源文件或目录路径。
 * to: 目标文件或目录路径（相对于输出目录）。
 * emptyDir: 可选布尔值，表示是否在复制前清空目标目录，默认为 false。
 * 示例如下：
 * const options = [
 *   { from: 'src/assets', to: 'dist/assets', emptyDir: true },
 *   { from: 'public/config.json', to: 'dist/config.json' }
 * ];
 * @returns 插件对象
 */
export const copyPlugin = function (options: CopyPluginOption[] = []): Plugin {
  let config: ResolvedConfig;
  return {
    name: 'cfg-copy-plugin',
    apply: 'build',
    // 构建工具解析配置后调用
    configResolved(resolvedConfig: ResolvedConfig) {
      config = resolvedConfig;
    },
    // 在构建完成后（即关闭打包时的调用钩子）调用
    closeBundle() {
      const outDir = config.build.outDir;
      for (const { from, to, emptyDir = false } of options) {
        const toDir = to.startsWith('/') ? to.substring(1) : to;
        if (emptyDir) {
          emptyDirSync(resolve(outDir, toDir));
        }
        copySync(resolve(from), resolve(outDir, toDir));
      }
    }
  };
};
