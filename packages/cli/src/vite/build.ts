import type { CreateViteConfigOptions } from './types';
import externalGlobals from 'rollup-plugin-external-globals';
import type { BuildOptions, LibraryOptions } from 'vite';
import { resolve } from 'path';

// 默认手动分块，将 node_modules 下的模块按照一定规则分块
const defaultManualChunks = (id: string) => {
  if (id.includes('node_modules')) {
    const arr = id.split('node_modules/');
    const dirs = arr[arr.length - 1].split('/');
    // 以第一级目录名作为分块名
    return dirs[0];
  }
};

const extMap: Record<string, string> = {
  es: '.mjs',
  cjs: '.cjs',
  umd: '.umd.js',
  iife: '.iife.js'
};

// 创建输入文件，用于生成 Rollup 输入配置
const createInput = (opts: CreateViteConfigOptions) => {
  if (!opts.pages) return undefined;
  const input: Record<string, string> = {};
  // pages包含：页面名name 和 文件路径file 的映射
  for (const [name, file] of Object.entries(opts.pages)) {
    // 相对路径解析为绝对路径
    input[name] = resolve(file);
  }
  return input;
};

/**
 * 生成 Vite 构建配置
 * @param opts 入参选项
 * @returns Vite 构建配置对象，用于在 Vite 项目中进行构建时使用
 */
export const createBuild = (opts: CreateViteConfigOptions) => {
  // Rollup 插件
  const rollupPlugins: any[] = opts.externalGlobals ? [externalGlobals(opts.externalGlobals)] : [];
  // 库模式不需要手动分块，否则使用opts.manualChunks 或者 defaultManualChunks手动分块
  const manualChunks = opts.lib ? undefined : opts.manualChunks || defaultManualChunks;
  // 构建目标
  const buildTarget = typeof opts.buildTarget === 'undefined' ? (opts.lib ? 'esnext' : 'es2015') : opts.buildTarget;
  // 返回 Vite 构建配置对象
  const build: BuildOptions = {
    // 构建目标  plugin-legacy overrode 'build.target'
    target: opts.legacy ? undefined : buildTarget,
    // 输出目录
    outDir: opts.outDir,
    // 是否在构建前清空输出目录
    emptyOutDir: opts.emptyOutDir,
    // 关闭可提高打包速度
    reportCompressedSize: false,
    // 分块大小警告限制，1024KB(2MB)
    chunkSizeWarningLimit: 1024,
    // 是否复制public目录
    copyPublicDir: opts.copyPublicDir,
    // 库模式配置，包含入口文件、库名、输出格式和文件名
    lib: opts.lib
      ? ({
          entry: opts.entry,
          name: opts.library,
          formats: opts.formats,
          fileName: (format) => {
            return opts.libFileName + extMap[format];
          }
        } as LibraryOptions)
      : false,
    // Rollup 选项，包含外部依赖、插件、输出选项和输入文件配置
    rollupOptions: {
      external: opts.external,
      plugins: rollupPlugins,
      output: {
        manualChunks,
        exports: opts.exports || 'auto'
      },
      input: createInput(opts)
    }
  };

  return build;
};
