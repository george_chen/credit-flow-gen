import type { UserConfig, ServerOptions, UserConfigExport } from 'vite';
import os from 'os';
import type { CreateViteConfigOptions, CreatePluginViteConfigOptions, ProxyConfig } from './types';
import { resolve } from 'path';
import { upperFirstCamelCase, pathExistsSync, copySync, removeSync, readJsonSync } from 'cfg-packages-node';
import { defaults } from './defaults';
import { createBuild } from './build';
import { mergePlugins } from './plugins';
export * from './types';

// 创建 Vite 服务器配置，包括：端口号、代理设置、HTTPS 支持、主机地址 等
const createServer = (port = 3010, proxy?: ProxyConfig, https?: boolean, host?: string) => {
  return {
    // Windows 和 macOS 系统下服务器启动后自动打开浏览器
    open: os.platform() === 'win32' || os.platform() === 'darwin',
    // 启用跨域资源共享（CORS）
    cors: true,
    port,
    host: host || '0.0.0.0',
    // 代理配置
    proxy,
    // 是否启用 HTTPS
    https
  } as ServerOptions;
};

// 针对指定模块列表，忽略对 node_modules 目录下该指定模块的监听
const getWatchIgnored = (modules: string[]) => {
  return modules.map((name) => {
    return `'!**/node_modules/${name}/**'`;
  });
};

/**
 * 核心-创建 Vite 配置
 * @param options 可选的 Vite 配置选项
 * @returns 最终的配置对象
 */
export function createViteConfig(options: CreateViteConfigOptions = {}): UserConfigExport {
  // 1. 可选配置和默认配置合二为一
  const opts = Object.assign({}, defaults, options);
  // 2. 设置路径别名，默认将 '@' 映射到 'src' 目录
  const alias = {
    '@': resolve('src'),
    ...opts.alias
  };
  // 3. 创建开发服务器 和 预览服务器 配置
  const server = createServer(opts.port as number, opts.proxy, opts.https, opts.host);
  if (opts.watchModules) {
    server.watch = {
      ignored: getWatchIgnored(opts.watchModules)
    };
  }
  const preview = createServer(opts.previewPort as number, opts.proxy, opts.https, opts.host);
  // 4. 调用 mergePlugins 函数来获取插件列表
  const plugins = mergePlugins(opts);
  // 5. 调用 createBuild 函数来生成构建配置
  const build = createBuild(opts);
  // 6. 设置依赖优化配置
  const optimizeDeps = {
    force: !!opts.force,
    include: opts.optimizeDeps ? opts.optimizeDeps : undefined,
    exclude: opts.watchModules
  };
  // 7. 返回最终的配置对象，如启用debug模式，则打印配置对象
  const config: UserConfig = {
    base: opts.base,
    resolve: {
      alias
    },
    server,
    preview,
    build,
    plugins,
    optimizeDeps
  };
  const userConfig = opts.defineConfig ? opts.defineConfig(config) : config;
  if (opts.debug) {
    console.log(JSON.stringify(userConfig, null, 2));
  }
  return userConfig;
}

/**
 * 核心-创建 插件 配置
 * @param options 可选的 插件 配置选项
 * @returns
 */
export function createPluginViteConfig(options: CreatePluginViteConfigOptions = {}) {
  const { style = 'style.css', outDir = 'dist', material = 'material.json', isUmd } = options;
  // 1. 读取 package.json 文件并解析包名
  const pkg = readJsonSync(resolve('package.json'));
  const library = upperFirstCamelCase(pkg.name);
  const outputFileName = pkg.name.replace(/\//gi, '__');
  // 2. 定义 buildEnd 函数，用于在构建结束时处理文件复制和清理
  const buildEnd = () => {
    const stylePath = resolve(outDir, style);
    const materialPath = resolve('src', material);
    if (!isUmd) {
      removeSync(stylePath);
      return;
    }
    if (pathExistsSync(stylePath)) {
      copySync(stylePath, stylePath.replace(style, `${outputFileName}.css`));
    }

    if (pathExistsSync(materialPath)) {
      copySync(materialPath, resolve(outDir, `${outputFileName}.json`));
    }
    removeSync(stylePath);
  };
  // 3. 设置默认配置，包括库名、输出文件名、外部依赖和全局变量配置
  const defaults = {
    copyPublicDir: false,
    exports: 'named',
    buildEnd,
    lib: true,
    dts: isUmd ? false : true,
    version: false,
    emptyOutDir: isUmd ? false : true,
    formats: isUmd ? ['umd'] : ['es'],
    buildTarget: 'es2015',
    library,
    libFileName: outputFileName,
    external: [
      'vue',
      'vue-router',
      'element-plus',
      '@element-plus/icons-vue',
      'cfg-packages-utils',
      'cfg-packages-icons',
      'cfg-packages-ui',
      'cfg-packages-core'
    ],
    externalGlobals: isUmd
      ? {
          vue: 'Vue',
          'vue-router': 'VueRouter',
          'element-plus': 'ElementPlus',
          '@element-plus/icons-vue': 'CfgIcons',
          'cfg-packages-utils': 'CfgUtils',
          'cfg-packages-icons': 'CfgIcons',
          'cfg-packages-ui': 'CfgUI'
        }
      : undefined
  };
  // 4. 调用 createViteConfig 函数生成最终的 Vite 配置
  return createViteConfig(Object.assign(defaults, options));
}
