import type { CreateViteConfigOptions } from './types';
import { type PluginOption } from 'vite';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import legacy from '@vitejs/plugin-legacy';
import basicSsl from '@vitejs/plugin-basic-ssl';
import dts from 'vite-plugin-dts';
import { visualizer } from 'rollup-plugin-visualizer';
import ElementPlus from 'unplugin-element-plus/vite';
import { nodePolyfills } from 'vite-plugin-node-polyfills';
import { removeSync, toArray } from 'cfg-packages-node';
import { copyPlugin, type CopyPluginOption } from '../plugins/copy';
import { babelPlugin } from '../plugins/babel';
import { versionPlugin } from '../plugins/version';
import { staticPlugin } from '../plugins/static';
import { loadingPlugin } from '../plugins/loading';
import { envPlugin } from '../plugins/env';

// 创建 Babel 插件
const createBabelPlugin = (targets: string[]) => {
  return babelPlugin({
    apply: 'build',
    babelConfig: {
      // 使用 @babel/preset-env 预设来配置 Babel，指定目标环境，禁用模块转换和内置实现
      presets: [['@babel/preset-env', { targets: targets, modules: false, useBuiltIns: false }]]
    }
  });
};

/**
 * 合并插件，输出最终Vite 配置所需的插件数组
 * @param opts 配置选项
 * @returns 插件数组
 */
export const mergePlugins = (opts: CreateViteConfigOptions) => {
  // 加载环境变量配置插件、支持 Vue.js 和 JSX插件
  const plugins: PluginOption[] = [envPlugin({ dir: opts.envPath }), vue(), vueJsx()];

  if (opts.version) {
    // 版本插件
    plugins.push(versionPlugin());
  }

  if (opts.babel && opts.targets) {
    // Babel 插件
    plugins.push(createBabelPlugin(toArray(opts.targets)));
  }

  if (opts.elementPlus) {
    // ElementPlus 插件
    plugins.push(ElementPlus(typeof opts.elementPlus === 'object' ? opts.elementPlus : {}));
  }

  if (opts.lib && opts.dts) {
    // 先移除TypeScript 定义文件输出目录
    if (opts.dtsOutputDir) {
      try {
        removeSync(opts.dtsOutputDir);
      } catch (_e) {
        console.error('remove dtsOutputDir error', _e);
      }
    }
    // TypeScript 定义文件插件 (dts)
    plugins.push(
      dts({
        outDir: opts.dtsOutputDir,
        staticImport: true,
        cleanVueFileName: true
      }) as PluginOption
    );
  }

  if (opts.https) {
    // HTTPS 支持插件
    plugins.push(basicSsl() as PluginOption);
  }

  if (!opts.lib) {
    // 旧版浏览器支持插件
    plugins.push(
      legacy({
        targets: opts.targets,
        polyfills: opts.polyfills,
        renderLegacyChunks: !!opts.legacy
      }) as PluginOption
    );
  }

  if (opts.visualizer) {
    // 可视化插件（用于生成报告）
    plugins.push(
      visualizer({
        open: true,
        gzipSize: true,
        brotliSize: true
      }) as any
    );
  }

  if (opts.staticDirs) {
    // 静态文件服务插件
    plugins.push(staticPlugin(opts.staticDirs));
    if (opts.copyStatic) {
      const copyOptions: CopyPluginOption[] = opts.staticDirs.map((n) => {
        return typeof n === 'string' ? { from: n, to: '/' } : { from: n.dir, to: n.path };
      });
      // 复制文件或目录插件
      plugins.push(copyPlugin(copyOptions));
    }
  }

  if (opts.loading) {
    // 进度条加载插件
    plugins.push(loadingPlugin());
  }

  if (opts.node) {
    // Node.js Polyfills 插件
    plugins.push(nodePolyfills(typeof opts.node === 'object' ? opts.node : {}));
  }

  if (opts.buildEnd) {
    // 自定义插件，在构建结束时调用该回调
    plugins.push({
      name: 'cfg-build-end-plugin',
      closeBundle() {
        opts.buildEnd && opts.buildEnd();
      }
    });
  }

  if (opts.plugins) {
    // 其余附加的插件
    plugins.push(...opts.plugins);
  }

  return plugins;
};
