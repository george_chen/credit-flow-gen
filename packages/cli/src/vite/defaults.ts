import type { CreateViteConfigOptions } from './types';

// 默认 Vite 配置选项
export const defaults: CreateViteConfigOptions = {
  debug: false,
  base: '/',
  port: 3010,
  previewPort: 3011,
  dtsOutputDir: 'types',
  targets: ['chrome > 60'],
  // buildTarget: 'esnext',
  polyfills: true,
  entry: 'src/index.ts',
  libFileName: 'index',
  formats: ['es', 'cjs', 'umd', 'iife'],
  loading: true,
  envPath: './',
  copyPublicDir: true
};
