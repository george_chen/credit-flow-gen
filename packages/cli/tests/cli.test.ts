import { expect, test } from 'vitest';
import { copy, rm, createViteConfig } from '../dist/index.mjs';

let copyFileName = '';

test('copy', () => {
  const timestamp = new Date().getTime();
  copyFileName = `dist/index-${timestamp}.cjs`;
  copy('dist/index.cjs', copyFileName);
  expect(true).toBe(true);
});

test('rm', () => {
  rm(copyFileName);
  console.log('rm success: ', copyFileName);
  expect(true).toBe(true);
});

test('createViteConfig', () => {
  createViteConfig({
    debug: true,
    lib: true,
    dts: true,
    version: true,
    formats: ['es', 'cjs']
  });
  expect(true).toBe(true);
});
