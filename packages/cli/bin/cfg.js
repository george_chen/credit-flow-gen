#!/usr/bin/env node
// 上面那行命令主要是为了让系统看到时会沿着该路径去查找node并执行，主要是为了兼容Mac，确保可执行。

import { rm, copy } from '../dist/index.mjs';

const type = process.argv[2];

if (type) {
  switch (type) {
    case 'rm':
      // eslint-disable-next-line no-case-declarations
      const target = process.argv[3];
      if (target) {
        rm(target);
      }
      break;
    case 'copy':
      // eslint-disable-next-line no-case-declarations
      const src = process.argv[3];
      // eslint-disable-next-line no-case-declarations
      const dest = process.argv[4];
      if (src && dest) {
        copy(src, dest);
      }
      break;
  }
} else {
  console.log('缺少操作命令类型');
}
