# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.10](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-cli@0.0.9...cfg-packages-cli@0.0.10) (2024-10-23)

### Features

- cfg-packages-cli createServer open 改成：Windows 和 macOS 系统下服务器启动后自动打开浏览器 ([71a07e3](https://gitee.com/george_chen/credit-flow-gen/commits/71a07e381a68d0e1d7cd87f7004a5d64f02d9464))

## [0.0.9](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-cli@0.0.8...cfg-packages-cli@0.0.9) (2024-08-22)

**Note:** Version bump only for package cfg-packages-cli

## [0.0.8](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-cli@0.0.7...cfg-packages-cli@0.0.8) (2024-07-06)

### Features

- cfg-packages-pro 低代码开发平台-通用版 CFG.PRO 开发完成 ([b157b61](https://gitee.com/george_chen/credit-flow-gen/commits/b157b61fe88c18593356c1d35859d626b102a2c9))

## [0.0.7](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-cli@0.0.6...cfg-packages-cli@0.0.7) (2024-06-13)

**Note:** Version bump only for package cfg-packages-cli

## [0.0.6](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-cli@0.0.5...cfg-packages-cli@0.0.6) (2024-06-03)

**Note:** Version bump only for package cfg-packages-cli

## [0.0.5](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-cli@0.0.4...cfg-packages-cli@0.0.5) (2024-05-31)

**Note:** Version bump only for package cfg-packages-cli

## [0.0.4](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-cli@0.0.3...cfg-packages-cli@0.0.4) (2024-05-31)

### Features

- 完成 cfg-packages-icons 开发 ([57a310b](https://gitee.com/george_chen/credit-flow-gen/commits/57a310b6e24601adc1696b94f602bdc28213f8e5))

## [0.0.3](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-cli@0.0.2...cfg-packages-cli@0.0.3) (2024-05-28)

### Features

- 提取项目公共 npm 包到根目录下的 package.json；完成 cfg-packages-core 模块的开发 ([b64dc5f](https://gitee.com/george_chen/credit-flow-gen/commits/b64dc5f86d396d0c14cc7423b15f22c0816415fe))

## [0.0.2](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-cli@0.0.1...cfg-packages-cli@0.0.2) (2024-05-18)

### Features

- 完成 cfg-packages-cli 命令行和创建 Vite 配置-工具包模块 开发 ([3d55414](https://gitee.com/george_chen/credit-flow-gen/commits/3d5541404251668777ab26dc2e29a3cbac86ddb4))
- 完成 cfg-packages-node 适配 Node 环境-工具包模块开发 ([a320df5](https://gitee.com/george_chen/credit-flow-gen/commits/a320df5ec92acdd18d4ff2b778ec6bf5b2693201))
- 完成 cfg-packages-node 适配 Node 环境-工具包模块开发 ([67b80ea](https://gitee.com/george_chen/credit-flow-gen/commits/67b80ea7d225a99ce0e58b2706eaf5c0816e4a40))
- 暂存 cfg-packages-cli，先开发 cfg-packages-node ([2416752](https://gitee.com/george_chen/credit-flow-gen/commits/24167525c18716515358a45c75ad89443e6b4b01))
- cfg-packages-cli 包开发中 ([cd1abc7](https://gitee.com/george_chen/credit-flow-gen/commits/cd1abc738f9cbbade1fd37268518c75885ce9a93))

## 0.0.1 (2024-05-12)

### Features

- packages/base 模块初始化 ([154afd9](https://gitee.com/george_chen/credit-flow-gen/commits/154afd9d88021af694f9e43dd6a461076aba8c92))
