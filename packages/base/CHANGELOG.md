# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.8](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-base@0.0.7...cfg-packages-base@0.0.8) (2024-10-23)

**Note:** Version bump only for package cfg-packages-base

## [0.0.7](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-base@0.0.6...cfg-packages-base@0.0.7) (2024-08-22)

### Features

- base 包增加 serializeParams 方法；utils 包完善 request 方法，增加 commonReqResHandler 方法 ([f2df3b0](https://gitee.com/george_chen/credit-flow-gen/commits/f2df3b090c49f995385f33635c04f3fe5e4ce09c))

## [0.0.6](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-base@0.0.5...cfg-packages-base@0.0.6) (2024-06-13)

### Features

- 完成 cfg-packages-ui 开发，准备发布 npm 包 ([96db65a](https://gitee.com/george_chen/credit-flow-gen/commits/96db65aa0ff114e77dba06ef85f7b2ae35d30de2))

## [0.0.5](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-base@0.0.4...cfg-packages-base@0.0.5) (2024-06-03)

### Features

- 完成 cfg-packages-local npm 包开发 ([154762d](https://gitee.com/george_chen/credit-flow-gen/commits/154762dce238724b37b670dbee9ba8719e26b6af))

## [0.0.4](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-base@0.0.3...cfg-packages-base@0.0.4) (2024-05-31)

### Features

- cfg-packages-utils 包开发中，需要加入部分方法 ([c9b80b4](https://gitee.com/george_chen/credit-flow-gen/commits/c9b80b469bc23624e05a74ea6576c328e7c12907))

## [0.0.3](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-base@0.0.2...cfg-packages-base@0.0.3) (2024-05-28)

### Features

- 提取项目公共 npm 包到根目录下的 package.json；完成 cfg-packages-core 模块的开发 ([b64dc5f](https://gitee.com/george_chen/credit-flow-gen/commits/b64dc5f86d396d0c14cc7423b15f22c0816415fe))

## [0.0.2](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-base@0.0.1...cfg-packages-base@0.0.2) (2024-05-18)

### Features

- 完成 cfg-packages-node 适配 Node 环境-工具包模块开发 ([a320df5](https://gitee.com/george_chen/credit-flow-gen/commits/a320df5ec92acdd18d4ff2b778ec6bf5b2693201))
- 完成 cfg-packages-node 适配 Node 环境-工具包模块开发 ([67b80ea](https://gitee.com/george_chen/credit-flow-gen/commits/67b80ea7d225a99ce0e58b2706eaf5c0816e4a40))
- 暂存 cfg-packages-cli，先开发 cfg-packages-node ([2416752](https://gitee.com/george_chen/credit-flow-gen/commits/24167525c18716515358a45c75ad89443e6b4b01))
- cfg-packages-cli 包开发中 ([cd1abc7](https://gitee.com/george_chen/credit-flow-gen/commits/cd1abc738f9cbbade1fd37268518c75885ce9a93))

## 0.0.1 (2024-05-12)

### Features

- 完成 cfg-packages-base 适配 Node 环境和浏览器环境-基础模块 ([990c579](https://gitee.com/george_chen/credit-flow-gen/commits/990c579032cffc12332548e31193092e64f7299e))
- packages/base 模块初始化 ([154afd9](https://gitee.com/george_chen/credit-flow-gen/commits/154afd9d88021af694f9e43dd6a461076aba8c92))
