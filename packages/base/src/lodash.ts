import { upperFirst, camelCase, isNil } from 'lodash-es';
export {
  isString,
  isFunction,
  isArray,
  isObject,
  isBoolean,
  isBuffer,
  isArrayBuffer,
  isDate,
  isUndefined,
  isNaN,
  isNull,
  isNumber,
  isSymbol,
  isPlainObject,
  isEqual,
  noop,
  upperFirst,
  camelCase,
  get,
  set,
  cloneDeep,
  merge,
  debounce,
  throttle,
  template,
  lowerFirst,
  kebabCase,
  isEmpty
} from 'lodash-es';

export function upperFirstCamelCase(name: string) {
  return upperFirst(camelCase(name));
}

export function isNotEmpty(value: string) {
  return !isNil(value) && value !== '';
}
