import _MD5 from 'crypto-js/md5';
import _Base64 from 'crypto-js/enc-base64';
import enc from 'crypto-js/enc-utf8';

/**
 * MD5加密
 * @param content
 * @returns
 */
export function MD5(content: string) {
  return String(_MD5(content));
}

/**
 * Base64编码
 * @param content
 * @returns
 */
export function base64(content: string) {
  const wordArray = enc.parse(content);
  return _Base64.stringify(wordArray);
}

/**
 * Base64解码
 * @param content
 * @returns
 */
export function unBase64(content: string) {
  return enc.stringify(_Base64.parse(content));
}
