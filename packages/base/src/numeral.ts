import numeral from 'numeral';

/**
 * 数字格式化，默认2位小数
 * @param value
 * @param format
 * @returns
 */
export function numberFormat(value: number, format = '0.00') {
  return numeral(value).format(format);
}

/**
 * 保留小数点，默认2位
 * @param value  数值
 * @param number 小数位数
 * @param round 是否四舍五入
 * @returns
 */
export function toFixed(value: number, number = 2, round: boolean) {
  const method = round ? Math.round : Math.floor;
  return method(Math.pow(10, number) * value) / Math.pow(10, number);
}

/**
 * 判断正整数是否在对应范围内
 * @param value
 * @param item
 * @returns
 */
export function isRangeNum(value: number, item: { min?: number; max?: number }) {
  const { min = 0, max = Number.MAX_SAFE_INTEGER } = item;
  const flag = value >= min && value <= max;
  return flag;
}

/**
 * 处理成仅数字，有最大值返回最大值
 * @param v
 * @param max
 * @returns
 */
export function onlyNumber(numStr: string, max = Number.MAX_SAFE_INTEGER) {
  if (!numStr) return '';
  const dealNum = +numStr.replace(/\D/g, ''); // 去掉非数字&首位0
  const realNum = !dealNum ? '' : dealNum; // 不能为 0
  return realNum ? Math.min(max, realNum) : '';
}

/**
 * 修改输入的正整数在大小范围内
 * @param v
 * @param item
 * @returns
 */
export function modifyRangeNum(v: number, item: { min?: number; max?: number }) {
  const { min = 0, max = Number.MAX_SAFE_INTEGER } = item;
  if (!v) return;
  if (v < min) {
    // 判断下限
    return min;
  }
  if (v > max) {
    // 判断上限
    return max;
  }
  return v;
}

export { numeral };
