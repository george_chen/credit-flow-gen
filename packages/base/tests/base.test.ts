import { expect, test } from 'vitest';
import {
  isMobile,
  isName,
  isIdCardNo,
  isNotEmpty,
  isEmpty,
  isRangeNum,
  onlyNumber,
  modifyRangeNum
} from '../dist/index.mjs';

test('isMobile', () => {
  const mobile = '93634093193';
  expect(isMobile(mobile)).toBe(true);
});

test('isName', () => {
  const name = '测试账号·翟宾有';
  expect(isName(name)).toBe(true);
});

test('isIdCardNo', () => {
  const idCardNo = '968992199904061868';
  expect(isIdCardNo(idCardNo)).toBe(true);
});

test('isNotEmpty', () => {
  const value = '0';
  expect(isNotEmpty(value)).toBe(true);
});

test('isEmpty', () => {
  const value = '';
  expect(isEmpty(value)).toBe(true);
});

test('isRangeNum has min and max', () => {
  const value = 1;
  const item = { min: 0, max: 10 };
  expect(isRangeNum(value, item)).toBe(true);
});

test('isRangeNum has not min and max', () => {
  const value = 9007199254740991;
  expect(isRangeNum(value, {})).toBe(true);
});

test('onlyNumber', () => {
  const numStr = '1234567890';
  expect(onlyNumber(numStr)).toBe(1234567890);
});

test('modifyRangeNum', () => {
  const v = 100;
  const item = { min: 0, max: 10 };
  expect(modifyRangeNum(v, item)).toBe(10);
});
