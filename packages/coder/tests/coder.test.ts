import { expect, test } from 'vitest';
import { replaceThis } from '../src/utils';

test('replaceThis', () => {
  expect(replaceThis('this.context = "123"')).toBe('context = "123"');
});
