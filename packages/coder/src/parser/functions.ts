import { type JSFunction } from 'cfg-packages-core';
import { replaceFunctionTag, parseValue, replaceComputedValue } from '../utils';

/**
 * 解析和转换一个函数映射（map），并将其转换为包含函数定义字符串的数组。
 * 该函数的目的是处理函数映射中的每个函数，替换标签和计算值，以便生成可以直接在代码中使用的函数字符串。ex:
 * const map = {
 *   fetchData: {
 *     value: 'async function fetchData() { return await fetch("/api/data"); }'
 *   },
 *   computeResult: {
 *     value: 'function computeResult() { return this.result.value; }'
 *   }
 * };
 * const computedKeys = ['result'];
 * parseFunctionMap(map, computedKeys); // 返回结果如下：
 * [
 *   'async fetchData() { return await fetch("/api/data"); }',
 *   'computeResult() { return this.result; }'
 * ]
 * @param map
 * @param computedKeys
 * @returns
 */
export function parseFunctionMap(map: Record<string, JSFunction> = {}, computedKeys: string[] = []) {
  return Object.entries(map).map(([name, val]) => {
    let handler = replaceFunctionTag(parseValue(val, false, false) as string);
    handler = replaceComputedValue(handler, computedKeys);
    if (handler.startsWith('async')) {
      return `async ${name}${handler.replace(/^async/, '')}`;
    }
    return `${name}${handler}`;
  });
}
