import type { BlockEmit } from 'cfg-packages-core';

/**
 * 解析事件名称列表，并将其转换为字符串数组。ex:
 * const emits = ['click', { name: 'submit' }];
 * parseEmits(emits); // 返回结果如下：
 * ["'click'", "'submit'"]
 * @param emits
 * @returns
 */
export function parseEmits(emits: Array<string | BlockEmit> = []) {
  return emits.map((n) => {
    const name = typeof n === 'string' ? n : n.name;
    return `'${name}'`;
  });
}
