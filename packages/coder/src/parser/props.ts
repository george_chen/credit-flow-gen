import { type BlockProp, type BlockPropDataType } from 'cfg-packages-core';
import { toArray } from 'cfg-packages-base';
import { parseValue, isJSCode } from '../utils';

/**
 * 将一个包含属性配置的数组解析为特定格式的字符串数组。每个字符串表示一个属性的配置信息，
 * 包括 type（类型）、required（是否必需）和 default（默认值）。ex:
 * const props = [
 *   'simpleProp',
 *   { name: 'complexProp', type: 'String', required: true, default: 'default value' }
 * ];
 * parseProps(props); // 返回结果如下：
 * [
 *   "simpleProp: {}",
 *   "complexProp: {
 *     type: [String],
 *     required: true,
 *     default: 'default value'
 *   }
 * ]

 * @param props
 * @returns
 */
export function parseProps(props: Array<string | BlockProp> = []) {
  const toTypes = (type?: BlockPropDataType | BlockPropDataType[]) => {
    if (!type) return undefined;
    const types = toArray(type);
    const _types = types.map((n: BlockPropDataType) => {
      // eslint-disable-next-line no-useless-escape
      return n.replace(/\'|\"/gi, '');
    });
    return `[${_types.join(',')}]`;
  };

  return props.map((prop) => {
    if (typeof prop === 'string') {
      return `${prop}: {}`;
    } else {
      if (isJSCode(prop.default) && !prop.default.value) {
        prop.default.value = 'undefined';
      }
      return `${prop.name}: {
            type: ${toTypes(prop.type)},
            required: ${parseValue(!!prop.required, true, false)},
            default: ${parseValue(prop.default, true, false)}
          }`;
    }
  });
}
