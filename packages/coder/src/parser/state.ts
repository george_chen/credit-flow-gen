import { type BlockState } from 'cfg-packages-core';
import { parseValue } from '../utils';

/**
 * 解析一个表示状态的对象并将其转换为特定格式的字符串数组。
 * @param state
 * @returns
 */
export function parseState(state: BlockState = {}) {
  return Object.entries(state).map(([name, val]) => {
    const value = parseValue(val, false);
    return `${name}:${value}`;
  });
}
