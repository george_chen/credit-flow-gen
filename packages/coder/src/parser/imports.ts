import { type MaterialDescription } from 'cfg-packages-core';
import { dedupArray } from 'cfg-packages-base';

/**
 * 解析组件的导入声明，并生成导入语句数组。其主要目的是根据传入的组件映射和其他配置，生成适当的导入语句。ex:
 * const componentMap = new Map<string, MaterialDescription>([
 *   ['Button', { package: 'element-ui', name: 'Button' }],
 *   ['Input', { package: 'element-ui', name: 'Input' }],
 *   ['Form', { package: 'ant-design-vue', name: 'Form' }]
 * ]);
 * const components = ['Button', 'Input', 'Form'];
 * const collectImports = {
 *   'element-ui': new Set(['Message']),
 *   'ant-design-vue': new Set(['FormItem'])
 * };
 * parseImports(componentMap, components, [], collectImports); // 返回结果如下：
 * [
 *   "import { defineComponent,reactive } from 'vue';",
 *   "import { Button,Input,Message } from 'element-ui';",
 *   "import { Form,FormItem } from 'ant-design-vue';"
 * ];
 * @param componentMap
 * @param components
 * @param importBlocks
 * @param collectImports
 * @returns
 */
export function parseImports(
  componentMap: Map<string, MaterialDescription>,
  components: string[] = [],
  importBlocks: string[] = [],
  collectImports: Record<string, Set<string>> = {}
) {
  const imports: Record<string, string[]> = {
    vue: ['defineComponent', 'reactive']
  };

  for (const name of components) {
    const desc = componentMap.get(name.split(':')[0]);
    if (desc && desc.package) {
      const items = imports[desc.package] ?? (imports[desc.package] = []);
      items.push(desc.parent || (desc.alias || '').split('.')[0] || desc.name);
    }
  }

  for (const [name, value] of Object.entries(collectImports)) {
    const items = imports[name] ?? (imports[name] = []);
    items.push(...Array.from(value));
  }

  return (
    Object.entries(imports)
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      .filter(([_name, values]) => !!values.length)
      .map(([name, values]) => {
        return `import { ${(dedupArray(values) as string[]).join(',')}} from '${name}';`;
      })
      .concat(importBlocks)
  );
}
