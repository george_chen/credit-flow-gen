import { type BlockInject } from 'cfg-packages-core';
import { parseValue } from '../utils';

/**
 * 将 BlockInject 数组解析为特定格式的字符串数组，每个字符串表示一个依赖注入的配置信息，
 * 包括 from（来源）和 default（默认值）。ex:
 * const inject = [
 *   { name: 'serviceA', from: 'ServiceA', default: () => {} },
 *   { name: 'serviceB', default: 'default value' }
 * ];
 * parseInject(inject); // 返回结果如下：
 * [
 *   "serviceA: {
 *     from: 'ServiceA',
 *     default: () => {}
 *   }",
 *   "serviceB: {
 *     from: 'serviceB',
 *     default: 'default value'
 *   }"
 * ];
 * @param inject
 * @returns
 */
export function parseInject(inject: BlockInject[] = []) {
  return inject.map((n) => {
    return `${n.name}: {
            from: '${n.from || n.name}',
            default: ${parseValue(n.default, true, false)}
        }`;
  });
}
