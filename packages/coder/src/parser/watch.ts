import { type BlockWatch, type JSFunction } from 'cfg-packages-core';
import { parseFunctionMap } from './functions';
import { isJSFunction, replaceFunctionTag } from '../utils';

/**
 * 解析 "watch" 配置，将它们转换为 JavaScript 对象和字符串，用于监听数据变化
 * @param watch
 * @param computedKeys
 * @returns
 */
export function parseWatch(watch: BlockWatch[] = [], computedKeys: string[] = []) {
  // 1. watch每一项`watcher_${current.id}`都需要加到computed里面，如遇到watch里面监听了computed已有的值，则去掉.value
  const watchers = watch.reduce((prev, current) => {
    if (current.id && isJSFunction(current.source)) {
      prev[`watcher_${current.id}`] = current.source;
    }
    return prev;
  }, {} as Record<string, JSFunction>);
  const computed = parseFunctionMap(watchers, computedKeys);

  const watches = watch.map((n) => {
    return `watcher_${n.id}: {
        deep: ${n.deep},
        immediate: ${n.immediate},
        handler${replaceFunctionTag(n.handler.value)}
      }`;
  });
  return {
    computed,
    watches
  };
}
