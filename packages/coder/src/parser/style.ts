import { jsonToStyle } from '../utils';

/**
 * 用于将传入的样式对象解析成字符串形式
 * @param style
 * @returns
 */
export function parseStyle(style: Record<string, Record<string, any>> = {}) {
  const result: string[] = [];
  for (const [name, json] of Object.entries(style)) {
    result.push(`
      ${name} {
        ${jsonToStyle(json)}
      }
    `);
  }
  return result.join('\n');
}
