import { type NodeFromUrlSchema, type NodeFromPlugin } from 'cfg-packages-core';

/**
 * 生成并返回一个字符串数组，每个字符串定义了一个 URL 模式组件
 * @param urlSchemas
 * @returns
 */
export function parseUrlSchemas(urlSchemas: Record<string, NodeFromUrlSchema> = {}) {
  const result: string[] = [];
  Object.entries(urlSchemas).forEach(([name, from]) => {
    result.push(`const ${name} = provider.defineUrlSchemaComponent('${from.url}');`);
  });
  return result;
}

/**
 * 生成并返回一个字符串数组，每个字符串定义了一个插件组件
 * @param plugins
 * @returns
 */
export function parseBlockPlugins(plugins: Record<string, NodeFromPlugin> = {}) {
  const result: string[] = [];
  Object.entries(plugins).forEach(([name, from]) => {
    result.push(`const ${name} = provider.definePluginComponent(${JSON.stringify(from)});`);
  });
  return result;
}
