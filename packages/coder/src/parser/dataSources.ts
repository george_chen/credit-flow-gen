import { type DataSourceSchema } from 'cfg-packages-core';
import { isJSFunction } from '../utils';

/**
 * 处理数据源的配置并将它们转换为异步方法
 * @param dataSources
 * @returns
 */
export function parseDataSources(dataSources: Record<string, DataSourceSchema> = {}) {
  return Object.values(dataSources).map((item) => {
    const transform = isJSFunction(item.transform) ? item.transform.value || `(res) => res` : `(res) => res`;
    return `async ${item.name}(...args:any[]) {
          return await this.provider.apis['${item.ref}'].apply(this, args).then(${transform});
        }`;
  });
}
