/**!
 * Copyright (c) 2024, credit-flow-gen CFG All rights reserved.
 * @name cfg-packages-coder
 * @version 0.0.53
 */
export const version = '0.0.53';
