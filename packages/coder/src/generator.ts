import { type BlockSchema, type MaterialDescription, type Dependencie, type PageFile } from 'cfg-packages-core';
import { cloneDeep } from 'cfg-packages-base';
import {
  tsFormatter,
  // htmlFormatter,
  cssFormatter,
  vueFormatter
} from './formatters';
import { Collecter } from './collecter';
import { parser } from './parser';
import { scriptCompiled, vueCompiled } from './templates';

/**
 * 核心-Vue文件代码生成，处理过程如下：
 * 1. 入参：dsl、componentMap、dependencies
 * 2. Collecter 预处理收集信息
 * 3. Parser 解析 dsl，提取 token
 * 4. scriptCompiled token 注入模板生成JS代码文件字符串
 * 5. template、css、script、style 生成vue文件字符串
 * 6. 如果需要格式化，则使用prettier处理
 * 7. 返回格式化后的vue文件内容
 */
export async function generator(
  dsl: BlockSchema,
  componentMap: Map<string, MaterialDescription> = new Map(),
  dependencies: Dependencie[] = [],
  formatterDisabled?: boolean
) {
  const collecter = new Collecter(cloneDeep(dsl), dependencies);
  const token = parser(collecter, componentMap);
  const script = scriptCompiled(token);
  const vue = vueCompiled({
    template: token.template,
    css: await cssFormatter(token.css, formatterDisabled),
    script: await tsFormatter(script, formatterDisabled),
    style: await cssFormatter(token.style, formatterDisabled)
  });
  return await vueFormatter(vue, formatterDisabled).catch((e) => {
    e.content = vue;
    return e;
  });
}

/**
 * 生成空白Vue文件
 * @param file
 * @returns
 */
export async function createEmptyPage(file: PageFile) {
  const content = `
    <template>
      <div>
        <h3>源码模式页面</h3>
        <div>文件路径：/.cfg/vue/${file.id}.vue</div>
      </div>
    </template>
    <script lang="ts" setup>
    </script>
    <style>
    </style>
  `;
  return await vueFormatter(content);
}
