import {
  type BlockSchema,
  type Dependencie,
  type JSExpression,
  type JSFunction,
  type NodeSchema,
  type NodeFromUrlSchema,
  type NodeFromPlugin,
  type NodeDirective
} from 'cfg-packages-core';
import { dedupArray } from 'cfg-packages-base';
import { isJSCode } from './utils';

/**
 * Collecter 类，用于处理和收集特定的代码和数据结构中的信息。
 * 通过遍历给定的 DSL 结构，提取并处理各类信息，包括依赖库的引用、上下文、样式、URL Schema 和插件信息。
 * 使得在处理和生成代码时，可以准确地导入所需的库、维护上下文状态、应用样式以及管理外部资源的引用。
 */
export class Collecter {
  /**
   * imports ex：{ 'element-plus': ['ElButton', 'ElInput' ...] }
   */
  public imports: Record<string, Set<string>> = {};
  public context: Record<string, Set<string>> = {};
  public style: Record<string, Record<string, any>> = {};
  public members: string[] = [];
  public urlSchemas: Record<string, NodeFromUrlSchema> = {};
  public blockPlugins: Record<string, NodeFromPlugin> = {};
  private libraryRegex: RegExp[] = [];

  constructor(public dsl: BlockSchema, public dependencies: Dependencie[]) {
    this.libraryRegex = this.collectLibrary();
    this.walk(dsl);
    this.walkNodes(dsl);
    this.members = this.getLibraryMember();
  }

  /**
   * 收集依赖库并生成匹配库引用的正则表达式数组
   * @returns
   */
  private collectLibrary() {
    return this.dependencies
      .filter((n) => !!n.library)
      .map((n) => {
        // eslint-disable-next-line no-useless-escape
        return new RegExp(`(this.\\$libs.${n.library}.([\\\w]+))`, 'g');
      });
  }

  /**
   * 解析匹配项，提取组件名称、路径和库名称，并将其记录到 imports 属性中。
   * @param regexMatchItem ex: this.$libs.ElementPlus.ElButton
   * @returns ex: { name: 'ElButton', path: 'this.$libs.ElementPlus.', library: 'ElementPlus' }
   */
  private collectImport(regexMatchItem: string) {
    const sections = regexMatchItem.split('.');
    if (sections.length === 4) {
      const name = sections.pop() as string;
      const path = sections.join('.') + '.';
      const library = sections.pop() as string;
      if (name && library) {
        const packageName = this.dependencies.find((n) => n.library === library)?.package;
        if (packageName) {
          const imports = this.imports[packageName] || (this.imports[packageName] = new Set());
          imports.add(name);
        }
      }
      return {
        name,
        path,
        library
      };
    }
    return null;
  }

  /**
   * 在代码中查找并替换库引用路径
   * @param code
   * @returns
   */
  private replaceLibraryPath(code: JSExpression | JSFunction) {
    const { libraryRegex } = this;
    let result = code.value;
    for (const regex of libraryRegex) {
      const matches = code.value.match(regex) || [];
      for (const match of matches) {
        const res = this.collectImport(match);
        if (res) {
          const pathStr = res.path.replace(/\$/g, '\\$');
          result = result.replace(new RegExp(pathStr, 'g'), '');
        }
      }
    }
    return result;
  }

  /**
   * 遍历 DSL 结构，并处理其中的 JS 代码，替换库引用路径
   * @param dsl
   */
  private walk(dsl: BlockSchema) {
    const walking = (item: unknown) => {
      if (!item) return;
      if (typeof item !== 'object') return;
      if (Array.isArray(item)) {
        for (const n of item) {
          walking(n);
        }
        return;
      }

      const values = Object.values(item as Record<string, any>);
      for (const value of values) {
        if (isJSCode(value)) {
          value.value = this.replaceLibraryPath(value);
        } else {
          walking(value);
        }
      }
    };
    walking(dsl);
  }

  /**
   * 获取所有库成员的名称，并去重
   * @param components
   * @returns
   */
  private getLibraryMember(components: string[] = []) {
    let array: string[] = [...components];
    for (const set of Object.values(this.imports)) {
      array = array.concat(Array.from(set));
    }
    return dedupArray(array);
  }

  /**
   * 收集 父节点上下文、循环上下文、插槽上下文 作为节点上下文
   * @param node
   * @param parent
   */
  private collectContext(node: NodeSchema, parent?: NodeSchema) {
    // 1. 父节点上下文
    const parentContext = new Set(parent?.id ? this.context[parent.id] : []);
    let nodeContext = new Set<string>(Array.from(parentContext));

    // 2. 循环上下文
    const vFor = (node.directives || []).find((n: NodeDirective) => n.name === 'vFor');
    if (vFor) {
      const { item = 'item', index = 'index' } = vFor.iterator || {};
      nodeContext = new Set([item, index, ...Array.from(nodeContext)]);
    }

    // 3. 插槽上下文
    const slot = node.slot;
    if (slot) {
      const params = typeof slot === 'string' ? [] : slot.params || [];
      const items = params.length ? params : [`scope_${parent?.id}`];
      nodeContext = new Set([...items, ...Array.from(nodeContext)]);
    }

    this.context[node.id as string] = nodeContext;
  }

  /**
   * 收集节点的样式信息
   * @param node
   */
  private collectStyle(node: NodeSchema) {
    if (node.id && node.props?.style) {
      const hasStyle = !!Object.keys(node.props.style).length;
      if (hasStyle) {
        this.style[`.${node.name}_${node.id}`] = node.props.style;
      }
    }
  }

  /**
   * 收集从 URL 导入的 Schema 信息
   * @param node
   */
  private collectUrlSchema(node: NodeSchema) {
    if (typeof node.from === 'object' && node.from.type === 'UrlSchema') {
      this.urlSchemas[node.name] = node.from;
    }
  }

  /**
   * 收集从插件导入的节点信息
   * @param node
   */
  private collectBlockPlugin(node: NodeSchema) {
    if (typeof node.from === 'object' && node.from.type === 'Plugin') {
      this.blockPlugins[node.name] = node.from;
    }
  }

  /**
   * 遍历 DSL nodes，收集上下文、样式、URL Schema 、插件信息
   * @param dsl
   */
  private walkNodes(dsl: BlockSchema) {
    const walking = (node: NodeSchema, parent?: NodeSchema) => {
      this.collectContext(node, parent);
      this.collectStyle(node);
      this.collectUrlSchema(node);
      this.collectBlockPlugin(node);
      if (Array.isArray(node.children)) {
        node.children.forEach((n: any) => walking(n, node));
      }
    };

    if (Array.isArray(dsl.nodes)) {
      dsl.nodes.forEach((n: NodeSchema) => walking(n));
    }
  }
}
