import { expect, test } from 'vitest';
import { widgetManager } from '../dist/index.mjs';

test('demo', () => {
  const widget = widgetManager.get('Logo');
  expect(widget.name).toBe('Logo');
});
