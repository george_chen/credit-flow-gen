import { defineComponent, h } from 'vue';
import { ProjectList } from '../components';
import { useTitle } from '@vueuse/core';

// 封装项目列表页组件
export const ProjectListWrapper = defineComponent({
  name: 'ProjectListWrapper',
  setup() {
    const title = 'CFG低代码平台 X 体验版';
    useTitle(title);
    return {
      title
    };
  },
  render() {
    return h(ProjectList, {
      title: this.title
    });
  }
});

export type ProjectListWrapperInstance = InstanceType<typeof ProjectListWrapper>;
