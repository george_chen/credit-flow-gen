import { type JSExpression, type JSFunction } from 'cfg-packages-core';
import { parseExpression, parseFunction } from 'cfg-packages-renderer';
import { ElNotification, ElMessageBox, ElMessage } from 'element-plus';

export function notify(message: string, title = '提示') {
  return ElNotification.warning({
    title,
    message
  });
}

export async function confirm(message: string) {
  return await ElMessageBox.confirm(message, '提示', { type: 'warning' }).catch(() => false);
}

export function message(message: string, type: 'success' | 'warning' | 'info' | 'error' = 'success') {
  return ElMessage({
    message,
    type
  });
}

export function expressionValidate(str: JSExpression | JSFunction, self: any, thisRequired = false) {
  let vaild = true;
  try {
    if (str.type === 'JSExpression') {
      parseExpression(str, self, thisRequired, true);
    } else {
      parseFunction(str, self, thisRequired, true);
    }
  } catch (e: any) {
    vaild = false;
    ElNotification.error({
      title: '代码错误',
      message: e.message
    });
  }
  return vaild;
}

export function getClassProperties(obj: any) {
  if (obj === null || obj === undefined) {
    throw new Error('obj cannot be null or undefined');
  }
  const prototype = Object.getPrototypeOf(obj);
  if (prototype === null || prototype === undefined) {
    return Object.keys(obj).filter((n) => !['constructor'].includes(n));
  }
  return Object.keys(obj)
    .concat(Object.getOwnPropertyNames(prototype))
    .filter((n) => !['constructor'].includes(n));
}
