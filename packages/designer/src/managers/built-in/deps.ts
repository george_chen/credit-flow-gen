import { type Dependencie, BUILT_IN_VUE, BUILT_IN_VUE_ROUTER } from 'cfg-packages-core';

// 内置依赖列表
export const builtInDeps: Dependencie[] = [
  {
    package: 'vue',
    version: 'latest',
    library: 'Vue',
    urls: ['cfg-packages-materials/deps/vue/vue.global.prod.js'],
    assetsLibrary: BUILT_IN_VUE,
    required: true,
    official: true,
    enabled: true
  },
  {
    package: 'vue-router',
    version: 'latest',
    library: 'VueRouter',
    urls: ['cfg-packages-materials/deps/vue-router/vue-router.global.prod.js'],
    assetsLibrary: BUILT_IN_VUE_ROUTER,
    required: true,
    official: true,
    enabled: true
  },
  {
    package: 'cfg-packages-utils',
    version: 'latest',
    library: 'CfgUtils',
    urls: ['cfg-packages-materials/deps/cfg-packages-utils/index.umd.js'],
    required: true,
    official: true,
    enabled: true
  },
  {
    package: 'cfg-packages-icons',
    version: 'latest',
    library: 'CfgIcons',
    urls: [
      'cfg-packages-materials/deps/cfg-packages-icons/style.css',
      'cfg-packages-materials/deps/cfg-packages-icons/index.umd.js'
    ],
    required: true,
    official: true,
    enabled: true
  },
  {
    package: '@vueuse/core',
    version: 'latest',
    library: 'VueUse',
    urls: [
      'cfg-packages-materials/deps/@vueuse/shared/index.iife.min.js',
      'cfg-packages-materials/deps/@vueuse/core/index.iife.min.js'
    ],
    required: true,
    official: true,
    enabled: true
  },
  {
    package: 'element-plus',
    version: 'latest',
    library: 'ElementPlus',
    localeLibrary: 'ElementPlusLocaleZhCn',
    urls: [
      'cfg-packages-materials/deps/element-plus/dark/css-vars.css',
      'cfg-packages-materials/deps/element-plus/index.css',
      'cfg-packages-materials/deps/element-plus/zh-cn.js',
      'cfg-packages-materials/deps/element-plus/index.full.min.js'
    ],
    assetsUrl: 'cfg-packages-materials/assets/element/index.umd.js',
    assetsLibrary: 'ElementPlusMaterial',
    required: false,
    official: true,
    enabled: true
  },
  {
    package: 'cfg-packages-ui',
    version: 'latest',
    library: 'CfgUI',
    urls: [
      'cfg-packages-materials/deps/cfg-packages-ui/style.css',
      'cfg-packages-materials/deps/cfg-packages-ui/index.umd.js'
    ],
    assetsUrl: 'cfg-packages-materials/assets/ui/index.umd.js',
    assetsLibrary: 'CfgUIMaterial',
    required: false,
    official: true,
    enabled: true
  },
  // TODO: 后续导入 ant-design 组件库
  // {
  //   package: 'ant-design-vue',
  //   version: 'latest',
  //   library: 'antd',
  //   urls: [
  //     'cfg-packages-materials/deps/ant-design-vue/reset.css',
  //     'cfg-packages-materials/deps/ant-design-vue/dayjs/dayjs.min.js',
  //     'cfg-packages-materials/deps/ant-design-vue/dayjs/plugin/customParseFormat.js',
  //     'cfg-packages-materials/deps/ant-design-vue/dayjs/plugin/weekday.js',
  //     'cfg-packages-materials/deps/ant-design-vue/dayjs/plugin/localeData.js',
  //     'cfg-packages-materials/deps/ant-design-vue/dayjs/plugin/weekOfYear.js',
  //     'cfg-packages-materials/deps/ant-design-vue/dayjs/plugin/weekYear.js',
  //     'cfg-packages-materials/deps/ant-design-vue/dayjs/plugin/advancedFormat.js',
  //     'cfg-packages-materials/deps/ant-design-vue/dayjs/plugin/quarterOfYear.js',
  //     'cfg-packages-materials/deps/ant-design-vue/antd.min.js'
  //   ],
  //   assetsUrl: 'cfg-packages-materials/assets/antdv/index.umd.js',
  //   assetsLibrary: 'AntdvMaterial',
  //   required: false,
  //   official: true,
  //   enabled: false
  // },
  {
    package: 'cfg-packages-charts',
    version: 'latest',
    library: 'CfgCharts',
    urls: [
      'cfg-packages-materials/deps/echarts/echarts.min.js',
      'cfg-packages-materials/deps/cfg-packages-charts/index.umd.js'
    ],
    assetsUrl: 'cfg-packages-materials/assets/charts/index.umd.js',
    assetsLibrary: 'CfgChartsMaterial',
    required: false,
    official: true,
    enabled: true
  }
];
