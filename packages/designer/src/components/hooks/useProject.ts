import { computed } from 'vue';
import { useEngine } from '../../framework';
import { useProvider } from 'cfg-packages-renderer';
import { type ProjectSchema } from 'cfg-packages-core';

export function useProject() {
  const engine = useEngine();
  const project = computed(() => engine.project.value);
  return {
    engine,
    project
  };
}

export function useProjectList() {
  const provider = useProvider();
  const getProjectsFiles = async () => {
    if (!provider.service) {
      return [];
    }
    const res = await provider.service.getProjectsFiles().catch(() => {
      return [];
    });
    res.sort((a: any, b: any) => b.__VERSION__ - a.__VERSION__);
    return res || [];
  };
  const isExistProject = (projectList: any[], id: string) => {
    return projectList.find((item) => item.id === id);
  };
  const createProject = async (formData: ProjectSchema, projectList: any[]) => {
    const id: string = formData.id || '';
    const exist = isExistProject(projectList, id);
    if (exist) {
      throw new Error('项目ID已存在，请更换');
    }
    if (!provider.service) {
      return;
    }
    await provider.service.createProjectFile(formData).catch(() => {
      throw new Error('创建项目失败');
    });
  };
  const updateProject = async (formData: ProjectSchema, id: string) => {
    if (!provider.service) {
      return;
    }
    await provider.service.saveProjectFile(id, formData).catch(() => {
      throw new Error('更新项目失败');
    });
  };
  return {
    provider,
    getProjectsFiles,
    createProject,
    updateProject
  };
}
