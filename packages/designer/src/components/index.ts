import Skeleton from './skeleton.vue';
import SetterView from './setter.vue';
import Editor from './editor';
import { widgets } from './widgets';
import { regions } from './regions';
import { setters } from './setters';

export * from './shared';
export * from './binders';

import ProjectList from './project-list.vue';

export { Skeleton, SetterView, widgets, regions, setters, Editor, ProjectList };
