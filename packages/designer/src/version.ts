/**!
 * Copyright (c) 2024, credit-flow-gen CFG All rights reserved.
 * @name cfg-packages-designer
 * @version 0.0.47
 */
export const version = '0.0.47';
