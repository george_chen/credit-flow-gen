import { createViteConfig } from 'cfg-packages-cli';

export default createViteConfig({
  lib: true,
  dts: true,
  version: true,
  formats: ['es'],
  external: [
    'vue',
    'vue-router',
    'element-plus',
    '@element-plus/icons-vue',
    'cfg-packages-renderer',
    'cfg-packages-core',
    'cfg-packages-utils',
    'cfg-packages-icons',
    'cfg-packages-ui',
    'element-plus/es/locale/lang/zh-cn',
    'monaco-editor',
    // 'monaco-editor/esm/vs/editor/editor.worker?worker',
    // 'monaco-editor/esm/vs/language/json/json.worker?worker',
    // 'monaco-editor/esm/vs/language/css/css.worker?worker',
    // 'monaco-editor/esm/vs/language/html/html.worker?worker',
    // 'monaco-editor/esm/vs/language/typescript/ts.worker?worker',
    'mockjs'
  ]
});
