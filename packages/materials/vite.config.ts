import { createViteConfig } from 'cfg-packages-cli';

const BUILD_TYPE = process.env.BUILD_TYPE || '';

const materials = {
  ui: {
    entry: 'src/ui/index.ts',
    library: 'CfgUIMaterial',
    outDir: 'dist/assets/ui'
  },
  element: {
    entry: 'src/element/index.ts',
    library: 'ElementPlusMaterial',
    outDir: 'dist/assets/element'
  },
  // TODO: 后续支持 ant-design 组件库
  /* antdv: {
    entry: 'src/antdv/index.ts',
    library: 'AntdvMaterial',
    outDir: 'dist/assets/antdv'
  }, */
  charts: {
    entry: 'src/charts/index.ts',
    library: 'CfgChartsMaterial',
    outDir: 'dist/assets/charts'
  }
};

function createConfig(name: string) {
  const { entry, library, outDir } = materials[name];
  return createViteConfig({
    library,
    entry,
    outDir,
    lib: true,
    dts: false,
    version: true,
    formats: ['umd'],
    buildTarget: 'es2015',
    external: ['vue', 'vue-router', 'cfg-packages-base', 'cfg-packages-core']
  });
}

export default createConfig(BUILD_TYPE);
