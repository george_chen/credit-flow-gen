/**
 * 通用物料属性
 */
import type { MaterialProp } from 'cfg-packages-core';

export function size(name = 'size'): MaterialProp {
  return {
    name,
    defaultValue: 'default',
    setters: 'SelectSetter',
    options: ['default', 'large', 'small']
  };
}

export function type(name = 'type'): MaterialProp {
  return {
    name,
    defaultValue: 'default',
    setters: 'SelectSetter',
    options: ['default', 'primary', 'success', 'warning', 'danger', 'info']
  };
}
