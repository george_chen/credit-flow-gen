import type { MaterialDescription, MaterialProp } from 'cfg-packages-core';

/**
 * 设置物料中组件的包名
 * @param components
 * @param name
 * @returns
 */
export function setPackageName(components: MaterialDescription[], name: string) {
  return components.map((n) => {
    return {
      ...n,
      package: name
    } as MaterialDescription;
  });
}

/**
 * 从物料属性数组中过滤掉指定名称的属性对象
 * @param props
 * @param names
 * @returns
 */
export function omitPropItem(props: MaterialProp[] = [], names: string[] = []) {
  return props.filter((n) => !names.includes(n.name));
}
