/**!
 * Copyright (c) 2024, credit-flow-gen CFG All rights reserved.
 * @name cfg-packages-materials
 * @version 0.0.49
 */
export const version = '0.0.49';
