import type { MaterialDescription } from 'cfg-packages-core';
import { size } from '../shared';

const desc: MaterialDescription = {
  name: 'XIcon',
  label: '图标',
  categoryId: 'base',
  props: [
    {
      name: 'icon',
      label: '图标',
      setters: 'IconSetter'
    },
    Object.assign(size('size'), { setters: ['SelectSetter', 'NumberSetter'] }),
    {
      name: 'color',
      label: '颜色',
      setters: 'ColorSetter'
    },
    {
      name: 'background',
      label: '背景色',
      setters: 'ColorSetter'
    },
    {
      name: 'src',
      label: '图片Url',
      setters: 'InputSetter'
    },
    {
      name: 'radius',
      label: '圆角值',
      setters: 'NumberSetter'
    },
    {
      name: 'padding',
      label: '内边距',
      setters: 'NumberSetter'
    }
  ],
  snippet: {
    props: {
      icon: 'Star'
    }
  }
};

export default desc;
