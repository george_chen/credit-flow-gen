import type { MaterialDescription } from 'cfg-packages-core';
import dialog from './dialog';
import { omitPropItem, size } from '../shared';

const desc: MaterialDescription = {
  name: 'XDialogForm',
  label: '对话框表单',
  categoryId: 'form',
  props: [
    {
      name: 'modelValue',
      setters: 'BooleanSetter',
      defaultValue: true
    },
    size(),
    {
      name: 'submit',
      setters: ['StringSetter', 'BooleanSetter'],
      defaultValue: '确定'
    },
    {
      name: 'cancel',
      setters: ['StringSetter', 'BooleanSetter'],
      defaultValue: '取消'
    },
    {
      name: 'model',
      setters: 'ObjectSetter'
    },
    {
      name: 'rules',
      setters: 'ObjectSetter'
    },
    {
      name: 'submitMethod',
      title: '表单提交处理方法, return true 关闭对话框',
      setters: 'FunctionSetter'
    },
    ...omitPropItem(dialog.props, ['modelValue', 'size', 'submit', 'cancel'])
  ],
  events: [
    { name: 'update:modelValue', params: ['modelValue'] },
    { name: 'submit', params: ['model'] },
    { name: 'close' }
  ],
  slots: ['default', 'extra', 'handle', 'footer'],
  snippet: {
    props: {
      title: '对话框表单'
    }
  }
};

export default desc;
