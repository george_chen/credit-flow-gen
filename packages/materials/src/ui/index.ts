import type { Material, MaterialCategory, MaterialDescription } from 'cfg-packages-core';
import { version } from '../version';
import { setPackageName } from '../shared';
import test from './test';
import icon from './icon';
import container from './container';
import action from './action';
import actionBar from './action-bar';
import attachment from './attachment';
import qrcode from './qrcode';
import panel from './panel';
import dataItem from './data-item';
import dialog from './dialog';
import dialogForm from './dialog-form';
import field from './field';
import form from './form';
import grid from './grid';
import header from './header';
import list from './list';
import queryForm from './query-form';
import tabs from './tabs';
import picker from './picker';
import imageCodeInput from './image-code-input';

const name = 'cfg-packages-ui';
const components: MaterialDescription[] = [
  test,
  icon,
  container,
  action,
  actionBar,
  qrcode,
  panel,
  attachment,
  dataItem,
  dialog,
  dialogForm,
  field,
  form,
  grid,
  header,
  list,
  queryForm,
  tabs,
  picker,
  imageCodeInput
].flat();

const categories: MaterialCategory[] = [
  {
    id: 'base',
    category: '基础元件'
  },
  {
    id: 'layout',
    category: '布局排版'
  },
  {
    id: 'form',
    category: '表单'
  },
  {
    id: 'data',
    category: '数据展示'
  },
  {
    id: 'test',
    category: '测试套件'
  }
];

const material: Material = {
  name: 'cfg-packages-ui',
  version,
  label: 'UI',
  library: 'CfgUIMaterial',
  order: 1,
  categories,
  components: setPackageName(components, name)
};

export default material;
