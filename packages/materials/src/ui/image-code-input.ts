import type { MaterialDescription } from 'cfg-packages-core';

const desc: MaterialDescription = {
  name: 'XImageCodeInput',
  label: '图形验证码输入框',
  categoryId: 'form',
  props: [
    {
      name: 'image',
      title: '图形加载函数: async () => {}',
      setters: 'FunctionSetter'
    },
    {
      name: 'maxLength',
      label: '输入框最大限制',
      setters: 'NumberSetter'
    },
    {
      name: 'placeholder',
      label: '输入框占位文本',
      setters: 'StringSetter'
    },
    {
      name: 'validate',
      title: '校验输入值函数: async (value) => {}',
      setters: 'FunctionSetter'
    }
  ],
  snippet: {
    props: {}
  }
};

export default desc;
