/**
 * 将：
 * - packages目录下utils、icons、ui、charts模块包的dist目录下的产物
 * - 根目录node_modules目录下的依赖包产物
 * 复制到dist/deps目录下
 */
import { copy } from 'cfg-packages-cli';

const rootModules = '../../node_modules/';
const packages = '../';
const dist = 'dist/';
const files = [
  [packages + 'utils/dist/index.umd.js', 'deps/cfg-packages-utils/index.umd.js'],
  [packages + 'icons/dist/index.umd.js', 'deps/cfg-packages-icons/index.umd.js'],
  [packages + 'icons/dist/style.css', 'deps/cfg-packages-icons/style.css'],
  [packages + 'ui/dist/index.umd.js', 'deps/cfg-packages-ui/index.umd.js'],
  [packages + 'ui/dist/style.css', 'deps/cfg-packages-ui/style.css'],
  [packages + 'charts/dist/index.umd.js', 'deps/cfg-packages-charts/index.umd.js'],

  [rootModules + 'vue/dist/vue.global.prod.js', 'deps/vue/vue.global.prod.js'],
  [rootModules + 'vue-router/dist/vue-router.global.prod.js', 'deps/vue-router/vue-router.global.prod.js'],
  [rootModules + 'element-plus/dist/index.full.min.js', 'deps/element-plus/index.full.min.js'],
  [rootModules + 'element-plus/dist/locale/zh-cn.js', 'deps/element-plus/zh-cn.js'],
  [rootModules + 'element-plus/theme-chalk/dark/css-vars.css', 'deps/element-plus/dark/css-vars.css'],
  [rootModules + 'element-plus/dist/index.css', 'deps/element-plus/index.css'],
  [rootModules + 'echarts/dist/echarts.min.js', 'deps/echarts/echarts.min.js'],
  [rootModules + '@vueuse/core/index.iife.min.js', 'deps/@vueuse/core/index.iife.min.js'],
  [rootModules + '@vueuse/shared/index.iife.min.js', 'deps/@vueuse/shared/index.iife.min.js']

  // TODO: 后续支持 ant-design 组件库
  /* [rootModules + 'ant-design-vue/dist/antd.min.js', 'deps/ant-design-vue/antd.min.js'],
  [rootModules + 'ant-design-vue/dist/reset.css', 'deps/ant-design-vue/reset.css'],
  [rootModules + 'dayjs/dayjs.min.js', 'deps/ant-design-vue/dayjs/dayjs.min.js'],
  [rootModules + 'dayjs/plugin/customParseFormat.js', 'deps/ant-design-vue/dayjs/plugin/customParseFormat.js'],
  [rootModules + 'dayjs/plugin/weekday.js', 'deps/ant-design-vue/dayjs/plugin/weekday.js'],
  [rootModules + 'dayjs/plugin/localeData.js', 'deps/ant-design-vue/dayjs/plugin/localeData.js'],
  [rootModules + 'dayjs/plugin/weekOfYear.js', 'deps/ant-design-vue/dayjs/plugin/weekOfYear.js'],
  [rootModules + 'dayjs/plugin/weekYear.js', 'deps/ant-design-vue/dayjs/plugin/weekYear.js'],
  [rootModules + 'dayjs/plugin/advancedFormat.js', 'deps/ant-design-vue/dayjs/plugin/advancedFormat.js'],
  [rootModules + 'dayjs/plugin/quarterOfYear.js', 'deps/ant-design-vue/dayjs/plugin/quarterOfYear.js'], */
];

function doCopy() {
  for (const item of files) {
    copy(item[0], dist + item[1]);
  }
}

doCopy();
