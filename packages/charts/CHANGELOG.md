# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.51](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.50...cfg-packages-charts@0.0.51) (2024-10-23)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.50](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.49...cfg-packages-charts@0.0.50) (2024-10-12)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.49](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.48...cfg-packages-charts@0.0.49) (2024-10-12)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.48](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.47...cfg-packages-charts@0.0.48) (2024-10-12)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.47](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.46...cfg-packages-charts@0.0.47) (2024-10-12)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.46](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.45...cfg-packages-charts@0.0.46) (2024-10-12)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.45](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.44...cfg-packages-charts@0.0.45) (2024-10-11)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.44](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.43...cfg-packages-charts@0.0.44) (2024-10-11)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.43](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.42...cfg-packages-charts@0.0.43) (2024-10-10)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.42](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.41...cfg-packages-charts@0.0.42) (2024-09-13)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.41](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.40...cfg-packages-charts@0.0.41) (2024-09-13)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.40](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.39...cfg-packages-charts@0.0.40) (2024-09-13)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.39](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.38...cfg-packages-charts@0.0.39) (2024-09-13)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.38](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.37...cfg-packages-charts@0.0.38) (2024-09-10)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.37](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.36...cfg-packages-charts@0.0.37) (2024-09-01)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.36](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.35...cfg-packages-charts@0.0.36) (2024-08-30)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.35](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.34...cfg-packages-charts@0.0.35) (2024-08-30)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.34](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.33...cfg-packages-charts@0.0.34) (2024-08-29)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.33](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.32...cfg-packages-charts@0.0.33) (2024-08-22)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.32](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.31...cfg-packages-charts@0.0.32) (2024-08-21)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.31](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.30...cfg-packages-charts@0.0.31) (2024-08-20)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.30](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.29...cfg-packages-charts@0.0.30) (2024-08-20)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.29](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.28...cfg-packages-charts@0.0.29) (2024-08-20)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.28](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.27...cfg-packages-charts@0.0.28) (2024-08-20)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.27](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.26...cfg-packages-charts@0.0.27) (2024-08-20)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.26](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.25...cfg-packages-charts@0.0.26) (2024-08-12)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.25](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.24...cfg-packages-charts@0.0.25) (2024-08-12)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.24](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.23...cfg-packages-charts@0.0.24) (2024-08-12)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.23](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.22...cfg-packages-charts@0.0.23) (2024-08-11)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.22](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.21...cfg-packages-charts@0.0.22) (2024-08-11)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.21](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.20...cfg-packages-charts@0.0.21) (2024-08-11)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.20](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.19...cfg-packages-charts@0.0.20) (2024-08-11)

### Features

- 开发完成，待发版 ([dfb56fd](https://gitee.com/george_chen/credit-flow-gen/commits/dfb56fdf7a7b478ccb1227b7ea9fe3b416e9b402))

## [0.0.19](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.18...cfg-packages-charts@0.0.19) (2024-08-11)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.18](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.17...cfg-packages-charts@0.0.18) (2024-08-09)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.17](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.16...cfg-packages-charts@0.0.17) (2024-08-09)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.16](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.15...cfg-packages-charts@0.0.16) (2024-08-09)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.15](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.12...cfg-packages-charts@0.0.15) (2024-08-09)

### Features

- 项目导出完善完成，准备发版 ([d744af6](https://gitee.com/george_chen/credit-flow-gen/commits/d744af60ea8cec90f266a0cc25c5634c41adbf0b))
- 项目导出完善完成，准备发版 ([66a970e](https://gitee.com/george_chen/credit-flow-gen/commits/66a970e777f06024a270c9be41aa6c26a165535f))

## [0.0.14](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.12...cfg-packages-charts@0.0.14) (2024-08-09)

### Features

- 项目导出完善完成，准备发版 ([66a970e](https://gitee.com/george_chen/credit-flow-gen/commits/66a970e777f06024a270c9be41aa6c26a165535f))

## [0.0.13](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.12...cfg-packages-charts@0.0.13) (2024-08-09)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.12](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.11...cfg-packages-charts@0.0.12) (2024-07-23)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.11](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.10...cfg-packages-charts@0.0.11) (2024-07-23)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.10](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.9...cfg-packages-charts@0.0.10) (2024-07-23)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.9](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.8...cfg-packages-charts@0.0.9) (2024-07-22)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.8](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.7...cfg-packages-charts@0.0.8) (2024-07-08)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.7](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.6...cfg-packages-charts@0.0.7) (2024-07-06)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.6](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.5...cfg-packages-charts@0.0.6) (2024-07-01)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.5](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.4...cfg-packages-charts@0.0.5) (2024-07-01)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.4](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.3...cfg-packages-charts@0.0.4) (2024-06-25)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.3](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.2...cfg-packages-charts@0.0.3) (2024-06-14)

**Note:** Version bump only for package cfg-packages-charts

## [0.0.2](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-charts@0.0.1...cfg-packages-charts@0.0.2) (2024-06-13)

### Features

- 完成 cfg-packages-ui 开发，准备发布 npm 包 ([96db65a](https://gitee.com/george_chen/credit-flow-gen/commits/96db65aa0ff114e77dba06ef85f7b2ae35d30de2))

## 0.0.1 (2024-06-03)

### Features

- 完成 cfg-packages-charts npm 包开发 ([62e90fd](https://gitee.com/george_chen/credit-flow-gen/commits/62e90fd7747705fe7b45876e02c56ca969d4e532))
