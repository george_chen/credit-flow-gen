/**!
 * Copyright (c) 2024, credit-flow-gen CFG All rights reserved.
 * @name cfg-packages-charts
 * @version 0.0.51
 */
export const version = '0.0.51';
