import { type MaybeRef, type Ref, onMounted, onUnmounted, ref, unref, watch, markRaw } from 'vue';

import { useResizeObserver } from '@vueuse/core';
import type { ECharts, EChartsOption } from 'echarts';
import { debounce } from 'cfg-packages-utils';

/**
 * 组合式函数 - 创建和管理 ECharts 图表实例
 * 处理图表的初始化、配置更新和尺寸调整，同时在组件卸载时进行清理
 * @param echarts
 * @param el
 * @param option
 * @returns
 */
export function useChart(echarts: any, el: MaybeRef<HTMLElement>, option: Ref<EChartsOption | undefined>) {
  const echartsInstance = ref<ECharts | undefined>();

  onMounted(() => {
    const target = unref(el);
    if (!target) return;
    if (!echartsInstance.value) {
      echartsInstance.value = markRaw(echarts.init(target));
    }
    if (echartsInstance.value) {
      echartsInstance.value.setOption(option.value || {});
    }
  });

  onUnmounted(() => {
    if (echartsInstance.value) {
      echartsInstance.value.dispose();
    }
  });

  watch(
    option,
    (o) => {
      if (echartsInstance.value) {
        echartsInstance.value.setOption(o || {});
      }
    },
    { deep: true }
  );

  useResizeObserver(
    el,
    debounce(() => {
      if (echartsInstance.value) {
        echartsInstance.value.resize();
      }
    }, 150)
  );

  return {
    echartsInstance
  };
}
