# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.8](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-create_cfg@0.0.7...cfg-packages-create_cfg@0.0.8) (2024-10-23)

**Note:** Version bump only for package cfg-packages-create_cfg

## [0.0.7](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-create_cfg@0.0.6...cfg-packages-create_cfg@0.0.7) (2024-10-12)

### Features

- 更新 create-cfg-app-template ([ce08d42](https://gitee.com/george_chen/credit-flow-gen/commits/ce08d42fc8d9aaf2bd798177d801b29b085e0725))

## [0.0.6](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-create_cfg@0.0.5...cfg-packages-create_cfg@0.0.6) (2024-10-12)

### Features

- pkg.json 中 devDependencies 依赖项放到 dependencies 中 ([6343215](https://gitee.com/george_chen/credit-flow-gen/commits/63432152bc2c3e8d2f2eae255fad6031974e0862))

## [0.0.5](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-create_cfg@0.0.4...cfg-packages-create_cfg@0.0.5) (2024-10-12)

### Features

- 完善 apps/web 和 create-cfg 脚手架下的 App.vue 文件，使之可以执行 npm run build 命令 ([dac659f](https://gitee.com/george_chen/credit-flow-gen/commits/dac659f8097c872070b7862a28f1c19c6cd5467c))

## [0.0.4](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-create_cfg@0.0.3...cfg-packages-create_cfg@0.0.4) (2024-10-11)

### Features

- 更新 cfg-packages-create_cfg 体验低代码 CLI 工具的 vite.config.ts ([111b36f](https://gitee.com/george_chen/credit-flow-gen/commits/111b36f9d604f70e46a895af685079bd817ce8e5))

## [0.0.3](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-create_cfg@0.0.2...cfg-packages-create_cfg@0.0.3) (2024-09-01)

### Features

- 完善 create-cfg 增加 sass ([b1be67e](https://gitee.com/george_chen/credit-flow-gen/commits/b1be67ea625dccf3240226c6e850a896f0bcef95))

## [0.0.2](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-create_cfg@0.0.1...cfg-packages-create_cfg@0.0.2) (2024-08-30)

### Features

- 完善 create-cfg 和部分 readme 文件 ([876a2c6](https://gitee.com/george_chen/credit-flow-gen/commits/876a2c62c3f4b1cbf43366750092472df708d7bb))

## 0.0.1 (2024-08-30)

### Features

- 完成 cfg-packages-create_cfg 体验低代码 CLI 工具开发，准备发布到 npm ([c3df64e](https://gitee.com/george_chen/credit-flow-gen/commits/c3df64ed3d5ba03ec8025e2b3c796660c2e9be63))
