import path from 'node:path';
import { fileURLToPath } from 'node:url';
import minimist from 'minimist';
import { red } from 'kolorist';
import { emptyDirSync, pathExistsSync, ensureDirSync } from 'cfg-packages-node';
import { createOptions, templates } from './options';
import { pkgFromUserAgent } from './utils';
import { createProject } from './generator';

// 读取命令行参数
const cwd = process.cwd();
const argv = minimist<{
  t?: string;
  template?: string;
}>(process.argv.slice(2), { string: ['_'] });
const argTemplate = argv.template || argv.t;
// 默认存放项目的目录名
const defaultTargetDir = 'cfg-project';

function getTemplatePath(name: string) {
  return path.resolve(fileURLToPath(import.meta.url), '../..', 'templates', name);
}

/**
 * 主函数
 * @returns
 */
async function init() {
  if (argTemplate) {
    const match = templates.find((item) => item.name === argTemplate);
    if (!match) {
      console.log(`\n  ${red('Error:')} 不存在项目模板: "${argTemplate}"`);
      return;
    }
  }
  // 1. 创建项目的CLI选项
  const options = await createOptions(defaultTargetDir, argTemplate);
  const templateName = argTemplate || options.template;
  const root = path.join(cwd, options.packageName);
  const pkgInfo = pkgFromUserAgent(process.env.npm_config_user_agent);
  const pkgManager = pkgInfo ? pkgInfo.name : 'npm';
  if (pathExistsSync(root)) {
    if (options.overwrite) {
      emptyDirSync(root);
    }
  } else {
    ensureDirSync(root);
  }
  // 2. 根据入参生成项目
  createProject({
    name: options.packageName,
    template: getTemplatePath(templateName),
    root
  });
  // 3. 打印提示信息
  console.log(`\n项目已生成，执行以下命令本地运行项目：\n`);
  if (root !== cwd) {
    console.log(`  cd ${path.relative(cwd, root)}`);
  }
  switch (pkgManager) {
    case 'yarn':
      console.log('  yarn');
      console.log('  yarn dev');
      break;
    default:
      console.log(`  ${pkgManager} install --registry=https://registry.npmmirror.com`);
      console.log(`  ${pkgManager} run dev`);
      break;
  }
  console.log();
}

// 执行主函数
init().catch((e) => {
  console.error(e);
  process.exit(1);
});
