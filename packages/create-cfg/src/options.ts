import fs from 'node:fs';
import prompts from 'prompts';
import { green, red, reset } from 'kolorist';
import { isValidPackageName, formatTargetDir, isEmpty } from './utils';

export interface ITemplate {
  name: string;
  display: string;
  color: (str: string | number) => string;
}

export const templates: ITemplate[] = [
  {
    name: 'app',
    display: 'Web应用( app )',
    color: green
  }
];

/**
 * 创建项目的CLI选项，并引导用户执行一系列创建项目的操作
 * @param defaultTargetDir
 * @param templateName
 * @returns
 */
export async function createOptions(defaultTargetDir: string, templateName = '') {
  let targetDir: string = defaultTargetDir;
  const result: prompts.Answers<'template' | 'packageName' | 'overwrite'> = await prompts(
    [
      {
        type: templateName ? null : 'select',
        name: 'template',
        message: reset('请选择项目模板:'),
        initial: 0,
        choices: templates.map((template) => {
          return {
            title: template.color(template.display || template.name),
            value: template.name
          };
        })
      },
      {
        type: 'text',
        name: 'packageName',
        message: reset('定义项目目录名称:'),
        initial: defaultTargetDir,
        onState: (state: any) => {
          targetDir = formatTargetDir(state.value) || defaultTargetDir;
        },
        validate: (dir: string) => isValidPackageName(dir) || '项目目录名称不合法，请更换！'
      },
      {
        type: () => (!fs.existsSync(targetDir) || isEmpty(targetDir) ? null : 'confirm'),
        name: 'overwrite',
        message: () =>
          (targetDir === '.' ? '当前目录' : `目标目录 "${targetDir}"`) + ` 不是一个空的文件夹，是否清空文件并继续创建？`
      }
    ],
    {
      onCancel: () => {
        throw new Error(red('✖') + ' Operation cancelled');
      }
    }
  );

  return result;
}
