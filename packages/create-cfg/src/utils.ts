import fs from 'node:fs';

/**
 * 格式化目标目录
 * @param targetDir
 * @returns
 */
export function formatTargetDir(targetDir: string | undefined) {
  return targetDir?.trim().replace(/\/+$/g, '');
}

/**
 * 判断给定path的目录是否为空
 * @param path
 * @returns
 */
export function isEmpty(path: string) {
  const files = fs.readdirSync(path);
  return files.length === 0 || (files.length === 1 && files[0] === '.git');
}

/**
 * 是否合法包名称
 * @param pkgName
 * @returns
 */
export function isValidPackageName(pkgName: string) {
  return /^(?:@[a-z\d\-*~][a-z\d\-*._~]*\/)?[a-z\d\-~][a-z\d\-._~]*$/.test(pkgName);
}

/**
 * 合法化包名
 * @param pkgName
 * @returns
 */
export function toValidPackageName(pkgName: string) {
  return pkgName
    .trim()
    .toLowerCase()
    .replace(/\s+/g, '-')
    .replace(/^[._]/, '')
    .replace(/[^a-z\d\-~]+/g, '-');
}

/**
 * 从本机代理中找到包管理工具的名称和版本号
 * @param userAgent
 * @returns
 */
export function pkgFromUserAgent(userAgent: string | undefined) {
  if (!userAgent) return undefined;
  const pkgSpec = userAgent.split(' ')[0];
  const pkgSpecArr = pkgSpec.split('/');
  return {
    name: pkgSpecArr[0],
    version: pkgSpecArr[1]
  };
}
