/// <reference types="vite/client" />

declare module '*.vue' {
  import type { DefineComponent } from 'vue';
  // eslint-disable-next-line @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

declare namespace NodeJS {
  interface ProcessEnv {
    NODE_ENV: 'development' | 'production';
    ENV_TYPE?: string;
    [key: string]: any;
  }
}

// eslint-disable-next-line @typescript-eslint/prefer-namespace-keyword
declare module global {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface Window {}
}

declare module 'vue' {
  interface ComponentCustomProperties {
    $uploader: any;
    $reqeust: any;
    $apis: any;
    $libs: any;
  }
}

export {};
