import { createRouter, createWebHashHistory } from 'vue-router';
import { ProjectListWrapper } from 'cfg-packages-designer';

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/project-list',
      name: 'CfgProjectList',
      component: ProjectListWrapper
    },
    {
      path: '/:pathMatch(?!.*\\/page)(.*)*',
      name: 'NotFound',
      component: () => import('@/views/not-found.vue')
    }
  ]
});

export default router;
