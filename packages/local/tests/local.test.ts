import { expect, test } from 'vitest';
import { createDevTools } from '../dist/index.mjs';

test('createDevTools', () => {
  const plugins = createDevTools();
  const aliasPluginName = 'cfg-alias-plugin';
  const targetPlugin: any = plugins.find((plugin) => plugin.name === aliasPluginName);
  expect(targetPlugin.name).toBe(aliasPluginName);
});
