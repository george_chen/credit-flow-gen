/**
 * 本地开发环境Api返回数据结果定义
 */

export interface ApiRequest {
  type: string;
  data: any;
  origin?: string; // 请求来源，如：http://localhost:3010
}

export interface ApiResponse {
  code: number;
  msg: string;
  data: any;
  success: boolean;
}

export enum Result {
  Success = 0,
  Fail = 1
}

export const success = (data: any): ApiResponse => {
  return {
    code: Result.Success,
    msg: 'success',
    data,
    success: true
  };
};

export const fail = (msg: string, data: any = null): ApiResponse => {
  return {
    code: Result.Fail,
    msg,
    data,
    success: false
  };
};
