import { type ProjectSchema, type BlockSchema, type HistorySchema, type PageFile } from 'cfg-packages-core';
import formidable from 'formidable';
import { type ApiRequest, type ApiResponse, fail } from './shared';
import * as service from './service';
import type { DevToolsOptions } from './plugin';
import { resolve } from 'path';

export interface Controller {
  [index: string]: (req: ApiRequest, opts: DevToolsOptions) => Promise<ApiResponse>;
}

/**
 * controller层
 */
const controller: Controller = {
  notMatch: async (_req: ApiRequest) => {
    return fail('找不到处理程序', _req);
  },
  getExtension: service.getExtension,
  init: async (req: ApiRequest, opts: DevToolsOptions) => {
    const project = req.data as ProjectSchema;
    return service.init(project, opts);
  },
  saveProject: async (req: ApiRequest) => {
    const project = req.data as ProjectSchema;
    return service.saveProject(project);
  },
  saveFile: async (req: ApiRequest) => {
    const file = req.data as BlockSchema;
    return service.saveFile(file);
  },
  getFile: async (req: ApiRequest) => {
    const id = req.data as string;
    return service.getFile(id);
  },
  removeFile: async (req: ApiRequest) => {
    const id = req.data as string;
    return service.removeFile(id);
  },
  getHistory: async (req: ApiRequest) => {
    const id = req.data as string;
    return service.getHistory(id);
  },
  saveHistory: async (req: ApiRequest) => {
    const file = req.data as HistorySchema;
    return service.saveHistory(file);
  },
  removeHistory: async (req: ApiRequest) => {
    const id = req.data as string;
    return service.removeHistory(id);
  },
  getHistoryItem: async (req: ApiRequest) => {
    const { fId, id } = req.data || {};
    return service.getHistoryItem(fId, id);
  },
  saveHistoryItem: async (req: ApiRequest) => {
    const { fId, item } = req.data || {};
    return service.saveHistoryItem(fId, item);
  },
  removeHistoryItem: async (req: ApiRequest) => {
    const { fId, ids = [] } = req.data || {};
    return service.removeHistoryItem(fId, ids);
  },
  saveMaterials: async (req: ApiRequest) => {
    const { project, materials } = req.data || {};
    return service.saveMaterials(project, materials);
  },
  publishFile: async (req: ApiRequest) => {
    const { project, file } = req.data || {};
    return service.publishFile(project, file);
  },
  publish: async (req: ApiRequest) => {
    const project = req.data || {};
    return service.publish(project);
  },
  genVueContent: async (req: ApiRequest) => {
    const { project, dsl } = req.data || {};
    return service.genVueContent(project, dsl);
  },
  createRawPage: async (req: ApiRequest) => {
    const file = req.data as PageFile;
    return service.createRawPage(file);
  },
  removeRawPage: async (req: ApiRequest) => {
    const id = req.data as string;
    return service.removeRawPage(id);
  },
  getStaticFiles: async (_req: ApiRequest, opts?: DevToolsOptions) => {
    return service.getStaticFiles(opts as any);
  },
  removeStaticFile: async (req: ApiRequest, opts?: DevToolsOptions) => {
    const name = req.data?.name as string;
    return service.removeStaticFile(name, opts as any);
  },
  clearStaticFiles: async (_req: ApiRequest, opts?: DevToolsOptions) => {
    return service.clearStaticFiles(opts as any);
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  getProjectsFiles: async (_req: ApiRequest) => {
    return service.getProjectsFiles();
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  getProject: async (req: ApiRequest) => {
    const id = req.data as string;
    return service.getProject(id);
  },
  createProjectFile: async (req: ApiRequest) => {
    const form = req.data as PageFile;
    return service.createProjectFile(form);
  },
  saveProjectFile: async (req: ApiRequest) => {
    const { id, form } = req.data || {};
    return service.saveProjectFile(id, form);
  },
  /**
   * 静态文件上传
   * @param req
   * @param opts
   * @returns
   */
  uploader: async (req: any, opts?: DevToolsOptions) => {
    if (!opts) return fail('异常错误');
    const uploadDir = resolve(opts.staticDir, opts.cfgDir);
    const form = formidable({
      keepExtensions: true,
      multiples: true,
      createDirsFromUploads: true,
      uploadDir
    });
    return await new Promise<ApiResponse>((reslove) => {
      form.parse(req, (err, _fields, files) => {
        if (err) {
          reslove(fail('异常错误', err));
          return;
        }
        const tempFiles = files.files || [];
        const result = service.uploadStaticFiles(tempFiles, opts as any);
        reslove(result);
      });
    });
  },
  exportPrj: async (req: ApiRequest) => {
    const project = req.data || {};
    const origin = req.origin;
    return service.exportPrj(project, origin);
  }
};

/**
 * router层
 * @param req
 * @param opts
 * @returns
 */
export const router = async (req: any, opts: DevToolsOptions) => {
  const body: ApiRequest = req.body || {};
  const reqUrl = req.url || '';
  const uploaderPath = `${opts.baseURL}${opts.uploader}`;
  const isUploader = reqUrl.startsWith(uploaderPath);
  // 调用上传接口，走静态文件上传处理逻辑
  if (isUploader) {
    return await controller.uploader(req, opts);
  } else {
    const origin = req.headers.origin || '';
    body.origin = origin;
    const handler = controller[body.type] || controller.notMatch;
    try {
      return await handler(body, opts);
    } catch (e) {
      const info = {
        input: body,
        error: e
      };
      await service.saveLogs(info);
      return fail('异常错误', e);
    }
  }
};
