/**
 * service层
 */
import {
  ProjectModel,
  type ProjectSchema,
  type BlockSchema,
  type HistorySchema,
  type HistoryItem,
  type MaterialDescription,
  type PageFile,
  type BlockFile
} from 'cfg-packages-core';
import { resolve } from 'path';
import { readJsonSync, upperFirstCamelCase, timestamp } from 'cfg-packages-node';
import { generator, createEmptyPage } from 'cfg-packages-coder';
import formidable from 'formidable';
import { fail, success, type ApiRequest } from './shared';
import {
  JsonRepository,
  VueRepository,
  StaticRepository,
  PluginRepository,
  ProjectExport,
  type StaticRepositoryOptions
} from './repository';
import type { DevToolsOptions } from './plugin';
import open from 'open';

let isInit = false;

export async function notMatch(_req: ApiRequest) {
  return fail('找不到处理程序', _req);
}

export async function saveLogs(e: any) {
  const name = `error-${timestamp()}`;
  const logs = new JsonRepository('logs');
  const json = JSON.parse(JSON.stringify(e));
  return logs.save(name, json);
}

/**
 * 从项目的 package.json 中读取项目的extension
 * @returns
 */
export async function getExtension() {
  const root = resolve('./');
  const pkg = readJsonSync(resolve(root, 'package.json'));
  // console.log(`local service getExtension pkg: ${JSON.stringify(pkg)}`);
  const { cfg = {} } = pkg || {};
  // console.log(`local service getExtension cfg: ${JSON.stringify(cfg)}`);
  return success(cfg.extension || null);
}

export async function init(prjParams: any, opts: DevToolsOptions) {
  const root = resolve('./');
  const pkg = readJsonSync(resolve(root, 'package.json'));
  const repository = new JsonRepository('projects');
  const pluginPepository = new PluginRepository(pkg, opts);

  // 从项目的 package.json 中读取项目信息
  // console.log(`local service init pkg: ${JSON.stringify(pkg)}`);
  const { cfg = {} } = pkg || {};
  // console.log(`local service init cfg: ${JSON.stringify(cfg)}`);
  const id = prjParams.id || cfg.id || pkg.name;
  const name = prjParams.name || cfg.name || pkg.name || upperFirstCamelCase(id);
  const description = prjParams.description || cfg.description || pkg.description || '';
  const creator = prjParams.creator || '';

  // 如果项目文件已经存在，则直接返回文件内容
  let dsl: ProjectSchema = repository.get(id);
  const plugins = pluginPepository.getPlugins();
  if (dsl) {
    const blocks = (dsl.blocks || []).filter((n: BlockFile) => !n.preset);
    dsl.blocks = plugins.concat(blocks);
    Object.assign(dsl, { id, name, description });
    if (!isInit) {
      isInit = true;
      repository.save(id, dsl);
    }
    return success(dsl);
  } else {
    const model = new ProjectModel({
      id,
      name,
      description,
      creator,
      blocks: plugins
    });
    dsl = model.toDsl();
    repository.save(id, dsl);
    return success(dsl);
  }
}

export async function saveProject(dsl: ProjectSchema) {
  const repository = new JsonRepository('projects');
  if (repository.exist(dsl.id as string)) {
    const ret = repository.save(dsl.id as string, dsl);
    return success(ret);
  } else {
    return fail('项目文件不存在');
  }
}

export async function saveFile(dsl: BlockSchema) {
  const repository = new JsonRepository('files');
  const ret = repository.save(dsl.id as string, dsl);
  return success(ret);
}

export async function getFile(id: string) {
  const repository = new JsonRepository('files');
  const json = repository.get(id);
  if (json) {
    return success(json);
  } else {
    return fail('文件不存在');
  }
}

export async function removeFile(id: string) {
  const repository = new JsonRepository('files');
  const ret = repository.remove(id);
  return success(ret);
}

export async function getHistory(id: string) {
  const repository = new JsonRepository('histories');
  const json = repository.get(id);
  if (json) {
    return success(json);
  } else {
    return success({});
  }
}

export async function saveHistory(file: HistorySchema) {
  const repository = new JsonRepository('histories');
  const ret = repository.save(file.id as string, file);
  return success(ret);
}

export async function removeHistory(id: string) {
  const repository = new JsonRepository('histories');
  const items = new JsonRepository(`histories/${id}`);
  items.clear();
  repository.remove(id);
  return success(true);
}

export async function getHistoryItem(fId: string, id: string) {
  const repository = new JsonRepository(`histories/${fId}`);
  const json = repository.get(id);
  if (json) {
    return success(json);
  } else {
    return fail('文件不存在');
  }
}

export async function saveHistoryItem(fId: string, item: HistoryItem) {
  const repository = new JsonRepository(`histories/${fId}`);
  repository.save(item.id, item);
  return success(true);
}

export async function removeHistoryItem(fId: string, ids: string[]) {
  const repository = new JsonRepository(`histories/${fId}`);

  ids.forEach((id: string) => {
    repository.remove(id);
  });

  return success(true);
}

export async function saveMaterials(project: ProjectSchema, materials: Record<string, MaterialDescription>) {
  const repository = new JsonRepository('materials');
  repository.save(project.id as string, materials);
  return success(true);
}

/**
 * 页面发布
 * @param project
 * @param file
 * @param componentMap
 * @returns
 */
export async function publishFile(
  project: ProjectSchema,
  file: PageFile | BlockFile,
  componentMap?: Map<string, MaterialDescription>
) {
  const materialsRepository = new JsonRepository('materials');
  const materials = materialsRepository.get(project.id as string);
  componentMap = componentMap || new Map<string, MaterialDescription>(Object.entries(materials || {}));
  const fileRepository = new JsonRepository('files');
  const dsl = fileRepository.get(file.id as string);
  if (dsl) {
    const content = await generator(dsl, componentMap, project.dependencies);
    const vueRepository = new VueRepository();
    vueRepository.save(file.id as string, content);
    const filePath = `文件位置：${vueRepository.getFilePath(file.id as string)}`;
    return success(filePath);
  }
  return fail('文件不存在');
}

/**
 * 项目发布
 * @param project
 * @returns
 */
export async function publish(project: ProjectSchema) {
  const { pages = [], blocks = [] } = project;
  const materialsRepository = new JsonRepository('materials');
  const materials = materialsRepository.get(project.id as string);
  const componentMap = new Map<string, MaterialDescription>(Object.entries(materials));

  for (const block of blocks) {
    if (!block.fromType || block.fromType === 'Schema') {
      await publishFile(project, block, componentMap);
    }
  }
  for (const page of pages) {
    if (!page.raw) {
      await publishFile(project, page, componentMap);
    }
  }

  return success('项目路径：/.cfg');
}

export async function genVueContent(project: ProjectSchema, dsl: BlockSchema) {
  const materialsRepository = new JsonRepository('materials');
  const materials = materialsRepository.get(project.id as string);
  const componentMap = new Map<string, MaterialDescription>(Object.entries(materials));

  const content = await generator(dsl, componentMap, project.dependencies);
  return success(content);
}

export async function createRawPage(file: PageFile) {
  const repository = new VueRepository();
  const page = await createEmptyPage(file);
  repository.save(file.id as string, page);
  return success(true);
}

export async function removeRawPage(id: string) {
  const repository = new VueRepository();
  repository.remove(id);
  return success(true);
}

export async function uploadStaticFiles(files: formidable.File[], options: StaticRepositoryOptions) {
  const repository = new StaticRepository(options);
  const error = repository.validate(files);
  if (error) {
    return fail('文件名称已存在', error);
  }
  const res = repository.save(files);
  return success(res);
}

export async function removeStaticFile(filename: string, options: StaticRepositoryOptions) {
  const repository = new StaticRepository(options);
  const ret = repository.remove(filename);
  return ret ? success(true) : fail('删除失败');
}

export async function getStaticFiles(options: StaticRepositoryOptions) {
  const repository = new StaticRepository(options);
  return success(repository.getAllFiles());
}

export async function clearStaticFiles(options: StaticRepositoryOptions) {
  const repository = new StaticRepository(options);
  return success(repository.clear());
}

export async function getProjectsFiles() {
  const projects = new JsonRepository('projects');
  return success(projects.getAllFiles());
}

export async function getProject(id: string) {
  const projects = new JsonRepository('projects');
  const json = projects.get(id);
  if (json) {
    return success(json);
  } else {
    return success({});
  }
}

export async function createProjectFile(form: ProjectSchema) {
  const { name, description = '', creator } = form;
  const repository = new JsonRepository('projects');
  const model = new ProjectModel({
    id: form.id,
    name,
    description,
    creator,
    blocks: []
  });
  const dsl = model.toDsl();
  repository.save(form.id as string, dsl);
  return success(true);
}

export async function saveProjectFile(id: string, form: ProjectSchema) {
  const repository = new JsonRepository('projects');
  const json = repository.get(id);
  json.name = form.name;
  json.description = form.description;
  json.creator = form.creator;
  json.__VERSION__ = timestamp().toString();
  repository.save(id as string, json);
  return success(true);
}

/**
 * 项目导出
 * @param project
 * @param origin
 * @returns
 */
export async function exportPrj(project: ProjectSchema, origin = '') {
  // 1. 先执行项目发布
  try {
    const publishRet = await publish(project);
    if (!publishRet) {
      return fail('项目发布失败，请检查项目', publishRet);
    }
  } catch (e) {
    console.error(e);
    return fail('项目发布失败，请检查项目', e);
  }
  const { id = '', pages = [] } = project;
  if (!Array.isArray(pages) || !pages.length) {
    return fail('页面文件不存在');
  }
  // 2. 导出项目实例化&初始化
  let prjExport;
  try {
    prjExport = new ProjectExport(id);
  } catch (e) {
    console.error(e);
    return fail('导出项目实例化&初始化出错', e);
  }
  // 3. .cfg/projects/${项目id}.json -> .exports/${相应项目}/.cfg/projects/${项目id}.json
  try {
    prjExport.handleProjectsSettingFile(id);
  } catch (e) {
    console.error(e);
    return fail('拷贝项目配置文件出错', e);
  }
  // // 4. .cfg/materials/${项目id}.json -> .exports/${相应项目}/.cfg/materials/${项目id}.json
  // try {
  //   prjExport.handleProjectsMaterialsFile(id);
  // } catch (e) {
  //   console.error(e);
  //   return fail('拷贝项目物料文件出错', e);
  // }
  // 5. 该项目下.cfg/files -> .exports/.cfg/files 和 .cfg/vue -> .exports/.cfg/vue
  try {
    prjExport.handleDslFilesAndVueFiles(id);
  } catch (e) {
    console.error(e);
    return fail('拷贝DSL文件和Vue文件出错', e);
  }
  // 6. 处理路由配置文件/router
  const { homepage = '' } = project;
  try {
    prjExport.handleRouter(pages, id, homepage);
  } catch (e) {
    console.error(e);
    return fail('处理路由配置文件/router出错', e);
  }
  // 7. 生成组合式函数/hooks
  try {
    prjExport.createHooksUse(pages);
  } catch (e) {
    console.error(e);
    return fail('生成组合式函数/hooks出错', e);
  }
  // 8. 生成入口文件index.ts
  try {
    prjExport.createIndexFile(id);
  } catch (e) {
    console.error(e);
    return fail('生成入口文件index.ts出错', e);
  }
  const prjPath = prjExport.getPrjPath(id);
  // 9. 压缩文件
  try {
    await prjExport.zipFile(prjPath);
  } catch (e) {
    console.error(e);
    return fail('压缩文件出错', e);
  }
  // 10. 下载压缩包
  try {
    open(`${origin}/${id}.zip`);
  } catch (e) {
    console.error(e);
    return fail('打开下载链接出错', e);
  }
  return success(`${origin}/${id}.zip`);
}
