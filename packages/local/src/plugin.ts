import { type Plugin, type ResolvedConfig, type ViteDevServer, type PreviewServer } from 'vite';
import { copyPlugin, staticPlugin, type CopyPluginOption, type StaticPluginOption } from 'cfg-packages-cli';
import { pathExistsSync, readJsonSync } from 'cfg-packages-node';
import { join, resolve } from 'path';
import bodyParser from 'body-parser';
import { router } from './controller';

export interface DevToolsOptions {
  baseURL: string;
  copy: boolean;
  server: boolean;
  staticBase: string;
  staticDir: string;
  link: boolean | string;
  linkOptions: LinkOptions | null;
  cfgDir: string;
  packagesDir: string;
  devMode: boolean;
  uploader: string;
  packageName: string;
  nodeModulesDir: string;
  presetPlugins: string[];
  pluginNodeModulesDir?: string;
  extensionDir: string;
  hm?: string;
}

export interface LinkOptions {
  entry?: string;
  href?: string;
  serveOnly?: boolean;
}

/**
 * 在开发服务器或预览服务器上设置 API 路由和中间件
 * 路由接口服务：router层 -> controller层 -> service层 -> repository层(本地文件)
 * @param server
 * @param options
 */
const setApis = (server: ViteDevServer | PreviewServer, options: DevToolsOptions) => {
  server.middlewares.use(bodyParser.json({ type: 'application/json', limit: '50000kb' }));
  server.middlewares.use(async (req, res, next) => {
    // 设置了 API 路由，使用 body-parser 中间件解析 JSON 请求，并根据请求路径调用 router 函数处理请求
    const reqUrl = req.url || '';
    if (reqUrl.startsWith(options.baseURL)) {
      const data = await router(req, options);
      res.writeHead(200, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify(data));
    } else {
      next();
    }
  });
};

/**
 * apiServerPlugin 插件
 * 将 setApis 函数应用于 Vite 开发服务器或预览服务器，用于处理 API 请求。
 * @param options
 * @returns
 */
const apiServerPlugin = function (options: DevToolsOptions): Plugin {
  return {
    name: 'cfg-api-plugin',
    apply: 'serve',
    configureServer(server) {
      setApis(server, options);
    },
    configurePreviewServer(server) {
      return () => {
        setApis(server, options);
      };
    }
  };
};

/**
 * linkPlugin 插件
 * HTML 文件中插入动态链接脚本，根据配置选项决定是否在页面中注入特定的链接和脚本
 * @param options
 * @returns
 */
const linkPlugin = function (options: DevToolsOptions): Plugin {
  const { entry = '/index.html', href = '', serveOnly = true } = options.linkOptions || {};
  let config: ResolvedConfig;
  return {
    name: 'cfg-link-plugin',
    apply: serveOnly ? 'serve' : undefined,
    configResolved(resolvedConfig: ResolvedConfig) {
      config = resolvedConfig;
    },
    transformIndexHtml(html, ctx) {
      // console.log('html ', html);
      if (html.includes('CFG-LINK')) {
        return html;
      }
      if (options.link) {
        // console.log('ctx ', ctx);
        if (ctx.path !== entry) {
          return html;
        }
        const link = typeof options.link === 'string' ? options.link : `${options.packageName}/link.js`;
        const url = `${config.base}${link}`;
        return html.replace(
          /<\/body>/,
          `
          <script>window.__CFG_LINK__ = { href: '${href}' }</script>
          <script src="${url}"></script></body>
          `
        );
      }
      return html;
    }
  };
};

/**
 * hmPlugin 插件
 * HTML 文件中插入百度统计代码，用于网站的访问统计。
 * @param options
 * @returns
 */
const hmPlugin = function (options: DevToolsOptions): Plugin {
  const { hm } = options;
  return {
    name: 'cfg-hm-plugin',
    transformIndexHtml(html) {
      return html.replace(
        /<\/body>/,
        `
        <script>
        (function () {
          window._hmt = window._hmt || [];
          const hm = document.createElement('script');
          hm.src = 'https://hm.baidu.com/hm.js?${hm}';
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(hm, s);
        })();       
        </script>
        `
      );
    }
  };
};

/**
 * aliasPlugin 插件
 * 根据配置选项设置路径别名，方便在开发模式下引用本地包。
 * @param options
 * @returns
 */
const aliasPlugin = function (options: DevToolsOptions): Plugin {
  return {
    name: 'cfg-alias-plugin',
    config(config) {
      const { root = process.cwd() } = config || {};
      const cfgDir = join(root, options.cfgDir);
      const packagesDir = join(root, options.packagesDir);
      const devAlias: Record<string, string> =
        options.devMode && process.env.NODE_ENV === 'development'
          ? {
              'cfg-packages-ui/dist/style.css': join(packagesDir, 'ui/src/style/index.scss'),
              'cfg-packages-icons/dist/style.css': join(packagesDir, 'icons/src/style.scss'),
              'cfg-packages-designer/dist/style.css': join(packagesDir, 'designer/src/style/index.scss'),
              'cfg-packages-base': join(packagesDir, 'base/src'),
              'cfg-packages-utils': join(packagesDir, 'utils/src/index.ts'),
              'cfg-packages-icons/svg': join(packagesDir, 'icons/dist/svg.ts'),
              'cfg-packages-icons': join(packagesDir, 'icons/src'),
              'cfg-packages-ui': join(packagesDir, 'ui/src'),
              'cfg-packages-charts': join(packagesDir, 'charts/src'),
              'cfg-packages-core': join(packagesDir, 'core/src'),
              'cfg-packages-designer': join(packagesDir, 'designer/src'),
              'cfg-packages-renderer': join(packagesDir, 'renderer/src'),
              'cfg-packages-coder': join(packagesDir, 'coder/src')
            }
          : {};
      if (config.resolve) {
        config.resolve.alias = Object.assign(config.resolve.alias || {}, {
          $cfg: cfgDir,
          ...devAlias
        });
      } else {
        config.resolve = {
          alias: {
            $cfg: cfgDir,
            ...devAlias
          }
        };
      }
    }
  };
};

/**
 * 解析预设插件（preset plugins）并生成与这些插件相关的复制和静态资源目录配置
 * @param options
 * @returns
 */
export function parsePresetPlugins(options: DevToolsOptions) {
  const { presetPlugins = [], pluginNodeModulesDir = 'node_modules', staticBase } = options;
  const pkg = readJsonSync(resolve('./package.json'));
  // 1. 从 devDependencies 和 dependencies 中筛选出符合预设插件前缀的依赖项
  const { devDependencies, dependencies } = pkg || {};
  const deps = Object.keys({ ...devDependencies, ...dependencies }).filter((name) =>
    presetPlugins.some((regex) => name.startsWith(regex))
  );
  const copies: CopyPluginOption[] = [];
  const staticDirs: StaticPluginOption[] = [];
  // 2. 遍历符合预设插件前缀的依赖项，在node_modules下存在依赖项的dist目录的，生成：
  // dist复制到@cfg/plugins
  // 静态资源目录配置
  for (const dep of deps) {
    const dist = join(pluginNodeModulesDir, dep, 'dist');
    if (pathExistsSync(dist)) {
      copies.push({
        from: dist,
        to: '@cfg/plugins',
        emptyDir: false
      });
      staticDirs.push({
        path: `${staticBase}@cfg/plugins`,
        dir: dist
      });
    }
  }
  return {
    copies,
    staticDirs
  };
}

/**
 * 根据传入的配置选项创建并返回一组插件，包括上面提到的全部插件
 * @param options
 * @returns
 */
export function createDevTools(options: Partial<DevToolsOptions> = {}) {
  // 默认配置
  const opts: DevToolsOptions = {
    baseURL: '/cfg/local/repository',
    copy: true,
    server: true,
    staticBase: '/',
    staticDir: 'public',
    link: true,
    linkOptions: null,
    cfgDir: '.cfg',
    packagesDir: '../../packages',
    devMode: false,
    uploader: '/uploader.json',
    packageName: 'cfg-packages-pro', // 低代码开发平台包名，默认：通用版CFG.PRO
    nodeModulesDir: 'node_modules',
    presetPlugins: ['@newpearl/plugin-', '@cfg/plugin-'],
    pluginNodeModulesDir: 'node_modules',
    extensionDir: '', // 暂时没用，恒为空字符串
    hm: '', // TODO: https://tongji.baidu.com/ 申请自己站点的统计ID
    ...options
  };

  const plugins: Plugin[] = [aliasPlugin(opts)];
  // 使用createDevTools的项目存在 node_modules/cfg-packages-pro/dist 路径
  const proPath = `${opts.nodeModulesDir}/${opts.packageName}/dist`;
  // node_modules/cfg-packages-materials/dist 路径
  const materialsPath = `${opts.nodeModulesDir}/cfg-packages-materials/dist`;
  // node_modules/cfg-packages-pro/cfg-packages-materials 路径
  const materialsPathPkg = `${opts.nodeModulesDir}/${opts.packageName}/${materialsPath}`;

  if (opts.copy) {
    // 复制物料目录
    const copyOptions: CopyPluginOption[] = [];
    if (pathExistsSync(materialsPath)) {
      copyOptions.push({
        from: materialsPath,
        to: 'cfg-packages-materials',
        emptyDir: true
      });
    } else if (pathExistsSync(materialsPathPkg)) {
      copyOptions.push({
        from: materialsPathPkg,
        to: 'cfg-packages-materials',
        emptyDir: true
      });
    } else {
      console.warn('\n cfg-packages-materials is not installed, please install it first.\n');
    }
    // 复制到扩展目录（暂时没用）
    if (opts.extensionDir && pathExistsSync(opts.extensionDir)) {
      copyOptions.push({
        from: opts.extensionDir,
        to: 'cfg/extension',
        emptyDir: true
      });
    }
    if (copyOptions.length) {
      plugins.push(copyPlugin(copyOptions));
    }
  }

  // 本地开发服务
  if (opts.server) {
    // api 服务
    plugins.push(apiServerPlugin(opts));

    // 静态资源服务
    const staticOptions: StaticPluginOption[] = [];
    if (pathExistsSync(proPath)) {
      staticOptions.push({
        path: `${opts.staticBase}${opts.packageName}`,
        dir: proPath
      });
    }
    // 静态资源服务-复制物料目录
    if (pathExistsSync(materialsPath)) {
      staticOptions.push({
        path: `${opts.staticBase}cfg-packages-materials`,
        dir: materialsPath
      });
    } else if (pathExistsSync(materialsPathPkg)) {
      staticOptions.push({
        path: `${opts.staticBase}cfg-packages-materials`,
        dir: materialsPathPkg
      });
    } else {
      console.warn('\n cfg-packages-materials is not installed, please install it first.\n');
    }
    // 静态资源服务-扩展目录（暂时没用）
    if (opts.extensionDir && pathExistsSync(opts.extensionDir)) {
      staticOptions.push({
        path: `${opts.staticBase}cfg/extension`,
        dir: opts.extensionDir
      });
    }
    if (staticOptions.length) {
      plugins.push(staticPlugin(staticOptions));
    }
  }

  if (opts.presetPlugins && opts.presetPlugins.length) {
    const { copies, staticDirs } = parsePresetPlugins(opts);
    if (copies.length) {
      plugins.push(copyPlugin(copies));
    }
    if (staticDirs.length) {
      plugins.push(staticPlugin(staticDirs));
    }
  }

  if (opts.link) {
    plugins.push(linkPlugin(opts));
  }

  if (opts.hm) {
    plugins.push(hmPlugin(opts));
  }

  return plugins;
}
