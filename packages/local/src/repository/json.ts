import { resolve, join } from 'path';
import {
  readdirSync,
  writeJsonSync,
  pathExistsSync,
  readJsonSync,
  removeSync,
  ensureFileSync
} from 'cfg-packages-node';

/**
 * 本地 .cfg 目录下操作.json文件
 */
export class JsonRepository {
  private path: string;

  constructor(path: string) {
    this.path = resolve('.cfg', path);
  }

  exist(name: string) {
    const filePath = join(this.path, `${name}.json`);
    return pathExistsSync(filePath);
  }

  save(name: string, json: any) {
    const filePath = join(this.path, `${name}.json`);
    if (!this.exist(name)) {
      ensureFileSync(filePath);
    }
    writeJsonSync(filePath, json);
    return true;
  }

  get(name: string) {
    const filePath = join(this.path, `${name}.json`);
    if (pathExistsSync(filePath)) {
      return readJsonSync(filePath);
    } else {
      return undefined;
    }
  }

  getAllFiles() {
    if (pathExistsSync(this.path)) {
      const files = readdirSync(this.path) || [];
      // console.log('files', files);
      return files.map((name: string) => {
        const content = this.get(name.replace('.json', ''));
        return content;
      });
    }
    return [];
  }

  remove(name: string) {
    const filePath = join(this.path, `${name}.json`);
    if (pathExistsSync(filePath)) {
      removeSync(filePath);
      return true;
    }
    return false;
  }

  clear() {
    if (pathExistsSync(this.path)) {
      removeSync(this.path);
      return true;
    }
    return false;
  }
}
