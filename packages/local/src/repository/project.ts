import { resolve, join, basename } from 'path';
import { outputFileSync, pathExistsSync, removeSync, mkdirpSync, copySync, readJsonSync } from 'cfg-packages-node';
import { type PageFile } from 'cfg-packages-core';
import AdmZip from 'adm-zip';

/**
 * 本地 .exports 目录下导出并生成相应项目的全量代码
 */
export class ProjectExport {
  private path: string;
  private cfgPath: string;
  private modulePath: string;
  private projectsSettingFileDir: string;
  private projectsMaterialsFileDir: string;
  private projectsDslFileDir: string;
  private projectsVueFileDir: string;
  private routerIndexPath: string;
  private routerMainPath: string;
  private hooksUsePath: string;
  private indexFileName: string;
  constructor(name: string) {
    this.path = resolve('.exports');
    this.cfgPath = '.cfg';
    this.modulePath = '';
    this.projectsSettingFileDir = 'projects';
    this.projectsMaterialsFileDir = 'materials';
    this.projectsDslFileDir = 'files';
    this.projectsVueFileDir = 'vue';
    this.routerIndexPath = 'router/index.ts';
    this.routerMainPath = 'router/mainAppRouter.ts';
    this.hooksUsePath = 'hooks';
    this.indexFileName = 'index.ts';
    this.init(name);
  }

  getPrjPath(name: string) {
    const dir = join(this.path, `${name}`);
    return dir;
  }

  /**
   * 初始化项目目录，如不存在则创建，如存在则清空目录
   * @param name
   */
  init(name: string) {
    const dir = join(this.path, name);
    this.modulePath = dir;
    if (this.exist(name)) {
      this.clearPrj(name);
      return;
    }
    mkdirpSync(dir);
  }

  /**
   * 是否存在name项目
   * @param name
   * @returns
   */
  exist(name: string) {
    const filePath = join(this.path, name);
    return pathExistsSync(filePath);
  }

  /**
   * .cfg/projects/${项目id}.json -> .exports/${相应项目}/.cfg/projects/${项目id}.json
   * @param name
   */
  handleProjectsSettingFile(name: string) {
    const filePath = `${this.cfgPath}/${this.projectsSettingFileDir}/${name}.json`;
    const projectContent = readJsonSync(resolve(filePath));
    // 修改项目配置mock数据开关为关闭
    if (projectContent?.config?.mock) {
      projectContent.config.mock = false;
    }
    const newPath = join(this.modulePath, this.cfgPath, this.projectsSettingFileDir, `${name}.json`);
    outputFileSync(newPath, JSON.stringify(projectContent, null, 2), 'utf-8');
  }

  /**
   * .cfg/materials/${项目id}.json -> .exports/${相应项目}/.cfg/materials/${项目id}.json
   * @param name
   */
  handleProjectsMaterialsFile(name: string) {
    const filePath = join(this.modulePath, this.cfgPath, this.projectsMaterialsFileDir, `${name}.json`);
    copySync(`${this.cfgPath}/${this.projectsMaterialsFileDir}/${name}.json`, filePath);
  }

  /**
   * 该项目下.cfg/files -> .exports/.cfg/files 和 .cfg/vue -> .exports/.cfg/vue
   * @param name
   */
  handleDslFilesAndVueFiles(name: string) {
    // 1.读取.exports/.cfg/projects/${name}.json 的 pages和blocks
    const file = join(this.modulePath, this.cfgPath, this.projectsSettingFileDir, `${name}.json`);
    const { pages, blocks } = readJsonSync(resolve(file));
    // console.log(pages, blocks);
    // 2.遍历页面
    const usePages = pages.filter(
      (page: PageFile) => page.type === 'page' && !page.dir && !page.raw && page.name && page.id
    );
    usePages.forEach((page: PageFile) => {
      // const dslPath = join(this.modulePath, this.cfgPath, this.projectsDslFileDir, `${page.id}.json`);
      // const dslFile = resolve(`${this.cfgPath}/${this.projectsDslFileDir}/${page.id}.json`);
      // copySync(dslFile, dslPath);
      const vuePath = join(this.modulePath, this.cfgPath, this.projectsVueFileDir, `${page.id}.vue`);
      const vueFile = resolve(`${this.cfgPath}/${this.projectsVueFileDir}/${page.id}.vue`);
      copySync(vueFile, vuePath);
    });
    // 3.遍历区块
    const useBlocks = blocks.filter((block: any) => block.type === 'block' && block.name && block.id);
    useBlocks.forEach((block: any) => {
      // const dslPath = join(this.modulePath, this.cfgPath, this.projectsDslFileDir, `${block.id}.json`);
      // const dslFile = resolve(`${this.cfgPath}/${this.projectsDslFileDir}/${block.id}.json`);
      // copySync(dslFile, dslPath);
      const vuePath = join(this.modulePath, this.cfgPath, this.projectsVueFileDir, `${block.id}.vue`);
      const vueFile = resolve(`${this.cfgPath}/${this.projectsVueFileDir}/${block.id}.vue`);
      copySync(vueFile, vuePath);
    });
  }

  /**
   * 路由配置文件/router
   * @param pages
   * @param name
   * @param homepage
   */
  handleRouter(pages: PageFile[], name: string, homepage: string) {
    // 1.生成应用使用的路由配置文件内容
    const usePages = pages.filter((page) => page.type === 'page' && !page.dir && !page.raw && page.name && page.id);
    // 生成路由数组
    const routes = usePages.map((page) => ({
      name: page.id,
      path: homepage === page.id ? `/${name}` : `/${name}/${page.name.toLowerCase()}`,
      component: page.name
    }));
    // 生成 import 语句
    const imports = usePages
      .map((page) => `import ${page.name} from '../${this.cfgPath}/${this.projectsVueFileDir}/${page.id}.vue';`)
      .join('\n');
    // 生成路由数组string内容
    const routesString = JSON.stringify(routes, null, 2).replace(/"component": "(\w+)"/g, '"component": $1');
    const content = `/* eslint-disable prettier/prettier */
${imports}

const routes = ${routesString};

export default routes;\n`;
    // 写入文件
    const routerIndexFile = join(this.modulePath, this.routerIndexPath);
    outputFileSync(routerIndexFile, content, 'utf-8');
    // 2.（使用微前端框架必需）生成该应用所属的主应用路由文件内容
    const mainRoutes = routes.map((item) => {
      const isHomepage = usePages.find((page) => homepage === page.id && item.name === page.name);
      return {
        path: item.path,
        meta: { title: isHomepage ? `${name}模块` : `${name}模块-${item.name.toLowerCase()}页面` },
        component: 'Vue3AppWrapper'
      };
    });
    // 生成路由配置文件内容
    const mainRoutesString = JSON.stringify(mainRoutes, null, 2).replace(/"component": "(\w+)"/g, '"component": $1');
    const mainContent = `/* eslint-disable prettier/prettier */
/**
 * tips: 如使用微前端框架，可将下面内容复制粘贴到主应用路由文件并做相应修改，然后删除该文件
 * 如未使用微前端，可直接删除该文件
 */
// 用于包装 Vue 3 应用的组件
// @ts-ignore
import Vue3AppWrapper from 'src/components/Vue3AppWrapper';

const routes = ${mainRoutesString};

export default routes;\n`;
    // 写入文件
    const routerMainFile = join(this.modulePath, this.routerMainPath);
    outputFileSync(routerMainFile, mainContent, 'utf-8');
  }

  /**
   * 生成组合式函数/hooks
   * @param pages
   * @param name
   */
  createHooksUse(pages: PageFile[]) {
    const hooksDir = join(this.modulePath, this.hooksUsePath);
    // 1.给每个页面创建组合式函数use文件（页面可选择引入）
    const usePages = pages.filter((page) => page.type === 'page' && !page.dir && !page.raw && page.name && page.id);
    const imports = [];
    for (const page of usePages) {
      const usePageName = `use${page.name}`;
      imports.push(`export * from './${usePageName}';`);
      const usePageFile = join(hooksDir, `${usePageName}.ts`);
      const banner = `/**
 * ../${this.cfgPath}/${this.projectsVueFileDir}/${page.id}.vue页面可通过以下方式按需引入-组合式函数：
 * import { ${usePageName} } from '../../hooks/${usePageName}';
 * ...
 * setup() {
 *   const { showAdd, showEdit } = ${usePageName}();
 *   return {
 *     showAdd,
 *     showEdit
 *   };
 * }
 * ...
 */\n`;
      const content = `${banner}import { ref, onMounted } from 'vue';\n
export function ${usePageName}() {
  const showAdd = ref(false);
  const showEdit = ref(false);
  onMounted(() => {
    // 处理新增和编辑权限
    showAdd.value = true;
    showEdit.value = true;
  });
  return {
    showAdd,
    showEdit
  };
}\n`;
      outputFileSync(usePageFile, content, 'utf-8');
    }
  }

  /**
   * 生成入口文件index.ts
   * @param name
   */
  createIndexFile(name: string) {
    const mainContent = `import { createProvider } from 'cfg-packages-renderer';
// 区分开发/生产环境下物料base地址，视情况改动
const isProdEnv = process.env.NODE_ENV === 'production';
const materialPath = isProdEnv ? './vue3-app/vue3-app-static/' : './vue3-app-static/';

function importAll(r, modulePrefixKey) {
  const modules: Record<string, () => Promise<any>> = {};
  r.keys().forEach((key) => {
    const modifiedKey = key.replace('./', modulePrefixKey);
    modules[modifiedKey] = () => Promise.resolve(r(key));
  });
  return modules;
}
const jsonModules = importAll((require as any).context('./${this.cfgPath}/${this.projectsSettingFileDir}', false, /\\.json$/), '${this.cfgPath}/${this.projectsSettingFileDir}/');
const vueModules = importAll((require as any).context('./${this.cfgPath}/${this.projectsVueFileDir}', false, /\\.vue$/), '${this.cfgPath}/${this.projectsVueFileDir}/');

const { provider, onReady } = createProvider({
  nodeEnv: 'production',
  modules: { ...jsonModules, ...vueModules },
  service: null,
  router: undefined,
  materialPath: materialPath,
  project: {
    id: '${name}'
  },
  dependencies: {
    Vue: () => import('vue'),
    VueRouter: () => import('vue-router'),
    ElementPlus: () => import('element-plus'),
    CfgUtils: () => import('cfg-packages-utils')
  }
});

export { provider, onReady };\n`;
    // 写入文件
    const file = join(this.modulePath, this.indexFileName);
    outputFileSync(file, mainContent, 'utf-8');
  }

  /**
   * 压缩指定目录为 ZIP 文件
   * @param {string} dirPath - 要压缩的目录路径
   * @returns {Promise<void>} - 当 ZIP 文件创建完成时解析的 Promise, 没有返回值
   */
  async zipFile(dirPath: string): Promise<void> {
    const zip = new AdmZip();
    zip.addLocalFolder(dirPath);
    const zipFilePath = join(dirPath, '..', `${basename(dirPath)}.zip`);

    return new Promise((resolve, reject) => {
      zip.writeZip(zipFilePath, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  clearAll() {
    if (pathExistsSync(this.path)) {
      removeSync(this.path);
      return true;
    }
    return false;
  }

  clearPrj(name: string) {
    const filePath = join(this.path, name);
    if (pathExistsSync(filePath)) {
      removeSync(filePath);
      return true;
    }
    return false;
  }
}
