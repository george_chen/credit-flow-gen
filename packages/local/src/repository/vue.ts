import { resolve, join } from 'path';
import { pathExistsSync, removeSync, outputFileSync, ensureFileSync } from 'cfg-packages-node';

/**
 * 本地 .cfg/vue 目录下操作.vue文件
 */
export class VueRepository {
  private path: string;

  constructor() {
    this.path = resolve('.cfg/vue');
  }

  getPath() {
    return this.path;
  }

  getFilePath(name: string) {
    const filePath = join(this.path, `${name}.vue`);
    return filePath;
  }

  exist(name: string) {
    const filePath = join(this.path, `${name}.vue`);
    return pathExistsSync(filePath);
  }

  save(name: string, content: any) {
    const filePath = join(this.path, `${name}.vue`);
    if (!this.exist(name)) {
      ensureFileSync(filePath);
    }
    outputFileSync(filePath, content, 'utf-8');
    return true;
  }

  remove(name: string) {
    const filePath = join(this.path, `${name}.vue`);
    if (pathExistsSync(filePath)) {
      removeSync(filePath);
      return true;
    }
    return false;
  }

  clear() {
    if (pathExistsSync(this.path)) {
      removeSync(this.path);
      return true;
    }
    return false;
  }
}
