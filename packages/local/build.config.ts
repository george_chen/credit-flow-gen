import { defineBuildConfig } from 'unbuild';

export default defineBuildConfig([
  {
    entries: [
      {
        builder: 'mkdist',
        input: 'src',
        outDir: 'dist'
      }
    ],
    declaration: true,
    clean: true,
    failOnWarn: false,
    rollup: {
      emitCJS: false
    },
    externals: ['path', 'cfg-packages-cli', 'cfg-packages-core', 'cfg-packages-node', 'serve-static', 'body-parser']
  }
]);
