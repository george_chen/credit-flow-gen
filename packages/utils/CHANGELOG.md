# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.54](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.53...cfg-packages-utils@0.0.54) (2024-10-23)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.53](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.52...cfg-packages-utils@0.0.53) (2024-10-12)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.52](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.51...cfg-packages-utils@0.0.52) (2024-10-12)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.51](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.50...cfg-packages-utils@0.0.51) (2024-10-12)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.50](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.49...cfg-packages-utils@0.0.50) (2024-10-12)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.49](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.48...cfg-packages-utils@0.0.49) (2024-10-12)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.48](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.47...cfg-packages-utils@0.0.48) (2024-10-11)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.47](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.46...cfg-packages-utils@0.0.47) (2024-10-11)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.46](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.45...cfg-packages-utils@0.0.46) (2024-10-10)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.45](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.44...cfg-packages-utils@0.0.45) (2024-09-13)

### Features

- 完善 utils 包的 commonReqResHandler 方法，准备发布到 npmjs ([9c28f0c](https://gitee.com/george_chen/credit-flow-gen/commits/9c28f0cdd5298ea64a9555db33d5f91e22cbeda7))

## [0.0.44](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.43...cfg-packages-utils@0.0.44) (2024-09-13)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.43](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.42...cfg-packages-utils@0.0.43) (2024-09-13)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.42](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.41...cfg-packages-utils@0.0.42) (2024-09-13)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.41](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.40...cfg-packages-utils@0.0.41) (2024-09-10)

### Features

- toFormData、发送数据类型 增加 qsjson 处理 ([efb5395](https://gitee.com/george_chen/credit-flow-gen/commits/efb5395e4681aec6d8ab31b5181dff676ef2ea31))

## [0.0.40](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.39...cfg-packages-utils@0.0.40) (2024-09-01)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.39](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.38...cfg-packages-utils@0.0.39) (2024-08-30)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.38](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.37...cfg-packages-utils@0.0.38) (2024-08-30)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.37](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.36...cfg-packages-utils@0.0.37) (2024-08-29)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.36](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.35...cfg-packages-utils@0.0.36) (2024-08-22)

### Features

- base 包增加 serializeParams 方法；utils 包完善 request 方法，增加 commonReqResHandler 方法 ([f2df3b0](https://gitee.com/george_chen/credit-flow-gen/commits/f2df3b090c49f995385f33635c04f3fe5e4ce09c))

## [0.0.35](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.34...cfg-packages-utils@0.0.35) (2024-08-21)

### Features

- request send 方法加上根据接口设置传入的 withCredentials 修改 axios.defaults.withCredentials ([f94557a](https://gitee.com/george_chen/credit-flow-gen/commits/f94557ab9ad8390c4059292601e7ef7eb6078c75))
- request send 中加上 withCredentials: true ([c28c35f](https://gitee.com/george_chen/credit-flow-gen/commits/c28c35f7d12acfb8cc583665a828159ec44898b4))

## [0.0.34](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.33...cfg-packages-utils@0.0.34) (2024-08-20)

### Features

- request send 中加上 withCredentials: true ([fd2ead8](https://gitee.com/george_chen/credit-flow-gen/commits/fd2ead858ab32d64600932513f372ed05f509734))

## [0.0.33](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.32...cfg-packages-utils@0.0.33) (2024-08-20)

### Features

- 删掉 send withCredentials ([a3017b7](https://gitee.com/george_chen/credit-flow-gen/commits/a3017b791d1c2f1c905fed9dd6ad4be804962067))

## [0.0.32](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.31...cfg-packages-utils@0.0.32) (2024-08-20)

### Features

- axios.create 加上 withCredentials true ([23ca824](https://gitee.com/george_chen/credit-flow-gen/commits/23ca8242969ccbcf5ae4c6e9e6c46f8ea1a7ecf8))

## [0.0.31](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.30...cfg-packages-utils@0.0.31) (2024-08-20)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.30](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.29...cfg-packages-utils@0.0.30) (2024-08-20)

### Features

- 完善 local project 和 API 添加是否允许发送凭据选项 ([dafa46f](https://gitee.com/george_chen/credit-flow-gen/commits/dafa46f758f696c9e6c0fcf19562ba253b5242ca))

## [0.0.29](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.28...cfg-packages-utils@0.0.29) (2024-08-12)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.28](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.27...cfg-packages-utils@0.0.28) (2024-08-12)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.27](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.26...cfg-packages-utils@0.0.27) (2024-08-12)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.26](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.25...cfg-packages-utils@0.0.26) (2024-08-11)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.25](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.24...cfg-packages-utils@0.0.25) (2024-08-11)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.24](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.23...cfg-packages-utils@0.0.24) (2024-08-11)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.23](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.22...cfg-packages-utils@0.0.23) (2024-08-11)

### Features

- 开发完成，待发版 ([dfb56fd](https://gitee.com/george_chen/credit-flow-gen/commits/dfb56fdf7a7b478ccb1227b7ea9fe3b416e9b402))

## [0.0.22](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.21...cfg-packages-utils@0.0.22) (2024-08-11)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.21](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.20...cfg-packages-utils@0.0.21) (2024-08-09)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.20](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.19...cfg-packages-utils@0.0.20) (2024-08-09)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.19](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.18...cfg-packages-utils@0.0.19) (2024-08-09)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.18](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.15...cfg-packages-utils@0.0.18) (2024-08-09)

### Features

- 项目导出完善完成，准备发版 ([d744af6](https://gitee.com/george_chen/credit-flow-gen/commits/d744af60ea8cec90f266a0cc25c5634c41adbf0b))
- 项目导出完善完成，准备发版 ([66a970e](https://gitee.com/george_chen/credit-flow-gen/commits/66a970e777f06024a270c9be41aa6c26a165535f))

## [0.0.17](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.15...cfg-packages-utils@0.0.17) (2024-08-09)

### Features

- 项目导出完善完成，准备发版 ([66a970e](https://gitee.com/george_chen/credit-flow-gen/commits/66a970e777f06024a270c9be41aa6c26a165535f))

## [0.0.16](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.15...cfg-packages-utils@0.0.16) (2024-08-09)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.15](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.14...cfg-packages-utils@0.0.15) (2024-07-23)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.14](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.13...cfg-packages-utils@0.0.14) (2024-07-23)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.13](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.12...cfg-packages-utils@0.0.13) (2024-07-23)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.12](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.11...cfg-packages-utils@0.0.12) (2024-07-22)

### Features

- 项目列表页，源码导出功能 开发中 ([3c4266b](https://gitee.com/george_chen/credit-flow-gen/commits/3c4266b8f454912752cbdf3959e2e66619e0dae7))

## [0.0.11](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.10...cfg-packages-utils@0.0.11) (2024-07-08)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.10](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.9...cfg-packages-utils@0.0.10) (2024-07-06)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.9](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.8...cfg-packages-utils@0.0.9) (2024-07-01)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.8](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.7...cfg-packages-utils@0.0.8) (2024-07-01)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.7](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.6...cfg-packages-utils@0.0.7) (2024-06-25)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.6](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.5...cfg-packages-utils@0.0.6) (2024-06-14)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.5](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.4...cfg-packages-utils@0.0.5) (2024-06-13)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.4](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.3...cfg-packages-utils@0.0.4) (2024-06-03)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.3](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.2...cfg-packages-utils@0.0.3) (2024-06-03)

**Note:** Version bump only for package cfg-packages-utils

## [0.0.2](https://gitee.com/george_chen/credit-flow-gen/compare/cfg-packages-utils@0.0.1...cfg-packages-utils@0.0.2) (2024-06-02)

### Features

- 更新 prettier 相关依赖安装包；完成 cfg-packages-coder 开发；cfg-packages-local 开发中 ([58b5531](https://gitee.com/george_chen/credit-flow-gen/commits/58b5531e8b800f029ac390e81d741e028ffccabd))

## 0.0.1 (2024-05-31)

### Features

- cfg-packages-utils 包开发中，需要加入部分方法 ([c9b80b4](https://gitee.com/george_chen/credit-flow-gen/commits/c9b80b469bc23624e05a74ea6576c328e7c12907))
