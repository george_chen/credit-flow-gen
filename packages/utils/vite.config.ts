import { createViteConfig } from 'cfg-packages-cli';

/**
 * UMD(Universal Module Definition)模式，旨在解决模块在不同环境中的兼容性问题。
 * 可以同时在浏览器和 Node.js 环境下运行。
 */
const isUmd = !!process.env.UMD;

export default createViteConfig({
  lib: true,
  dts: true,
  version: true,
  library: 'CfgUtils',
  buildTarget: isUmd ? 'es2015' : 'esnext', // UMD 模式下的构建目标
  emptyOutDir: isUmd ? false : true,
  external: isUmd ? undefined : ['cfg-packages-base'],
  formats: isUmd ? ['umd', 'iife'] : ['es', 'cjs'] // UMD 格式
});
