/* eslint-disable */
import { expect, test, vi } from 'vitest';
import { polling, fn2ObjURL, slicedArrayCb, url, licenseCodeFormatter, commonReqResHandler } from '../dist/index.mjs';

test('polling', async () => {
  const res = await polling({
    max: 2,
    runCallback: async (): Promise<any> => {
      console.log('runCallback executed');
      return 'goon';
    }
  });
  expect(res).toBe('timeout');
});

test('fn2ObjURL', () => {
  // 启用 Vitest 的 Fake Timers
  vi.useFakeTimers();

  // Mock URL.createObjectURL 和 URL.revokeObjectURL 方法
  const testBlobUrl = 'blob:http://localhost:3000/test-blob-url';
  // @ts-ignore
  global.URL.createObjectURL = vi.fn(() => testBlobUrl);
  // @ts-ignore
  global.URL.revokeObjectURL = vi.fn(() => {
    console.log('URL.revokeObjectURL executed');
  });

  // 执行方法并验证返回的 URL 是否正确
  const res = fn2ObjURL(() => {
    console.log('fn2ObjURL executed');
  });
  expect(res).toBe(testBlobUrl);
  expect(global.URL.createObjectURL).toHaveBeenCalled();

  // 在未到期时，revokeObjectURL 不应被调用
  expect(global.URL.revokeObjectURL).not.toHaveBeenCalled();

  // 触发所有定时器回调
  vi.runAllTimers();

  // 验证 revokeObjectURL 是否在定时器到期时被调用
  expect(global.URL.revokeObjectURL).toHaveBeenCalledWith(testBlobUrl);

  // 恢复原始实现
  // @ts-ignore
  delete global.URL.createObjectURL;
  // @ts-ignore
  delete global.URL.revokeObjectURL;
});

test('slicedArrayCb', () => {
  const list: any[] = [];
  slicedArrayCb([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 3, (arr) => {
    list.push(arr);
    return arr;
  });
  expect(list).toEqual([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10]]);
});

test('url functions', () => {
  expect(url.stringify({ a: 1, b: 2 })).toBe('a=1&b=2');
  expect(url.parse('a=1&b=2')).toEqual({ a: '1', b: '2' });
  expect(url.append('www.baidu.com', 'a=1&b=2')).toBe('www.baidu.com?a=1&b=2');
  expect(url.concat('www.baidu.com', { a: 'b', c: 'd' })).toBe('www.baidu.com?a=b&c=d');
});

test('licenseCodeFormatter', () => {
  const originalCode = 'oizsv123XYY';
  const formattedCode = licenseCodeFormatter(originalCode);
  expect(formattedCode).toEqual('01257123XYY');
});

test('commonReqResHandler return result_rows', () => {
  const exampleData = [
    {
      test: 1
    }
  ];
  const reqResParams = {
    response: {
      data: {
        retcode: 0,
        retmsg: 'success',
        result_rows: exampleData
      }
    },
    responseDataKey: 'data',
    resCodeKey: 'retcode',
    resDataKey: 'result_rows',
    loginJumpCodes: [10200007, 10200005, 19002028],
    successCode: 0,
    loginJumpUrl: '',
    failMsgKey: 'retmsg',
    throwError: false
  };
  const ret = commonReqResHandler(reqResParams);
  expect(ret).toEqual(exampleData);
});

test('commonReqResHandler throw Error', () => {
  const errObj = {
    retcode: 123,
    retmsg: '发生错误'
  };
  const reqResParams = {
    response: {
      data: errObj
    },
    responseDataKey: 'data',
    resCodeKey: 'retcode',
    resDataKey: 'result_rows',
    loginJumpCodes: [10200007, 10200005, 19002028],
    successCode: 0,
    loginJumpUrl: '',
    failMsgKey: 'retmsg',
    throwError: false
  };
  const res = commonReqResHandler(reqResParams);
  expect(res.retcode).toEqual(errObj.retcode);
  expect(res.retmsg).toEqual(errObj.retmsg);
});
