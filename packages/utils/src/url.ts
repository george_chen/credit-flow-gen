import { isClient } from './util';
import { rURL } from 'cfg-packages-base';

/**
 * 获取当前页面的 host
 */
export function getCurrentHost(includePath: boolean) {
  if (isClient) {
    const { protocol, host, pathname } = location;
    return `${protocol}//${host}${includePath ? pathname : ''}`;
  } else {
    return null;
  }
}

/**
 * 获取指定url的host
 */
export function getHost(url = '') {
  const matches = url.match(rURL);
  if (matches) {
    return matches[0];
  }
  return '';
}

/**
 * 键值对转换成查询字符串
 */
export function stringify(query: Record<string, any>) {
  const array = [];
  for (const key in query) {
    if (Object.prototype.hasOwnProperty.call(query, key)) {
      array.push([key, encodeURIComponent(query[key])].join('='));
    }
  }
  return array.join('&');
}

/**
 * 参数字符串转换成对象形式，如：a=1&b=2 转换成 {a:1, b:2}
 */
export function parse(str: string, sep?: string, eq?: string) {
  const obj: Record<string, any> = {};
  str = (str || location.search).replace(/^[^]*\?/, '');
  sep = sep || '&';
  eq = eq || '=';
  let arr;
  const reg = new RegExp('(?:^|\\' + sep + ')([^\\' + eq + '\\' + sep + ']+)(?:\\' + eq + '([^\\' + sep + ']*))?', 'g');
  while ((arr = reg.exec(str)) !== null) {
    if (arr[1] !== str) {
      obj[decodeURIComponent(arr[1])] = decodeURIComponent(arr[2] || '');
    }
  }
  return obj;
}

/**
 * 在url追加参数
 */
export function append(url: string, query: string | Record<string, any>) {
  query = typeof query === 'string' ? parse(query) : query;
  const path = url.split('?')[0];
  const originalQuery = parse(url);
  const joinQuery = Object.assign({}, originalQuery, query);
  const queryStr = stringify(joinQuery);
  return queryStr ? [path, queryStr].join('?') : url;
}

/**
 * 生成拼接的URL链接
 * Url.concat('www.baidu.com', {'a': 'b', 'c': 'd'})
 * return www.baidu.com?a=b&c=d
 * @param href 指定链接
 * @param params 指定URL后面参数，可靠默认为当前URL
 * @return 拼接完成的url
 */
export function concat(href: string, params: Record<string, any>): string {
  let url = '';
  if (href.indexOf('?') < 0) {
    url = url.concat(href, '?');
  } else {
    url = url.concat(href, '&');
  }
  for (const key in params) {
    url = url.concat(key, '=', encodeURIComponent(params[key]), '&');
  }
  url = url.substr(0, url.length - 1);
  return url;
}
