import { delay } from 'cfg-packages-base';

/**
 * 是否浏览器环境
 */
export const isClient = typeof window !== 'undefined';

/**
 * 文件对象 File 转换为 base64
 * @param file
 * @returns
 */
export const fileToBase64 = (file: File) => {
  return new Promise<string>((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      resolve(reader.result as string);
    };
    reader.onerror = (error) => {
      reject(error);
    };
  });
};

/**
 * FormData 转换为 json
 * @param data
 * @returns
 */
export function formDataToJson(data: FormData) {
  const json: Record<string, any> = {};
  if (!data) return {};
  data.forEach((val, key) => {
    json[key] = typeof val === 'string' ? decodeURIComponent(val) : val;
  });
  return json;
}

/**
 * 轮询工具函数
 * @param param 轮询参数，具体如下：
 * param.max - 最大轮询次数，默认 10
 * param.runCallback - 运行时回调函数，返回值为 false 时结束轮询，默认返回 true
 * param.timeOutCallback - 超时回调函数，默认打印 '轮询超时'
 * param.speed - 轮询速度，默认 1000ms
 * param.stateMap - 状态映射，默认 { done: 'done', goon: 'goon', timeout: 'timeout' }
 * @returns
 */
export async function polling({
  max = 10,
  runCallback = async (): Promise<any> => {
    return true;
  },
  timeOutCallback = () => {
    console.log('轮询超时');
  },
  speed = 1000,
  stateMap = {
    // 完成状态
    done: 'done',
    // 继续状态
    goon: 'goon',
    // 超时状态
    timeout: 'timeout'
  }
}) {
  let state;
  for (let i = 0; i < max; i++) {
    const res = await runCallback();
    switch (res) {
      case false:
        state = stateMap.done;
        break;
      case true:
        state = stateMap.goon;
        break;
      default:
        // 自定义状态
        state = res;
    }
    if (state !== stateMap.goon) return state;
    await delay(speed);
  }
  timeOutCallback();
  return stateMap.timeout;
}

/**
 * 将函数转换为 Blob URL
 * @param fn - 要转换的函数
 * @returns 生成的 Blob URL
 * @throws {TypeError} - 如果传入的参数不是函数
 */
export function fn2ObjURL(fn: () => void): string {
  if (typeof fn !== 'function') {
    throw new TypeError('Expected a function');
  }

  const blob = new Blob([`(${fn.toString()})();`], {
    type: 'application/javascript'
  });
  // 创建一个 Blob URL
  const url = URL.createObjectURL(blob);
  // 设置一个定时器在 1 分钟后释放 URL 对象，防止内存泄漏
  setTimeout(() => {
    URL.revokeObjectURL(url);
  }, 60000);
  return url;
}

/**
 * 将数组按指定大小切片并执行回调函数
 * @param arr - 要切片的数组
 * @param sliceSize - 每个切片的大小
 * @param callback - 回调函数，接收切片作为参数
 * @throws {TypeError} - 如果参数类型不正确
 */
export function slicedArrayCb(arr: any[], sliceSize: number, callback: (slice: any[]) => void): void {
  if (!Array.isArray(arr)) {
    throw new TypeError('Expected an array for the first argument');
  }

  if (typeof sliceSize !== 'number' || sliceSize <= 0 || !Number.isInteger(sliceSize)) {
    throw new TypeError('Expected a positive integer for the slice size');
  }

  if (typeof callback !== 'function') {
    throw new TypeError('Expected a function for the callback');
  }

  for (let i = 0; i < arr.length; i += sliceSize) {
    const slice = arr.slice(i, i + sliceSize);
    callback(slice);
  }
}

/**
 * 营业执照-统一社会代码 格式化
 * @param code
 * @returns
 */
export function licenseCodeFormatter(code: string) {
  const reg = /[o|i|z|s|v]|[^a-zA-Z0-9]/gi;
  code = code.replace(reg, (match) => {
    switch (match) {
      case 'o':
        return '0';
      case 'i':
        return '1';
      case 'z':
        return '2';
      case 's':
        return '5';
      case 'v':
        return '7';
      default:
        return '';
    }
  });
  if (code.length > 18) {
    code = code.slice(0, 18);
  }
  code = code.toUpperCase();
  return code;
}

/**
 * 接口请求返回响应方法处理器
 * @param params
 * @returns
 */
export function commonReqResHandler(params: any) {
  const {
    response = null,
    responseDataKey = '',
    resCodeKey = '',
    resDataKey = '',
    loginJumpCodes = [],
    successCode = 0,
    loginJumpUrl = '',
    failMsgKey = '',
    throwError = true
  } = params;
  if (!response) {
    return Promise.reject(new Error('传参response为空'));
  }
  if (!responseDataKey || !response[responseDataKey]) {
    return Promise.reject(new Error('传参responseDataKey为空或response下不存在responseDataKey'));
  }
  const res = response[responseDataKey];
  if (!resCodeKey || res[resCodeKey] === undefined || res[resCodeKey] === null) {
    return Promise.reject(new Error('传参resCodeKey为空或response[responseDataKey]下不存在resCodeKey'));
  }
  // 需要跳转登录页的情况（未登录或登录已过期则跳到登录页）
  const isNeedJumpLogin = loginJumpCodes.length && loginJumpCodes.includes(+res[resCodeKey]) && loginJumpUrl;
  if (isNeedJumpLogin) {
    window.location.href = loginJumpUrl;
    return;
  }
  if (!resDataKey) {
    return Promise.reject(new Error('传参resDataKey为空或response[responseDataKey]下不存在resDataKey'));
  }
  if (+res[resCodeKey] === successCode && res[resDataKey]) {
    return res[resDataKey];
  }
  // 其余错误的情况
  if (throwError) {
    console.error(`[${res[resCodeKey]}] ${res[failMsgKey] || ''}`);
    return Promise.reject(res);
  }
  return res;
}
