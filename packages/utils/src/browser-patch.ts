/**
 * IIFE，修改 EventTarget.prototype.addEventListener 方法的默认行为
 * 确保在没有明确指定 capture 参数的情况下，将其设置为 { passive: false }
 */
(function () {
  if (typeof window === 'undefined' || typeof EventTarget === 'undefined') {
    return;
  }
  const func = EventTarget.prototype.addEventListener;
  EventTarget.prototype.addEventListener = function (type, fn, capture) {
    if (typeof capture !== 'boolean') {
      capture = capture || {};
      capture.passive = false;
    }
    func.call(this, type, fn, capture);
  };
})();
