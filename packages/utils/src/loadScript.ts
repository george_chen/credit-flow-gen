import load from 'load-script';

export interface LoadScriptOptions {
  // 一个可选的布尔值，用于设置脚本的 async 属性。默认为 true
  async?: boolean;

  // 一个可选的键值对对象，在将脚本节点添加到 DOM 之前设置该脚本节点上的属性。默认是一个空对象。允许为脚本元素设置自定义属性，例如 data-* 属性。
  attrs?: Record<string, string>;

  // 一个可选的字符串值，用于设置脚本的 charset 属性。默认值是 utf8。
  charset?: string;

  // 一个可选的字符串，在将脚本节点添加到 DOM 之前附加到脚本节点。默认是空字符串。
  text?: string;

  // 一个可选的字符串，用于设置脚本的 type 属性。默认是 text/javascript。
  type?: string;

  // 一个可选的字符串，表示 JavaScript 导出的名称。
  library?: string;
}

/**
 * 加载外部脚本并返回一个 Promise
 * @param src 要加载的脚本的 URL
 * @param options 加载脚本的选项
 * @returns
 */
export function loadScript<T = any>(src: string, options: LoadScriptOptions = {}): Promise<T | undefined> {
  return new Promise((resolve, reject) => {
    const { library } = options;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    load(src, options, (err, _script) => {
      // console.log('loadScript', err, _script);
      if (err) {
        reject(err);
      } else {
        resolve(library ? (window as any)[library] : undefined);
      }
    });
  });
}
