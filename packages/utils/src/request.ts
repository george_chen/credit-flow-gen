/**
 * 通用 Axios 封装 HTTP 请求库
 * 包含：配置选项、请求和响应拦截器、错误处理和加载状态管理
 */
import { ref } from 'vue';
import axios from 'axios';
import type {
  AxiosInstance,
  CreateAxiosDefaults,
  AxiosResponse,
  AxiosRequestConfig,
  RawAxiosRequestHeaders,
  InternalAxiosRequestConfig,
  CancelTokenSource
} from 'axios';
import { merge, omit, debounce, throttle, uuid, pathToRegexpCompile, template } from 'cfg-packages-base';
import qs from 'qs';

const TYPES = {
  form: 'application/x-www-form-urlencoded',
  json: 'application/json',
  data: 'multipart/form-data'
};
// HTTP 请求方法，需要发送数据的方法
const DATA_METHODS = ['put', 'post', 'patch'];
// 请求 ID
const LOCAL_REQUEST_ID = 'Local-Request-Id';
// 本地加载延迟
const LOADING_DELAY = 200;
// 错误延迟
const ERROR_DELAY = 500;

/**
 * 定义请求跳过警告的接口
 */
export interface IRequestSkipWarn {
  // 处理程序
  executor: (resolve: (value: unknown) => void, reject: (reason?: any) => void) => void;
  // 响应编码
  code: string | number;
  // 参数名称
  name?: string;
  // 响应数据成功回调
  callback?: (res: AxiosResponse) => void;
  // 请求完成回调
  complete?: () => void;
}
export interface IRequestSkipWarnResponse extends AxiosResponse {
  promise: any;
}

/**
 * 定义 API 响应包装器
 */
export interface IResultWrapper<T = any> {
  code: number;
  data: T;
  msg: string;
  success: boolean;
}

/**
 * 定义原始响应类型
 */
export type RequestOriginResponse<R = any, D = any> = AxiosResponse<IResultWrapper<R>, D>;

/**
 * 定义请求设置接口，包含发送数据类型、自定义请求头、加载状态和错误处理等
 */
export interface IRequestSettings {
  /**
   * 发送数据类型
   */
  type?: 'form' | 'json' | 'data';

  /**
   *  是否注入自定义的请求头
   */
  injectHeaders?: boolean;

  /**
   * 是否允许发送凭据
   */
  withCredentials?: boolean;

  /**
   * 自定义请求头
   */
  headers?:
    | RawAxiosRequestHeaders
    | ((id: string, config: AxiosRequestConfig, settings: IRequestSettings) => RawAxiosRequestHeaders);

  /**
   * 是否显示 loading
   */
  loading?: boolean;

  /**
   * 显示 loading
   */
  showLoading?: () => void;

  /**
   * 关闭 loading
   */
  hideLoading?: () => void;

  /**
   * 显示失败提示
   */
  failMessage?: boolean;

  /**
   * 自定义失败提示
   */
  showError?: (msg: string, e: any) => void;

  /**
   *  返回原始 axios 响应对象
   */
  originResponse?: boolean;

  /**
   * 校验响应成功
   */
  validSuccess?: boolean;

  /**
   * 自定义校验方法
   */
  validate?: (res: AxiosResponse) => boolean;

  /**
   * 请求响应警告执行程序插件
   */
  skipWarn?: IRequestSkipWarn;

  /**
   * 其他自定义扩展参数
   */
  [index: string]: any;
}

/**
 * 定义请求选项和配置
 */
export interface IRequestOptions extends CreateAxiosDefaults {
  settings?: IRequestSettings;
}
export interface IRequestConfig<D = any> extends AxiosRequestConfig<D> {
  settings?: IRequestSettings;
  // url path 参数对象
  query?: Record<string, any>;
}

/**
 * 定义请求记录
 */
export interface IRequestRecord {
  settings: IRequestSettings;
  config: AxiosRequestConfig;
  source: CancelTokenSource;
}

/**
 * 核心-请求类
 */
export class Request {
  axios: AxiosInstance;
  settings: IRequestSettings;
  records: Record<string, IRequestRecord> = {};
  isLoading = false;
  private stopSkipWarn?: () => void;
  private showLoading: (settings: IRequestSettings) => void;
  private showError: (settings: IRequestSettings, e: any) => void;

  constructor(options: IRequestOptions = {}) {
    this.settings = options.settings || {};
    const defaults = omit<IRequestOptions, CreateAxiosDefaults>(options, ['settings']);
    this.axios = axios.create(
      merge(
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          // 默认不允许发送凭据
          withCredentials: false,
          timeout: 2 * 60 * 1000
        },
        defaults
      )
    );
    this.setupSkipWarn(this.settings);
    this.showLoading = debounce(this.openLoading.bind(this), LOADING_DELAY);
    this.showError = throttle(this.showErrorExe.bind(this), ERROR_DELAY, {
      leading: true,
      trailing: false
    });
  }

  /**
   * 设置新的请求配置
   * @param options
   */
  setConfig(options: IRequestOptions = {}) {
    this.settings = merge(this.settings, options.settings || {});
    const defaults = omit<IRequestOptions, CreateAxiosDefaults>(options, ['settings']);
    this.axios.defaults = merge(this.axios.defaults, defaults);
    this.setupSkipWarn(this.settings);
  }

  /**
   * 取消指定 ID 或所有未完成的请求
   * @param id
   * @param message
   * @returns
   */
  cancel(id?: string, message = '请求已取消') {
    // 取消对应id单独请求
    if (id) {
      const record = this.records[id];
      if (!record) return;
      record.source.cancel(message);
    } else {
      // 不指定id，取消全部未完成的请求
      for (const record of Object.values(this.records)) {
        record.source.cancel(message);
      }
    }
  }

  /**
   * 创建请求头，支持自定义头部注入
   * @param id
   * @param settings
   * @param config
   * @returns
   */
  private createHeaders(id: string, settings: IRequestSettings, config: AxiosRequestConfig) {
    const injectHeaders = settings.injectHeaders
      ? typeof settings.headers === 'function'
        ? settings.headers(id, config, settings)
        : settings.headers || {}
      : {};

    const headers: RawAxiosRequestHeaders = {
      'Content-Type': TYPES[settings.type || 'form'],
      ...config.headers,
      ...injectHeaders
    };

    if (settings.skipWarn) {
      headers[LOCAL_REQUEST_ID] = id;
    }
    return headers;
  }

  /**
   * 检查头部是否为 JSON 类型
   * @param headers
   * @returns
   */
  private isJsonType(headers: RawAxiosRequestHeaders) {
    return Object.entries(headers).some(([k, v]) => {
      return k.toLowerCase() === 'content-type' && String(v).includes('application/json');
    });
  }

  /**
   * 将数据转换为 FormData 或 URLSearchParams 或 使用qs.stringify处理数据
   * @param data
   * @param type
   * @returns
   */
  private toFormData(data: any, type = 'data'): any {
    if (data instanceof FormData || data instanceof URLSearchParams) {
      return data;
    }
    // 处理qsjson类型：qs.stringify(jsonData);
    if (type === 'qsjson') {
      return qs.stringify(data);
    }
    const params: any = type === 'data' ? new FormData() : new URLSearchParams();
    Object.entries(data).forEach(([key, value]) => {
      params.append(key, value);
    });
    return params;
  }

  /**
   * 创建发送数据，包括处理不同的请求方法和数据类型
   * @param settings
   * @param config
   * @param headers
   * @param isSkipWarn
   * @returns
   */
  private createSendData(
    settings: IRequestSettings,
    config: AxiosRequestConfig,
    headers: RawAxiosRequestHeaders,
    isSkipWarn: boolean
  ) {
    const { type, skipWarn } = settings;
    const { name = 'skipWarn' } = skipWarn || {};
    let { data, params } = config;
    const { method = 'get' } = config;
    const skip = isSkipWarn ? { [name]: true } : {};
    if (DATA_METHODS.includes(method.toLowerCase())) {
      data = Object.assign(data || {}, skip);
      data = type !== 'json' || !this.isJsonType(headers) ? this.toFormData(data, type) : data;
    } else {
      params = {
        ...data,
        ...params,
        ...skip
      };
    }

    return {
      data,
      params
    };
  }

  /**
   * 生成请求 URL，支持路径参数
   * @param config
   * @returns
   */
  private createUrl(config: IRequestConfig) {
    let { url } = config;
    const { query } = config;
    if (url) {
      if (query) {
        const compiled = template(url);
        url = compiled(query);
      }
      try {
        const toPath = pathToRegexpCompile(url, { encode: encodeURIComponent });
        return toPath(query || {});
      } catch (e) {
        console.error('createUrl', 'pathToRegexpCompile error', url);
      }
    }
    return url;
  }

  /**
   * 控制加载状态的显示
   * @param settings
   * @returns
   */
  private openLoading(settings: IRequestSettings) {
    const { loading, showLoading } = settings;
    if (!loading) return;
    if (showLoading) {
      const records = Object.keys(this.records);
      if (records.length > 0) {
        this.isLoading = true;
        showLoading();
      }
    }
  }

  /**
   * 控制加载状态的隐藏
   * @param settings
   * @returns
   */
  private closeLoading(settings: IRequestSettings) {
    const { loading, hideLoading } = settings;
    if (!loading) return;
    this.isLoading = false;
    const records = Object.keys(this.records);
    if (hideLoading && records.length === 0) {
      this.isLoading = false;
      hideLoading();
    }
  }

  /**
   * 显示错误信息
   * @param settings
   * @param e
   */
  private showErrorExe(settings: IRequestSettings, e: any) {
    const { failMessage, showError } = settings;
    if (failMessage && showError) {
      const msg = e?.message || e?.msg || '未知错误';
      showError(msg, e);
    }
  }

  /**
   * 验证响应是否成功
   * @param settings
   * @param res
   * @returns
   */
  private validResponse(settings: IRequestSettings, res: AxiosResponse) {
    const { validSuccess, validate } = settings;
    if (validSuccess && validate) {
      return !!validate(res);
    }
    return true;
  }

  /**
   * 检查响应是否跳过警告
   * @param res
   * @returns
   */
  private isSkipWarnResponse(res: any): res is IRequestSkipWarnResponse {
    return !!res.promise;
  }

  /**
   * 发送请求的核心方法，返回一个 Promise
   * @param options
   * @param isSkipWarn
   * @returns
   */
  send<R = any, D = any>(options: IRequestConfig<D> = {}, isSkipWarn = false) {
    const settings = merge({}, this.settings, options.settings || {});
    const config = omit<IRequestConfig<D>, IRequestConfig<D>>(options, ['settings']);
    const id = uuid(false);
    const source = axios.CancelToken.source();
    this.records[id] = { settings, config, source };
    const url = this.createUrl(config);
    const headers = this.createHeaders(id, settings, config);
    const { data, params } = this.createSendData(settings, config, headers, isSkipWarn);
    this.showLoading(settings);
    // 根据接口设置传入的withCredentials修改axios.defaults.withCredentials
    this.axios.defaults.withCredentials = settings?.withCredentials || false;
    return new Promise<R>((resolve, reject) => {
      this.axios({
        cancelToken: source.token,
        ...config,
        url,
        headers,
        data,
        params
      })
        .then((res) => {
          if (this.isSkipWarnResponse(res)) {
            return resolve(res.promise);
          }
          if (this.validResponse(settings, res)) {
            return resolve(settings.originResponse ? res : res.data?.data);
          } else {
            this.showError(settings, res.data);
            return reject(res.data);
          }
        })
        .catch((e) => {
          this.showError(settings, e);
          return reject(e);
        })
        .finally(() => {
          delete this.records[id];
          this.closeLoading(settings);
        });
    });
  }

  /**
   * 设置响应拦截器
   * @param onFulfilled
   * @param onRejected
   * @returns
   */
  useResponse(
    onFulfilled?: ((value: AxiosResponse) => AxiosResponse | Promise<AxiosResponse>) | null,
    onRejected?: ((error: any) => any) | null
  ) {
    const { response } = this.axios.interceptors;
    const id = response.use(onFulfilled, onRejected);
    return () => response.eject(id);
  }

  /**
   * 设置请求拦截器
   * @param onFulfilled
   * @param onRejected
   * @returns
   */
  useRequest(
    onFulfilled?:
      | ((value: InternalAxiosRequestConfig) => InternalAxiosRequestConfig | Promise<InternalAxiosRequestConfig>)
      | null,
    onRejected?: ((error: any) => any) | null
  ) {
    const { request } = this.axios.interceptors;
    const id = request.use(onFulfilled, onRejected);
    return () => request.eject(id);
  }

  /**
   * 设置跳过警告的处理程序
   * @param settings
   * @returns
   */
  private setupSkipWarn(settings: IRequestSettings) {
    if (this.stopSkipWarn) {
      this.stopSkipWarn();
      this.stopSkipWarn = undefined;
    }
    if (!settings.skipWarn) return;
    const { code, executor, callback, complete } = settings.skipWarn;
    this.stopSkipWarn = this.useResponse((res) => {
      const headers: Record<string, any> = res.config.headers || {};
      const id = headers[LOCAL_REQUEST_ID] as string;
      const requestCache = this.records[id];
      if (!requestCache) return res;

      const { data } = res;
      if (!data || typeof data !== 'object') return res;
      if (data?.code === code) {
        callback && callback(res);
        const promise = new Promise(executor).then(() => {
          return this.send(
            {
              ...requestCache.config,
              settings: requestCache.settings
            },
            true
          );
        });
        promise
          .catch((e) => e)
          .finally(() => {
            complete && complete();
          });

        (res as IRequestSkipWarnResponse).promise = promise;
      }
      return res;
    });
  }
}

export interface IStaticRequest extends Request {
  (options: IRequestConfig): Promise<AxiosResponse>;
  instance: Request;
}

/**
 * 创建一个 Request 实例并返回静态请求方法
 */
export function createRequest(options: IRequestOptions = {}): IStaticRequest {
  const request = new Request(options);
  const send = request.send.bind(request);
  const cancel = request.cancel.bind(request);
  const setConfig = request.setConfig.bind(request);
  const useRequest = request.useRequest.bind(request);
  const useResponse = request.useResponse.bind(request);
  return Object.assign(send, {
    ...request,
    instance: request,
    send,
    cancel,
    setConfig,
    useRequest,
    useResponse
  }) as IStaticRequest;
}
export const request: IStaticRequest = createRequest({
  settings: {
    injectHeaders: true,
    loading: true,
    originResponse: true
  }
});

/**
 * 根据配置创建 API 方法
 */
export function createApi<R = any, D = any>(config: string | IRequestConfig) {
  const _conifg: IRequestConfig = typeof config === 'string' ? { url: config } : config;
  return (data?: D, opts?: IRequestConfig) => request.send<R, D>(merge(_conifg, opts || {}, { data }));
}
export interface IApiMap {
  [name: string]: string | IRequestConfig;
}
export function createApis(map: IApiMap) {
  const apis: Record<string, ReturnType<typeof createApi>> = {};
  for (const [name, opts] of Object.entries(map)) {
    apis[name] = createApi(opts);
  }
  return apis;
}

/**
 * 封装请求逻辑的 Vue 组合式函数，用于处理响应、错误和加载状态
 * @param api
 * @param transform
 * @returns
 */
export function useApi<R = any>(api: Promise<R>, transform?: (res: any) => R) {
  const data = ref<R | null>(null);
  const error = ref<any>();
  const loading = ref<boolean>(true);
  api
    .then((res: any) => {
      data.value = transform ? transform(res) : res;
    })
    .catch((err) => {
      console.error(err);
      error.value = err;
    })
    .finally(() => {
      loading.value = false;
    });
  return {
    data,
    error,
    loading
  };
}

export { axios, LOCAL_REQUEST_ID, type AxiosRequestConfig, type AxiosResponse, type RawAxiosRequestHeaders };
