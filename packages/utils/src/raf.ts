import { isClient } from './util';

// requestAnimationFrame polyfill
export const rAF = (fn: () => void) =>
  isClient ? window.requestAnimationFrame(fn) : (setTimeout(fn, 16) as unknown as number);

// cancelAnimationFrame polyfill
export const cAF = (handle: number) => (isClient ? window.cancelAnimationFrame(handle) : clearTimeout(handle));
