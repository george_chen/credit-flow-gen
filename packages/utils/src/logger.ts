/**
 * 可配置的日志系统，支持不同的日志级别和业务名称过滤。
 * 通过 Logger 类，可以记录和过滤日志消息，并添加业务名称前缀
 */
// 定义日志级别类型
export type LoggerLevel = 'debug' | 'log' | 'info' | 'warn' | 'error';

// 定义选项接口和日志级别映射
interface Options {
  level?: LoggerLevel;
  bizName?: string;
}
const levels = { debug: -1, log: 0, info: 0, warn: 1, error: 2 };

/**
 * 高阶函数，返回一个记录日志的函数。它会根据目标日志级别和业务名称来决定是否记录日志。
 * @param logLevel
 * @param targetLevel
 * @param bizName
 * @param targetBizName
 * @returns
 */
const record = function (logLevel: LoggerLevel, targetLevel: LoggerLevel, bizName: string, targetBizName: string) {
  return function (...args: any[]) {
    if (targetLevel && levels[targetLevel] <= levels[logLevel]) {
      // hack: IE9下没有apply函数
      if (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        console[logLevel].apply &&
        (targetBizName === '*' || bizName.startsWith(targetBizName))
      ) {
        // eslint-disable-next-line prefer-spread
        return console[logLevel].apply(console, getLogArgs(args, bizName));
      }
    }
  };
};

/**
 * 日志消息添加业务名称前缀
 * @param args
 * @param bizName
 * @returns
 */
function getLogArgs(args: any[], bizName: string) {
  if (bizName !== '*') {
    if (typeof args[0] === 'string') {
      args[0] = `[${bizName}] ${args[0]}`;
    } else {
      args = ['[' + bizName + ']'].concat(args);
    }
  }
  return args;
}

/**
 * 解析日志配置字符串，并返回一个包含目标日志级别和业务名称的对象
 * @param logConf
 * @param options
 * @returns
 */
function parseLogConf(logConf: any, options: Options): { targetLevel: LoggerLevel; targetBizName: string } {
  if (!logConf) {
    return {
      targetLevel: options.level as LoggerLevel,
      targetBizName: options.bizName as string
    };
  }
  if (~logConf.indexOf(':')) {
    const pair = logConf.split(':');
    return {
      targetLevel: pair[0],
      targetBizName: pair[1]
    };
  }
  return {
    targetLevel: logConf,
    targetBizName: '*'
  };
}

/**
 * 默认的日志选项
 */
const defaultOptions: Options = {
  level: 'warn',
  bizName: '*'
};

/**
 * 日志类，支持不同的日志级别和业务名称过滤
 */
class Logger {
  config: { targetLevel: LoggerLevel; targetBizName: string };
  options: Options;

  constructor(options: Options) {
    this.options = { ...defaultOptions, ...options };
    const _location = typeof location !== 'undefined' ? location : ({} as any);
    /**
     * __logConf__ 格式为 logLevel[:bizName]
     *  1. log|warn|debug|error
     *  2. log|warn|debug|error:*
     *  3. log|warn|debug|error:bizName
     */
    const logConf = (/__(?:logConf|logLevel)__=([^#/&]*)/.exec(_location.href) || [])[1];
    this.config = parseLogConf(logConf, options);
  }

  private _log(level: LoggerLevel): any {
    const { targetLevel, targetBizName } = this.config;
    const { bizName } = this.options;
    return record(level, targetLevel, bizName as string, targetBizName);
  }

  debug(...args: any[]): any {
    return this._log('debug')(...args);
  }

  log(...args: any[]): any {
    return this._log('log')(...args);
  }

  info(...args: any[]): any {
    return this._log('info')(...args);
  }

  warn(...args: any[]): any {
    return this._log('warn')(...args);
  }

  error(...args: any[]): any {
    return this._log('error')(...args);
  }
}

export function getLogger(config: { level: LoggerLevel; bizName: string }): Logger {
  return new Logger(config);
}
const logger = getLogger({ level: 'log', bizName: 'CFG' });

export { Logger, logger };
