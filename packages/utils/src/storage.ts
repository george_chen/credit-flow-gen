/**
 * 统一接口，用于在不同存储类型（如缓存、localStorage、sessionStorage）中存储、获取、删除和清空数据。
 * 通过传入选项，可以灵活配置存储类型、过期时间和键前缀。
 */
import { isClient } from './util';

export interface StorageOptions {
  /**
   * 存储类型
   */
  type: 'cache' | 'local' | 'session';

  /**
   * 在多少毫秒后失效, 0为永不过期
   */
  expired: number;

  /**
   * key前缀
   */
  prefix: string;
}

export interface StorageTypes {
  local: any;
  session: any;
  cache: any;
}

/**
 * Storage 类，用于在不同存储类型中存储、获取、删除和清空数据。
 */
export class Storage {
  // 默认存储选项，默认为：缓存 (cache)、永不过期 (expired: 0)、键前缀。
  options: StorageOptions = {
    type: 'cache',
    expired: 0,
    prefix: '__CFG_'
  };
  // 存储缓存数据的对象
  private caches: Record<string, any> = {};
  // 存储类型的对象
  private types: StorageTypes;

  constructor(options: Partial<StorageOptions> = {}) {
    // 根据 isClient 判断环境，客户端使用 localStorage 和 sessionStorage，服务器端使用 caches
    this.types = {
      local: isClient ? window.localStorage : this.caches,
      session: isClient ? window.sessionStorage : this.caches,
      cache: this.caches
    };
    this.config(options);
  }

  /**
   * 配置存储选项
   * @param options
   */
  config(options: Partial<StorageOptions> = {}) {
    this.options = Object.assign(this.options, options);
  }

  /**
   * 保存数据
   * @param key
   * @param value
   * @param opts
   */
  save(key: string, value: any, opts: Partial<StorageOptions> = {}) {
    const { type, expired, prefix } = { ...this.options, ...opts };
    const timestamp = Date.now();
    const realKey = prefix + key;
    const storage = this.types[type] || this.caches;
    const info = {
      value,
      timestamp,
      expired
    };
    if (storage === this.caches) {
      storage[realKey] = info;
    } else {
      storage.setItem(realKey, JSON.stringify(info));
    }
  }

  /**
   * 获取数据
   */
  get(key: string, opts: Partial<StorageOptions> = {}) {
    const { type, prefix } = { ...this.options, ...opts };
    const realKey = prefix + key;
    const storage = this.types[type] || this.caches;
    let info;
    if (storage === this.caches) {
      info = storage[realKey];
    } else {
      const content = storage.getItem(realKey);
      if (content) {
        info = JSON.parse(content);
      }
    }
    // 不存在缓存
    if (!info) return null;
    const { value, timestamp, expired } = info;
    // 缓存是否过期
    const isExpired = expired > 0 && timestamp + expired < Date.now();
    // 过期清空缓存，返回null
    if (isExpired) {
      this.remove(key, opts);
      return null;
    }
    return value;
  }

  /**
   * 删除数据
   * @param key
   * @param opts
   */
  remove(key: string, opts: Partial<StorageOptions> = {}) {
    const { type, prefix } = { ...this.options, ...opts };
    const storage = this.types[type] || this.caches;
    const realKey = prefix + key;
    if (storage === this.caches) {
      delete storage[realKey];
    } else {
      storage.removeItem(realKey);
    }
  }

  /**
   * 清空数据
   * @param opts
   */
  clear(opts: Partial<StorageOptions> = {}) {
    const { type } = { ...this.options, ...opts };
    const storage = this.types[type] || this.caches;
    if (storage === this.caches) {
      this.caches = {};
    } else {
      storage.clear();
    }
  }
}

export const storage = new Storage();
