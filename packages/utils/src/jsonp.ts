import fetchJsonp from 'fetch-jsonp';
import { append } from './url';
import { template } from 'cfg-packages-base';

export interface FetchJsonpOptions extends fetchJsonp.Options {
  query?: Record<string, any>;
}

/**
 * 处理 JSONP（JSON with Padding）请求的方法
 * @param url 要请求的 URL
 * @param options FetchJsonpOptions 类型的可选参数，包含请求选项和查询参数
 * @returns 返回一个 Promise，resolve 时返回请求结果
 */
export async function jsonp<T = any>(url: string, options: FetchJsonpOptions = {}): Promise<T> {
  const { query = {} } = options;
  // 1. 如果url中包含${}，则进行模板编译
  if (url.includes('${')) {
    const compiled = template(url);
    url = compiled(query || {});
  }
  // 2. 附加查询参数，拼接url
  const _url = append(url, query);
  // 3. 发起jsonp请求并解析响应
  const res = await fetchJsonp(_url, options);
  return await (res.json() as Promise<T>);
}

export type Jsonp = typeof jsonp;
