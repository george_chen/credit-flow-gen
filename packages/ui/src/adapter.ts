import { type Plugin, type App, type InjectionKey, inject, getCurrentInstance } from 'vue';
import { storage } from 'cfg-packages-utils';
import { type GridCustomInfo, type BuiltinFieldEditor, registerFieldEditors } from './components';
import type { VXETableConfigOptions } from 'vxe-table';

// 定义-注入键，用于在 Vue 应用中提供和注入 Adapter 对象
export const ADAPTER_KEY: InjectionKey<Adapter> = Symbol('ADAPTER_KEY');

// 定义-上传响应的接口，包含上传后的 URL 和文件名等信息
export interface UploaderResponse {
  url: string;
  name?: string;
  [index: string]: any;
}

// 定义-上传函数类型，接受一个 File 对象，返回一个 Promise(解析为 string、UploaderResponse 或 null)
export type Uploader = (file: File) => Promise<string | UploaderResponse | null>;

/**
 * 组合式函数-获取当前实例的适配器对象
 * @returns
 */
export function useAdapter(): Adapter {
  const instance = getCurrentInstance();
  const adapter = instance?.appContext.config.globalProperties.$adapter;
  if (adapter) {
    return adapter;
  }
  return inject(ADAPTER_KEY, {} as Adapter);
}

// Adapter 接口定义
export interface Adapter {
  fieldEditors?: Record<string, BuiltinFieldEditor>;
  uploader?: Uploader;
  vxeConfig?: VXETableConfigOptions;
  vxePlugin?: any;
  getCustom?: (id: string) => Promise<GridCustomInfo>;
  saveCustom?: (info: GridCustomInfo) => Promise<any>;
  [index: string]: any;
}

// 默认适配器对象
const defaults: Adapter = {
  getCustom: async (id: string) => {
    return storage.get(id, { type: 'local' });
  },
  saveCustom: async (info: GridCustomInfo) => {
    storage.save(info.id, info, { type: 'local' });
  }
};

// Adapter Vue 插件-install 方法，用于注册适配器
export const AdapterPlugin: Plugin = {
  install(app: App, options: Adapter = {}) {
    const adapter = Object.assign(defaults, options);
    if (adapter.fieldEditors) {
      registerFieldEditors(adapter.fieldEditors);
    }
    app.config.globalProperties.$adapter = adapter;
    // 将 adapter 提供给 Vue 应用上下文
    app.provide(ADAPTER_KEY, adapter);
  }
};
