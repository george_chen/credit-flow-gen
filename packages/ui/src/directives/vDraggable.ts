import { type Directive, watch, effectScope, type EffectScope, type MaybeRef, unref } from 'vue';
import { useDraggable, type UseDraggableOptions } from '@vueuse/core';
import { isEqual } from 'cfg-packages-utils';

// 扩展全局的HTMLElement接口，添加一个可选的__draggable__属性，该属性可以存储一个Draggable实例或null
declare global {
  interface HTMLElement {
    __draggable__?: Draggable | null;
  }
}

// 在拖拽时添加到document.body的类名
const BODY_CLASS = 'user-select-none';

// 定义-拖拽配置选项
export interface DraggableOptions extends UseDraggableOptions {
  /**
   * 限制在元素范围内拖拽
   */
  target?: MaybeRef<HTMLElement> | string;

  /**
   * 拖拽 handle 选择器
   */
  selector?: string;

  /**
   *  禁用
   */
  disabled?: boolean;

  /**
   * 延时
   */
  delay?: number;

  /**
   * 目标边缘距离
   */
  edge?: number;
}

/**
 * Draggable类，封装拖拽逻辑
 */
export class Draggable {
  private scope: EffectScope;
  public dragging = false;

  constructor(public el: HTMLElement, public options: DraggableOptions = {}) {
    // 创建一个副作用作用域
    this.scope = effectScope();
    this.scope.run(() => {
      this.init();
    });
  }

  // 根据selector选项获取拖拽句柄元素
  private getHandle(): any {
    const { selector, handle } = this.options;
    return selector ? this.el.querySelector(selector) : handle;
  }

  // 根据target选项获取拖拽目标元素
  private getTarget() {
    const { target = 'body' } = this.options;
    return typeof target === 'string' ? document.querySelector(target) : unref(target) || document.body;
  }

  init() {
    const { el, options } = this;
    const { disabled, delay = 150, onStart, onEnd } = options;
    if (disabled) return;
    let timer: any = null;

    const handle = this.getHandle();
    const target = this.getTarget();

    let rect = el.getBoundingClientRect();
    let targetRect: DOMRect | null = null;
    const { x, y } = useDraggable(el, {
      initialValue: { x: rect.x, y: rect.y },
      ...options,
      handle,
      onStart: (position, e) => {
        document.body.classList.add(BODY_CLASS);
        clearTimeout(timer);
        timer = setTimeout(() => {
          this.dragging = true;
          rect = el.getBoundingClientRect();
          targetRect = target?.getBoundingClientRect() as DOMRect;
          onStart && onStart(position, e);
        }, delay);
      },
      onEnd: (position, e) => {
        clearTimeout(timer);
        document.body.classList.remove(BODY_CLASS);
        if (this.dragging && targetRect) {
          this.dragging = false;
          const { x, y } = position;
          const endPostion = this.getPosition(targetRect, rect, x, y);
          onEnd && onEnd(endPostion, e);
          targetRect = null;
        }
      }
    });

    watch([x, y], () => {
      if (this.dragging && targetRect) {
        const position = this.getPosition(targetRect, rect, x.value, y.value);
        el.style.left = `${position.x}px`;
        el.style.top = `${position.y}px`;
      }
    });
  }

  // 计算元素在拖拽时的位置，考虑边界和边缘距离
  getPosition(targetRect: DOMRect, rect: DOMRect, x: number, y: number) {
    const { edge = 50 } = this.options;
    const xMin = -rect.width + edge;
    const xMax = targetRect.width - edge;
    const yMin = 0;
    const yMax = targetRect.height - edge;

    const left = Math.min(xMax, Math.max(x, xMin));
    const top = Math.min(yMax, Math.max(y, yMin));
    return {
      x: left,
      y: top
    };
  }

  destory() {
    this.scope.stop();
  }
}

/**
 * 指令-可拖拽指令（v-draggable），为元素添加拖拽功能
 */
export const vDraggable: Directive<HTMLElement, DraggableOptions> = {
  // 在元素挂载时创建Draggable实例，并将其存储在元素的__draggable__属性中。
  mounted(el, binding) {
    const options = binding.value || {};
    const draggable = new Draggable(el, options);
    el.__draggable__ = draggable;
  },
  // 在元素更新时检查选项是否变化，如果变化则销毁旧的Draggable实例并创建新的实例。
  updated(el, binding) {
    const options = binding.value || {};
    const draggable = el.__draggable__;
    if (draggable && !isEqual(draggable.options, options)) {
      draggable.destory();
      el.__draggable__ = new Draggable(el, options);
    }
  },
  // 在元素卸载时销毁Draggable实例，并清除__draggable__属性。
  unmounted(el) {
    const draggable = el.__draggable__;
    if (draggable) {
      draggable.destory();
      el.__draggable__ = null;
    }
  }
};
