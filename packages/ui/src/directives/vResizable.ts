import { type Directive, watch, effectScope, type EffectScope, ref, type Ref } from 'vue';
import { isEqual } from 'cfg-packages-utils';
import { useEventListener, useMouseInElement, type Fn } from '@vueuse/core';

// 扩展全局的HTMLElement接口，添加一个可选的__resizable__属性，该属性可以存储一个Resizable实例或null
declare global {
  interface HTMLElement {
    __resizable__?: Resizable | null;
  }
}

// 元素被调整大小时添加到document.body的类名
const BODY_CLASS = 'user-select-none';

export type UseMouseInElementReturn = ReturnType<typeof useMouseInElement>;

// 可调整的方向，可选：'n'（北）、's'（南）、'w'（西）、'e'（东）
export type ResizableDir = 'n' | 's' | 'w' | 'e';

// 配置Resizable的行为
export interface ResizableOptions {
  disabled?: boolean;
  edge?: number;
  dirs?: ResizableDir[];
  onStart?: (dir: string, mie: UseMouseInElementReturn) => void;
  onEnd?: (dir: string, mie: UseMouseInElementReturn) => void;
  onResizing?: (dir: string, mie: UseMouseInElementReturn) => void;
  minWidth?: number;
  minHeight?: number;
  maxWidth?: number;
  maxHeight?: number;
}

/**
 * Resizable类，封装可拖拉调整大小的逻辑
 */
export class Resizable {
  private scope: EffectScope;
  // resizing 和 direction属性：用于跟踪当前是否处于调整大小状态 以及 调整的方向
  public resizing: Ref<boolean> = ref(false);
  public direction: Ref<string> = ref('');
  // MIE属性：存储useMouseInElement返回的对象，用于跟踪鼠标在元素内的位置
  public MIE: UseMouseInElementReturn | null = null;
  public cleanMousedown?: Fn;
  public cleanMouseup?: Fn;

  constructor(public el: HTMLElement, public options: ResizableOptions = {}) {
    // 创建一个副作用作用域
    this.scope = effectScope();
    this.scope.run(() => {
      this.init();
    });
  }

  init() {
    const { el, options } = this;
    const { disabled, onStart, onEnd } = options;
    if (disabled) return;

    this.MIE = useMouseInElement(el);

    this.cleanMousedown = useEventListener(document, 'mousedown', () => {
      if (this.direction?.value && this.MIE) {
        this.resizing.value = true;
        el.classList.add('is-resizing', `is-${this.direction.value}-resizing`);
        onStart && onStart(this.direction.value, this.MIE);
      }
    });

    this.cleanMouseup = useEventListener(document, 'mouseup', () => {
      if (this.resizing.value && this.direction?.value && this.MIE) {
        el.classList.remove('is-resizing', `is-${this.direction.value}-resizing`);
        onEnd && onEnd(this.direction.value, this.MIE);
      }
      this.resizing.value = false;
    });

    watch(this.direction, (v) => {
      const body = document.body;
      body.style.cursor = v ? `${v}-resize` : '';
      if (v) {
        body.classList.add(BODY_CLASS);
      } else {
        body.classList.remove(BODY_CLASS);
      }
    });

    const { x, y } = this.MIE;

    watch([x, y], () => {
      if (this.resizing.value) {
        this.resize();
      } else {
        this.direction.value = this.getDirection();
      }
    });
  }

  // 根据鼠标位置和当前方向调整元素的大小。
  resize() {
    const { MIE, direction, resizing, options, el } = this;
    const dir = direction?.value || '';
    if (!MIE || !resizing.value || !dir) return;
    const { x, y, elementX, elementY, elementHeight, elementWidth } = MIE;
    const { onResizing } = options;
    const { minWidth = 0, minHeight = 0, maxWidth = 99999, maxHeight = 99999 } = options;
    if (dir.includes('e')) {
      const width = Math.min(Math.max(elementX.value, minWidth), maxWidth);
      el.style.width = `${width}px`;
    }

    if (dir.includes('s')) {
      const height = Math.min(Math.max(elementY.value, minHeight), maxHeight);
      el.style.height = `${height}px`;
    }

    if (dir.includes('w')) {
      const width = Math.min(Math.max(elementWidth.value - elementX.value, minWidth), maxWidth);
      el.style.width = `${width}px`;
      el.style.left = `${x.value}px`;
    }

    if (dir.includes('n')) {
      const height = Math.min(Math.max(elementHeight.value - elementY.value, minHeight), maxHeight);
      this.el.style.height = `${height}px`;
      this.el.style.top = `${y.value}px`;
    }

    onResizing && onResizing(dir, MIE);
  }

  // 根据鼠标位置确定当前的调整方向。
  getDirection() {
    if (!this.MIE) return '';
    const { elementX, elementY, elementHeight, elementWidth, isOutside } = this.MIE;
    if (isOutside.value) return '';
    const { dirs = ['n', 's', 'w', 'e'], edge = 5 } = this.options;
    let dir = '';
    if (dirs.includes('n') && elementY.value <= edge) {
      dir += 'n';
    } else if (dirs.includes('s') && elementY.value > elementHeight.value - edge) {
      dir += 's';
    }
    if (dirs.includes('w') && elementX.value <= edge) {
      dir += 'w';
    } else if (dirs.includes('e') && elementX.value > elementWidth.value - edge) {
      dir += 'e';
    }
    return dir;
  }

  destory() {
    const body = document.body;
    body.style.cursor = '';
    body.classList.remove(BODY_CLASS);
    this.cleanMousedown && this.cleanMousedown();
    this.cleanMouseup && this.cleanMouseup();
    this.MIE?.stop();
    this.scope.stop();
  }
}

/**
 * 指令-可拖拉调整大小（v-resizable），为元素提供可调整大小的功能
 */
export const vResizable: Directive<HTMLElement, ResizableOptions> = {
  // 在元素挂载时创建Resizable实例，并将其存储在元素的__resizable__属性中。
  mounted(el, binding) {
    const options = binding.value || {};
    const resizable = new Resizable(el, options);
    el.__resizable__ = resizable;
  },
  // 在元素更新时检查选项是否变化，如果变化则销毁旧的Resizable实例并创建新的实例。
  updated(el, binding) {
    const options = binding.value || {};
    const resizable = el.__resizable__;
    if (resizable && !isEqual(resizable.options, options)) {
      resizable.destory();
      el.__resizable__ = new Resizable(el, options);
    }
  },
  // 在元素卸载时销毁Resizable实例，并清除__resizable__属性。
  unmounted(el) {
    const resizable = el.__resizable__;
    if (resizable) {
      resizable.destory();
      el.__resizable__ = null;
    }
  }
};
