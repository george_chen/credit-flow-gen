import type { App } from 'vue';

import { INSTALLED_KEY } from '../constants';
import { ADAPTER_KEY, type Adapter } from '../adapter';

export interface InstallerOptions {
  adapter?: Adapter;
}

/**
 * Vue install，包括：批量注册组件，并可选地提供适配器对象。
 * 简化组件和依赖的注册过程，确保组件只被安装一次，并通过 Vue 的依赖注入机制提供适配器对象。
 * @param components
 * @returns
 */
export const makeInstaller = (components: any[] = []) => {
  const install = (app: App, options: InstallerOptions = {}) => {
    if ((app as any)[INSTALLED_KEY]) return;

    (app as any)[INSTALLED_KEY] = true;

    // 注册组件
    components.forEach((c) => {
      if (c.name) {
        app.component(c.name, c);
      }
    });

    // 提供适配器对象
    if (options.adapter) {
      app.provide(ADAPTER_KEY, options.adapter);
    }
  };
  return {
    install
  };
};
