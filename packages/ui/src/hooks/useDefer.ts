import { ref, onMounted, onUnmounted } from 'vue';
import { rAF, cAF } from 'cfg-packages-utils';

/**
 * 组合式函数-延时渲染
 * @param maxFrameCount
 * @returns
 * ex:
 * const defer = useDefer();
 * v-if="defer(10)" // 在第10帧开始渲染
 */
export function useDefer(maxFrameCount = 1000) {
  const frameCount = ref(0);
  let id: number;

  const refreshFrameCount = () => {
    id = rAF(() => {
      ++frameCount.value;
      if (frameCount.value < maxFrameCount) {
        refreshFrameCount();
      }
    });
  };

  onMounted(refreshFrameCount);

  onUnmounted(() => {
    if (id) {
      cAF(id);
    }
  });

  return (showInFrameCount: number) => {
    return frameCount.value >= showInFrameCount;
  };
}
