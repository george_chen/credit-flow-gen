import { type MaybeRef, computed, unref } from 'vue';

export type IUseDisabled = MaybeRef<undefined | boolean | ((...args: any[]) => boolean)>;

/**
 * 组合式函数-用于确定某个 UI 元素（如按钮）是否应该被禁用
 * @param disabled
 * @param params
 * @returns 使用 computed 返回一个计算属性。这个计算属性在每次其依赖项（即 disabled）发生变化时都会重新计算
 */
export function useDisabled(disabled: IUseDisabled, params?: any) {
  return computed(() => {
    const value = unref(disabled);
    return typeof value === 'function' ? value(params) : !!value;
  });
}
