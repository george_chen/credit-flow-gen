import { type MaybeRef, type ShallowRef, type Ref, shallowRef, unref, watch, ref } from 'vue';

export interface UseLoaderResult<T> {
  data: ShallowRef<T>;
  loading: Ref<boolean>;
  loader: (params?: any) => Promise<T>;
}

/**
 * 组合式函数-处理数据加载器（Loader）的逻辑，一般用于异步请求数据后加载数据的逻辑。
 * 允许传入一个数据加载器和默认值，并自动管理数据加载和状态（如：加载中状态）
 * @param loaderRef 一个数据（同步或异步）或 一个返回数据（同步或异步）的函数
 * @param defaultValue 数据加载前 或 加载失败时 使用的默认值
 * @param params 传递给加载器函数的参数
 * @returns 对象，包括：
 * data：使用shallowRef包装的加载后的数据。
 * loading：表示加载状态的ref。
 * loader：手动触发加载的函数。
 */
export function useLoader<T = any, P = any>(
  loaderRef: MaybeRef<T | ((params?: P) => T | Promise<T>)>,
  defaultValue: T,
  params?: P
): UseLoaderResult<T> {
  const data = shallowRef(defaultValue);
  const loading = ref(false);

  const run = async (params?: P) => {
    const loader = unref(loaderRef);
    if (!loader) return defaultValue;

    return typeof loader === 'function' ? await (loader as (params?: P) => T | Promise<T>)(params) : loader;
  };

  const doAction = async (params?: P) => {
    loading.value = true;
    return await run(params).then((res) => {
      data.value = res || defaultValue;
      loading.value = false;
      return unref(data);
    });
  };

  watch(
    () => loaderRef,
    () => {
      doAction(params);
    },
    { immediate: true }
  );

  if (params) {
    watch(params, () => {
      doAction(params);
    });
  }

  return {
    data,
    loading,
    loader: doAction
  };
}
