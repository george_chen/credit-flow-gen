/**!
 * Copyright (c) 2024, credit-flow-gen CFG All rights reserved.
 * @name cfg-packages-ui
 * @version 0.0.50
 */
export const version = '0.0.50';
