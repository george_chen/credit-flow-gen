/**
 * 面板组件，包括：PanelHeader、PanelBody、PanelFooter
 */
import XPanel from './Panel.vue';
export { XPanel };
export * from './types';
