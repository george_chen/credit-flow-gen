/**
 * 支持无限滚动或者分页的列表组件
 */
import XList from './List.vue';
export { XList };
export * from './types';
