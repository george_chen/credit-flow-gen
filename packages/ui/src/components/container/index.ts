/**
 * 其余组件使用的容器组件
 */
import XContainer from './Container.vue';
export { XContainer };
export * from './types';
