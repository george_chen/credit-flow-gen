/**
 * vxetable.interceptor 拦截器
 */
import type { VxeGlobalInterceptorHandles } from 'vxe-table';

export const handleClerEdit: VxeGlobalInterceptorHandles.InterceptorCallback = (params) => {
  const { $grid, $event, $table } = params;
  const parent = $table.getParentElem();
  if (parent.contains($event.target)) {
    $grid.clearValidate();
  } else {
    // 如果不是表格组件子节点触发的clearEdit事件，阻止关闭编辑模式
    return false;
  }
};

export const handleClearFilter: VxeGlobalInterceptorHandles.InterceptorCallback = (params) => {
  const { $event, $table } = params;
  const parent = $table.getParentElem();
  if (parent.contains($event.target)) {
    // $grid.clearValidate();
  } else {
    // 如果不是表格组件子节点触发的clearFilter事件，阻止关闭筛选模式
    return false;
  }
};
