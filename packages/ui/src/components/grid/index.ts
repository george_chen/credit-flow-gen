/**
 * 基于 vxe-table 的高性能、高度定制化 数据表格组件
 */
import XGrid from './Grid.vue';
export { XGrid };
export * from './types';
