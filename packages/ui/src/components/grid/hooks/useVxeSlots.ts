import { useSlots } from 'vue';
/**
 * 组合式函数-使用vxe-table插槽
 * @param excludes
 * @returns
 */
export function useVxeSlots(excludes: string[] = []) {
  const slots = useSlots();
  return Object.keys(slots).filter((n) => !excludes.includes(n));
}
