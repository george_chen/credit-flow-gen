import { getCurrentInstance } from 'vue';
// 基于 Vue.js 的高性能表格组件库，用于创建功能强大、灵活和高效的数据表格。
import {
  VxeTableFilterModule,
  VxeTableEditModule,
  VxeTableMenuModule,
  VxeTableExportModule,
  VxeTableKeyboardModule,
  VxeTableValidatorModule,
  VxeTableCustomModule,
  VxeGrid,
  VXETable,
  VxeTooltip,
  VxeToolbar,
  VxeModal,
  type VXETableConfigOptions
} from 'vxe-table';
import { useAdapter } from '../../../adapter';
import { RenderPlugin } from '../renderers';

/**
 * 组合式函数-使用vxe-table相关功能模块，返回VxeGrid和VXETable
 * @param options
 * @returns
 */
export function useVxe(options: VXETableConfigOptions = {}) {
  const modules = [
    VxeTableFilterModule,
    VxeTableEditModule,
    VxeTableMenuModule,
    VxeTableExportModule,
    VxeTableKeyboardModule,
    VxeTableValidatorModule,
    VxeTableCustomModule,
    VxeGrid,
    VxeTooltip,
    VxeToolbar,
    VxeModal
  ];
  const instance = getCurrentInstance();
  const app = instance?.appContext.app;
  const { vxeConfig, vxePlugin } = useAdapter();

  if (app && !(app as any).__installVxe) {
    VXETable.use(RenderPlugin);
    if (vxePlugin) {
      VXETable.use(vxePlugin);
    }
    VXETable.setConfig({
      ...vxeConfig,
      ...options
    });
    modules.forEach((n) => app.use(n));
    (app as any).__installVxe = true;
  }

  return {
    VxeGrid,
    VXETable
  };
}
