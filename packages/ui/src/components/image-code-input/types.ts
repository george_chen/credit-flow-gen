import type { PropType } from 'vue';
import type { ComponentPropsType } from '../shared';

export const imageCodeInputProps = {
  image: {
    type: Function as PropType<() => Promise<string>>
  },
  maxLength: {
    type: Number,
    default: 4
  },
  placeholder: {
    type: String,
    default: '请输入图形验证码'
  },
  validate: {
    type: Function as PropType<(value: string) => Promise<boolean>>
  }
};

export type ImageCodeInputProps = ComponentPropsType<typeof imageCodeInputProps>;
