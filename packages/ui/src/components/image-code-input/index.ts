/**
 * 图形验证码输入框组件
 */
import XImageCodeInput from './ImageCodeInput.vue';
export { XImageCodeInput };
export * from './types';
