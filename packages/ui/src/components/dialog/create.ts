import { createVNode, render, type AppContext } from 'vue';
import { type DialogProps } from './types';
import XDialog from './Dialog.vue';

export type CreateDialogProps = DialogProps & { [index: string]: any };

/**
 * 创建Dialog 对话框
 * @param props
 * @param context
 * @returns
 */
export function createDialog(props: CreateDialogProps, context?: AppContext | null) {
  const container = document.createElement('div');
  const vnode = createVNode(XDialog, props);
  vnode.appContext = context ?? (createDialog as any)._context;
  render(vnode, container);

  const destroy = () => {
    render(null, container);
    container.parentNode?.removeChild(container);
  };

  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  vnode.props!.onDestroy = () => {
    destroy();
  };

  // vnode.props!.onClose = () => {
  //   destroy();
  // };

  document.body.appendChild(container);

  return {
    vnode,
    destroy
  };
}
