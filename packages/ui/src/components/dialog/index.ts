/**
 * Dialog 对话框
 */
import XDialog from './Dialog.vue';
export { XDialog };
export * from './types';
export * from './create';
