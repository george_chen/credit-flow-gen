import type { ComponentPropsType } from '../shared';
import logo from './assets/logo.svg';

export const startupProps = {
  logo: {
    type: String,
    default: logo
  },
  name: {
    type: String,
    default: 'CFG低代码平台'
  },
  tagline: {
    type: String,
    default: '基于 Vue3 + TypeScript 快速打造的低代码平台'
  },
  actionText: {
    type: String,
    default: '开始使用'
  }
};

export type StartupProps = ComponentPropsType<typeof startupProps>;
