/**
 * 基于ElForm二次封装的表单组件
 */
import XForm from './Form.vue';
export { XForm };
export * from './types';
