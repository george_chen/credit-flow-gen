import {
  ref,
  h,
  Teleport,
  reactive,
  computed,
  nextTick,
  provide,
  toRaw,
  getCurrentInstance,
  type ComponentInternalInstance
} from 'vue';
import { routeLocationKey, type RouteLocationNormalizedLoaded } from 'vue-router';
import { upperFirstCamelCase } from 'cfg-packages-utils';
import { useEventListener } from '@vueuse/core';
import { createDialog } from '../../';
import type { MaskTab } from '../types';
import { useTabs } from './useTabs';

// useTabs 函数的返回类型，用作 useContent 函数的参数类型
export type UseContentOptions = ReturnType<typeof useTabs>;

/**
 * XMask使用的组合式函数。功能：创建视图、管理对话框、缓存控制和拖放操作
 * @param options
 * @returns
 */
export function useContent(options: Partial<UseContentOptions>) {
  const views = new Map(); // 存储视图的 Map 对象
  const exclude = ref<string[]>([]); // 存储需要排除的视图名称的响应式引用
  const dialogs = reactive<any>({}); // 存储对话框实例的响应式对象
  const dialogInstances: Record<string, any> = {}; // 存储对话框实例的对象
  const instance = getCurrentInstance(); // 当前组件实例
  const {
    updateTab,
    // isCurrentTab,
    // activeHome,
    tabs
  } = options as UseContentOptions; // 解构 useTabs 函数返回的属性

  /**
   * 创建并返回视图对象，如果视图已存在则返回已存在的视图
   * @param module
   * @param route
   * @returns
   */
  const createView = (module: any, route: RouteLocationNormalizedLoaded) => {
    const fullPath = route.fullPath;
    if (views.has(fullPath)) {
      return views.get(fullPath);
    } else {
      const name = upperFirstCamelCase(fullPath);
      const view = {
        name,
        setup() {
          const to = computed(() => dialogs[fullPath] || document.body);
          const disabled = computed(() => !dialogs[fullPath]);
          const visible = computed(() => {
            return !exclude.value.includes(name);
          });
          provide(routeLocationKey, toRaw({ ...route }) as RouteLocationNormalizedLoaded);
          return () => {
            return visible.value
              ? h(
                  Teleport,
                  {
                    to: to.value,
                    disabled: disabled.value
                  },
                  [h(module)]
                )
              : null;
          };
        }
      };
      views.set(fullPath, view);
      return view;
    }
  };

  /**
   * 关闭单个对话框并清理相关数据
   * @param tab
   */
  const closeDialog = (tab: MaskTab) => {
    tab.dialog = undefined;
    delete dialogs[tab.url];
    const instance = dialogInstances[tab.id];
    if (instance) {
      instance.destroy();
      delete dialogInstances[tab.id];
      updateTab(tab);
    }
  };

  /**
   * 遍历多个对话框并关闭
   * @param tabs
   */
  const closeDialogs = (tabs: MaskTab[] = []) => {
    tabs.forEach((tab) => {
      closeDialog(tab);
    });
  };

  /**
   * 异步打开对话框，并更新对话框和标签数据
   * @param tab
   * @returns
   */
  const openDialog = async (tab: MaskTab) => {
    tab.dialog = {
      ...tab.dialog,
      onMinimized: () => {
        closeDialog(tab);
      },
      onClose: async () => {
        closeDialog(tab);
        // tabs.value = tabs.value.filter((n) => n.id !== tab.id);
      }
    };

    updateTab(tab);

    const dialog = createDialog(
      {
        title: tab.title,
        icon: tab.icon,
        modal: false,
        resizable: true,
        draggable: true,
        maximizable: true,
        minimizable: true,
        ...tab.dialog,
        onOpen(dialog: ComponentInternalInstance) {
          dialogs[tab.url] = (dialog.refs.panelRef as any)?.bodyRef?.$el;
        }
      },
      instance?.appContext
    );

    await nextTick();
    // if (isCurrentTab(tab)) {
    //   activeHome();
    // }
    dialogInstances[tab.id] = dialog;
    return dialog;
  };

  /**
   * 刷新指定标签
   * @param tab
   */
  const refresh = async (tab: MaskTab) => {
    exclude.value = [tab.name];
    await nextTick();
    exclude.value = [];
  };

  /**
   * 清理指定标签的缓存
   * @param tabs
   */
  const cleanCache = async (tabs: MaskTab[]) => {
    exclude.value = tabs.map((n) => n.name);
    await nextTick();
    exclude.value = [];
  };

  /**
   * 拖动悬停事件处理函数
   * @param e
   * @returns
   */
  const onDragover = (e: any) => {
    e.preventDefault();
    return false;
  };

  /**
   * 拖放事件处理函数
   * @param e
   */
  const onDrop = (e: any) => {
    if (e.dataTransfer) {
      const id = e.dataTransfer.getData('tab');
      const tab = tabs.value.find((n) => n.id === id);
      if (tab) {
        const { clientX, clientY } = e;
        tab.dialog = {
          left: clientX,
          top: clientY
        };
        openDialog(tab);
      }
    }
  };

  /**
   * 检查指定 URL 是否有打开的对话框
   * @param url
   * @returns
   */
  const hasDialog = (url: string) => {
    return !!dialogs[url];
  };

  // 监听 dragover 和 drop 事件
  useEventListener(document, 'dragover', onDragover);
  useEventListener(document, 'drop', onDrop);

  return {
    createView,
    openDialog,
    refresh,
    exclude,
    cleanCache,
    hasDialog,
    closeDialog,
    closeDialogs
  };
}
