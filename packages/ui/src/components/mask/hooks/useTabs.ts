import { computed, watch, ref, type ComputedRef, type Ref, nextTick, provide } from 'vue';
import { useRouter, useRoute } from 'vue-router';
import { ElMessageBox } from 'element-plus';
import { upperFirstCamelCase, uid } from 'cfg-packages-utils';
import { useElementSize } from '@vueuse/core';
import {
  type MaskProps,
  type MaskEmitsFn,
  TAB_ITEM_WIDTH,
  type MaskTab,
  type TabCreator,
  TAB_CREATORS_KEY
} from '../types';
import type { MenuDataItem } from '../../';

/**
 * XMask使用的组合式函数。管理和操作标签页（Tabs）的状态和行为
 * @param _props
 * @param _emit
 * @param menus
 * @param active
 * @param home
 * @returns
 */
export function useTabs(
  _props: MaskProps,
  _emit: MaskEmitsFn,
  menus: ComputedRef<MenuDataItem[]>,
  active: Ref<MenuDataItem | null>,
  home: ComputedRef<MaskTab>
) {
  // 获取路由对象
  const route = useRoute();
  const router = useRouter();
  // 存储标签页创建函数的对象
  const tabCreators: Record<string, TabCreator> = {};
  // 存储所有标签页的响应式数组
  const tabs: Ref<MaskTab[]> = ref([]);
  // 标签页容器的引用
  const tabRef = ref();
  // 标签页容器的宽度
  const { width } = useElementSize(tabRef);
  // 计算属性，显示的标签页数量
  const showCount = computed(() => Math.floor(width.value / TAB_ITEM_WIDTH));
  // 计算属性，当前显示的标签页
  const showTabs = computed(() => tabs.value.slice(0, showCount.value).filter((n) => !n.dialog));
  // 计算属性，当前在下拉菜单中的标签页
  const dropdownTabs = computed(() => tabs.value.slice(showCount.value));
  // 根据URL获取菜单项
  const getMenuByUrl = (url: string) => menus.value.find((n) => n.url === url);

  /**
   * 检查是否为当前激活的标签页
   * @param tab
   * @returns
   */
  const isCurrentTab = (tab: MaskTab) => {
    return route.fullPath === tab.url;
  };

  /**
   * 根据 ID 获取标签页
   * @param id
   * @returns
   */
  const getTab = (id: string) => {
    if (home.value.id === id) {
      return home.value;
    }
    return tabs.value.find((n) => n.id === id);
  };

  // 当前激活标签页的 ID
  const tabValue = ref<string>('');
  // 计算属性，当前激活的标签页
  const currentTab = computed(() => getTab(tabValue.value));

  /**
   * 切换到指定的标签页
   * @param tab
   */
  const changeTab = (tab: MaskTab) => {
    // 如果是项目列表页则直接跳转
    if (tab.url === '/#/project-list') {
      activeHome();
      return;
    }
    router.push(tab.url).catch((e) => e);
  };

  /**
   * 激活指定的标签页
   * @param tab
   */
  const activeTab = (tab: MaskTab) => {
    tabValue.value = tab.id;
    if (route.fullPath !== tab.url) {
      changeTab(tab);
    }
  };

  /**
   * 激活主页标签页，跳转到项目列表页
   */
  const activeHome = () => {
    const path = window.location.pathname + '#/project-list';
    window.location.href = path;
  };

  /**
   * 添加标签页，如果标签页已存在则激活它，否则添加新标签页并激活
   * @param tab
   */
  const addTab = (tab: MaskTab) => {
    // 检测tab是否已经存在
    const match = tabs.value.find((n) => n.url === tab.url || n.id === tab.id);
    if (match) {
      activeTab(match);
    } else {
      tabs.value.unshift(tab);
      activeTab(tab);
    }
  };

  /**
   * 创建一个新的标签页
   * @param menu
   * @returns
   */
  const createTab = async (menu?: MenuDataItem): Promise<MaskTab> => {
    const { url = route.fullPath, icon, title = '新建标签页' } = menu || {};
    const creator = tabCreators[url];
    const id = uid();
    const name = upperFirstCamelCase(url);
    return {
      id,
      name,
      url,
      icon,
      title,
      closable: true,
      menu,
      ...(creator ? await creator() : {})
    };
  };

  /**
   * 删除指定的标签页
   * @param tab
   * @returns
   */
  const removeTab = async (tab: MaskTab) => {
    const ret = await ElMessageBox.confirm('是否关闭页签', '提示', {
      type: 'warning'
    }).catch(() => false);

    if (!ret) return;

    tabs.value = tabs.value.filter((n) => n.id !== tab.id);
    // 删除的是激活tab
    if (tabValue.value === tab.id) {
      const first = tabs.value[0];
      changeTab(first ? first : home.value);
    }
    return tab;
  };

  /**
   * 更新指定的标签页
   * @param tab
   */
  const updateTab = (tab: MaskTab) => {
    const index = tabs.value.findIndex((n) => n.id === tab.id);
    if (index >= 0) {
      const match = tabs.value[index];
      tabs.value.splice(index, 1, Object.assign(match, tab));
    }
  };

  /**
   * 删除所有标签页
   * @returns
   */
  const removeAllTabs = async () => {
    const ret = await ElMessageBox.confirm('是否关闭全部页签', '提示', {
      type: 'warning'
    }).catch(() => false);
    if (!ret) return;
    const items = tabs.value;
    tabs.value = [];
    activeTab(home.value);
    return items;
  };

  /**
   * 删除除当前激活标签页之外的所有标签页
   * @returns
   */
  const removeOtherTabs = async () => {
    const ret = await ElMessageBox.confirm('是否关闭其他页签', '提示', {
      type: 'warning'
    }).catch(() => false);
    if (!ret) return;
    const match = tabs.value.filter((n) => n.id !== tabValue.value);
    tabs.value = tabs.value.filter((n) => n.id === tabValue.value);
    return match;
  };

  /**
   * 将标签页移动到显示区域并激活
   * @param tab
   */
  const moveToShow = (tab: MaskTab) => {
    const items = tabs.value.filter((n) => n.id !== tab.id);
    tabs.value = [tab, ...items];
    activeTab(tab);
  };

  /**
   * 初始化标签页，根据当前路由设置激活标签页
   */
  const init = async () => {
    await nextTick();

    const isHome = home.value.url === route.fullPath;
    // console.log('isHome', isHome);
    const menu = getMenuByUrl(route.fullPath);
    // console.log('menu', menu);
    if (isHome) {
      tabValue.value = home.value.id;
    } else if (menu) {
      const tab = await createTab(menu);
      // console.log('tab', tab);
      addTab(tab);
    }

    await nextTick();

    active.value = menu || null;
  };

  watch(menus, init);
  watch(
    () => route.params.prjId,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (_newPrjId) => {
      // 只监听项目ID变化下page相关路由变化
      if (route.path.startsWith('/page/')) {
        // console.log('Project ID changed:', _newPrjId);
        tabs.value = [];
        init();
      }
    },
    { immediate: true }
  );

  // 提供tabCreators，defineTab中inject使用，其余组件同样可以通过inject使用
  provide(TAB_CREATORS_KEY, tabCreators);

  return {
    tabRef,
    tabs,
    showTabs,
    currentTab,
    changeTab,
    removeTab,
    updateTab,
    addTab,
    home,
    tabValue,
    isCurrentTab,
    activeHome,
    activeTab,
    dropdownTabs,
    removeAllTabs,
    removeOtherTabs,
    moveToShow
  };
}
