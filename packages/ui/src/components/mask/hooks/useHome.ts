import { computed } from 'vue';
import type { MaskProps, MaskTab } from '../types';
import { HomeFilled } from 'cfg-packages-icons';
import { uid } from 'cfg-packages-utils';

/**
 * XMask使用的组合式函数。功能：返回主页Tab对象
 * @param props
 * @returns
 */
export function useHome(props: MaskProps) {
  const id = uid();
  return computed<MaskTab>(() => {
    const home = props.home;
    return Object.assign(
      // 默认跳转到项目列表页
      { id, url: '/#/project-list', name: 'MaskHome', icon: HomeFilled, closable: false },
      typeof home === 'string' ? { url: home } : home || {}
    );
  });
}
