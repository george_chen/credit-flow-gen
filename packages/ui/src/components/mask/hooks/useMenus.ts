import { shallowRef, computed, watchEffect, ref, type Ref } from 'vue';
import { useRouter } from 'vue-router';
import { arrayToMap, isUrl } from 'cfg-packages-utils';
import type { MaskProps, MaskEmitsFn } from '../types';
import { type MenuDataItem, createDialog } from '../../';

/**
 * 将嵌套的菜单项数组展平为一个扁平数组
 * @param array
 * @param menuAdapter
 * @returns
 */
function toFlat(array: MenuDataItem[], menuAdapter?: (menu: MenuDataItem) => MenuDataItem) {
  let result: MenuDataItem[] = [];
  array.forEach((n) => {
    n = menuAdapter ? menuAdapter(n) : n;
    if (!n.children) {
      result.push(n);
    } else {
      result = result.concat(toFlat(n.children, menuAdapter));
    }
  });
  return result;
}

/**
 * XMask使用的组合式函数。管理菜单项及其相关的功能。提供了对菜单项的选择、收藏、初始化等操作。
 * @param props
 * @param emit
 * @returns
 */
export function useMenus(props: MaskProps, emit: MaskEmitsFn) {
  const router = useRouter();

  // 菜单原始数据
  const menus = shallowRef<MenuDataItem[]>([]);
  // 收藏菜单项
  const favorites = shallowRef<MenuDataItem[]>([]);
  // 扁平菜单项
  const flatMenus = computed(() => toFlat(menus.value, props.menuAdapter));
  // 菜单项的映射，用于根据 ID 快速查找
  const menusMap = computed(() => arrayToMap(flatMenus.value, 'id'));
  // 当前激活的菜单项
  const active: Ref<MenuDataItem | null> = ref(null);

  /**
   * 根据菜单项的类型执行不同的操作（导航、打开窗口或对话框）
   * @param id
   * @returns
   */
  const select = (id: string | number | MenuDataItem): void => {
    const menuId = typeof id === 'object' ? id.id : id;
    const menu = menusMap.value.get(menuId);
    if (!menu) {
      console.warn('找不到菜单', id);
      return;
    }

    const { type = 'route', url, title, icon } = menu;

    if (!url) {
      active.value = menu;
      emit('select', menu);
      return;
    }

    if (type === 'route') {
      if (isUrl(url) || url.startsWith('//')) {
        window.open(url);
      } else {
        active.value = menu;
        router.push(url).catch((e) => e);
      }
      return;
    }

    if (type === 'window') {
      window.open(url);
      return;
    }

    if (type === 'dialog') {
      createDialog({
        resizable: true,
        bodyPadding: false,
        width: '80%',
        height: '80%',
        title,
        icon,
        src: url
      });
    }
  };

  /**
   * 初始化菜单和收藏数据，支持函数和数组类型的数据源
   */
  const init = async () => {
    menus.value = typeof props.menus === 'function' ? (await props.menus()) || [] : props.menus ?? [];
    favorites.value = typeof props.favorites === 'function' ? (await props.favorites()) || [] : props.favorites ?? [];
  };

  /**
   * 添加菜单项到收藏列表
   * @param item
   */
  const addFavorite = (item: MenuDataItem) => {
    favorites.value = [item, ...favorites.value];
    if (props.addFavorite) {
      props.addFavorite(item);
    }
  };

  /**
   * 从收藏列表中移除菜单项
   * @param item
   */
  const removeFavorite = (item: MenuDataItem) => {
    favorites.value = favorites.value.filter((n) => n.id !== item.id);
    if (props.removeFavorite) {
      props.removeFavorite(item);
    }
  };

  /**
   * 检查菜单项是否在收藏列表中
   * @param item
   * @returns
   */
  const isFavorite = (item: MenuDataItem) => {
    return !!favorites.value.find((n) => n === item || n.id === item.id);
  };

  /**
   * 切换菜单项的收藏状态
   * @param item
   */
  const toggleFavorite = (item: MenuDataItem) => {
    isFavorite(item) ? removeFavorite(item) : addFavorite(item);
  };

  // 监视 init 函数，确保在菜单和收藏数据变化时自动初始化
  watchEffect(init);

  // 返回：菜单数据、收藏数据、选择菜单、切换收藏等函数和属性
  return {
    menus,
    favorites,
    flatMenus,
    active,
    select,
    toggleFavorite
  };
}
