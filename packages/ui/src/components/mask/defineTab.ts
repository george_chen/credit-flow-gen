import { computed, inject } from 'vue';
import { useRoute } from 'vue-router';
import { type MaskTab, type TabCreator, TAB_CREATORS_KEY, MASK_KEY } from './types';

/**
 * 定义标签页
 * @param options
 * @returns
 */
export function defineTab(options: Partial<MaskTab> | TabCreator = {}) {
  const route = useRoute();

  // useTabs里面provide出来，这里inject使用
  const tabCreators = inject(TAB_CREATORS_KEY, null);
  if (tabCreators) {
    const creator = typeof options === 'function' ? options : async () => options;
    tabCreators[route.fullPath] = creator;
  }

  const mask: any = inject(MASK_KEY, null);
  const tab = computed<MaskTab | null>(() => {
    return mask ? mask.tabs.value.find((n: MaskTab) => n.url === route.fullPath) : null;
  });

  return {
    tab,
    mask
  };
}
