/**
 * 二维码（带有刷新生成功能）组件
 */
import XQrcode from './Qrcode.vue';
export { XQrcode };
export * from './types';
