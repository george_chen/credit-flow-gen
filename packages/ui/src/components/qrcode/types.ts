import type { PropType } from 'vue';
import type { ComponentPropsType } from '../shared';
import type { QRCodeToDataURLOptions, QRCodeToDataURLOptionsJpegWebp } from 'qrcode';

export type { QRCodeSegment } from 'qrcode';

export type QRCodeValue = string | (() => Promise<string>);

export const TYPES = ['image/png', 'image/jpeg', 'image/webp'] as const;

export type QRCodeProps = Omit<QRCodeToDataURLOptions, 'renderOptions'> &
  QRCodeToDataURLOptionsJpegWebp['rendererOpts'] & {
    value: QRCodeValue;
  };

export const qrcodeProps = {
  /**
   * 外边距
   */
  margin: {
    type: Number
  },
  /**
   * 规模
   */
  scale: {
    type: Number
  },
  /**
   * 宽度
   */
  width: {
    type: Number,
    default: 200
  },
  color: {
    type: Object
  },
  darkColor: {
    type: String,
    default: '#000000ff'
  },
  lightColor: {
    type: String,
    default: '#ffffffff'
  },
  quality: {
    type: Number,
    default: 0.92
  },
  value: {
    type: [String, Function] as PropType<QRCodeValue>,
    default: ''
  },
  /**
   * 超时时间 毫秒
   */
  timeout: {
    type: Number,
    default: 0
  },
  /**
   * 提示
   */
  tip: {
    type: String,
    default: '二维码已失效请刷新后重试'
  }
};

export type QrcodeProps = ComponentPropsType<typeof qrcodeProps>;

export type qrcodeEmits = {
  refresh: [];
};
