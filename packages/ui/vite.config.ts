import { createViteConfig } from 'cfg-packages-cli';

/**
 * UMD(Universal Module Definition)模式，旨在解决模块在不同环境中的兼容性问题。
 * 可以同时在浏览器和 Node.js 环境下运行。
 */
const isUmd = !!process.env.UMD;

export default createViteConfig({
  lib: true,
  dts: isUmd ? false : true,
  buildTarget: isUmd ? 'es2015' : 'esnext',
  entry: isUmd ? 'src/install.ts' : 'src/index.ts',
  version: true,
  library: 'CfgUI',
  emptyOutDir: isUmd ? false : true,
  external: isUmd
    ? [
        'vue',
        'vue-router',
        'element-plus',
        '@element-plus/icons-vue',
        '@vueuse/core',
        'cfg-packages-utils',
        'cfg-packages-icons',
        'echarts'
      ]
    : [
        'vue',
        'vue-router',
        'element-plus',
        '@element-plus/icons-vue',
        '@vueuse/core',
        'cfg-packages-utils',
        'cfg-packages-icons',
        'echarts'
      ],
  externalGlobals: isUmd
    ? {
        vue: 'Vue',
        'vue-router': 'VueRouter',
        'element-plus': 'ElementPlus',
        '@element-plus/icons-vue': 'CfgIcons',
        '@vueuse/core': 'VueUse',
        'cfg-packages-utils': 'CfgUtils',
        'cfg-packages-icons': 'CfgIcons',
        echarts: 'echarts'
      }
    : undefined,
  formats: isUmd ? ['umd'] : ['es']
});
