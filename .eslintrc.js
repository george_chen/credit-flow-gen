/**
 * 基础eslint配置，支持ts
 */
module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es2021: true
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    // 'plugin:prettier/recommended',
    /**
     * eslint-config-prettier 的缩写，覆盖冲突的eslint规则
     * plugin:prettier/recommended内部extends配置了prettier规则，
     * 测试过这里不配置prettier也行，需要注意的是prettier规则放最后
     */
    // 'prettier',
    'plugin:vue/vue3-recommended',
    '@vue/eslint-config-typescript',
    /**
     * 编码过程中提示prettier格式问题，
     * 如需跳过可设置成：'@vue/eslint-config-prettier/skip-formatting'
     */
    '@vue/eslint-config-prettier'
    // '@vue/eslint-config-prettier/skip-formatting'
  ],
  parser: 'vue-eslint-parser',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
    parser: '@typescript-eslint/parser',
    ecmaFeatures: {
      jsx: true,
      tsx: true
    }
  },
  plugins: [
    /**
     * eslint-plugin-prettier 缩写
     * plugin:prettier/recommended内部plugins配置了prettier插件，
     * 测试过这里不配置prettier也行
     */
    // 'prettier',
    /**
     * eslint-plugin-vue 缩写
     * plugin:vue/vue3-recommended->basse内部plugins配置了vue插件，
     * 同样这里不配置vue也行
     */
    'vue',
    '@typescript-eslint'
  ],
  rules: {
    // 强制generator函数*号的空格规则 - 前面没空格，后面有空格
    'generator-star-spacing': ['error', { before: false, after: true }],
    // 禁止多余的 return 语句
    'no-useless-return': 'error',
    // 在正则表达式的开头明确禁止除法运算符，默认关闭
    'no-div-regex': 'error',
    // 在 function 定义左括号之前强制保持一致的间距
    'space-before-function-paren': [
      'error',
      {
        anonymous: 'always', // function () {}
        named: 'never', // function foo() {}
        asyncArrow: 'always' // async () => {}
      }
    ],
    // 关闭禁止TypeScript 中存在 any 类型的规范，即允许使用 any 类型
    '@typescript-eslint/no-explicit-any': 'off',
    // 关闭组件名称是多词的规范，即允许组件名称是单词的形式
    'vue/multi-word-component-names': 'off'
  },
  // 忽略检测的全局变量,视项目情况而定
  globals: {}
};
